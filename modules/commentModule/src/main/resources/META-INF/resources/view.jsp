<%@ include file="/init.jsp" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>


<c:set var="currentUrl" value="${currentURL}"/>
<c:set var="class" value="${className}"/>
<portlet:actionURL name="commentTestSubmit" var="discussionURL" />

<!-- Uncomment this to add a rating system to the comment portlet, can serve as the voting for the module
<liferay-ui:ratings
	className= "${class}"
    classPK= "${classPK}"
    type="stars"
/>
-->

<liferay-ui:discussion
	classPK="${classPK}"
	userId="${userId}"
	className="${class}"
	formAction="<%= discussionURL%>"
	ratingsEnabled="<%=true%>"
	formName="fm2"
	redirect="${currentUrl}"
/>