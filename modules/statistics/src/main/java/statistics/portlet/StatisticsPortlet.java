package statistics.portlet;

import statistics.constants.StatisticsPortletKeys;


import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;


import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ProcessAction;

/**
 * @author englmeier
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.EEN",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=statistics Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + StatisticsPortletKeys.Statistics,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class StatisticsPortlet extends MVCPortlet{

}