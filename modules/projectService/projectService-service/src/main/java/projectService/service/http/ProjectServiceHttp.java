/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package projectService.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import projectService.service.ProjectServiceUtil;

/**
 * Provides the HTTP utility for the
 * {@link ProjectServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectServiceSoap
 * @see HttpPrincipal
 * @see ProjectServiceUtil
 * @generated
 */
@ProviderType
public class ProjectServiceHttp {
	public static java.lang.String getProjectById(HttpPrincipal httpPrincipal,
		long projectId) throws projectService.exception.NoSuchProjectException {
		try {
			MethodKey methodKey = new MethodKey(ProjectServiceUtil.class,
					"getProjectById", _getProjectByIdParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, projectId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof projectService.exception.NoSuchProjectException) {
					throw (projectService.exception.NoSuchProjectException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getAllProjects(HttpPrincipal httpPrincipal) {
		try {
			MethodKey methodKey = new MethodKey(ProjectServiceUtil.class,
					"getAllProjects", _getAllProjectsParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String insertNewProject(
		HttpPrincipal httpPrincipal, long groupId, long companyId, long userId,
		java.lang.String title, java.lang.String description,
		boolean isPublished, java.lang.String titleImgRef,
		java.lang.String url, long layoutRef, long titleFileRef) {
		try {
			MethodKey methodKey = new MethodKey(ProjectServiceUtil.class,
					"insertNewProject", _insertNewProjectParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId,
					companyId, userId, title, description, isPublished,
					titleImgRef, url, layoutRef, titleFileRef);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(ProjectServiceHttp.class);
	private static final Class<?>[] _getProjectByIdParameterTypes0 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getAllProjectsParameterTypes1 = new Class[] {
			
		};
	private static final Class<?>[] _insertNewProjectParameterTypes2 = new Class[] {
			long.class, long.class, long.class, java.lang.String.class,
			java.lang.String.class, boolean.class, java.lang.String.class,
			java.lang.String.class, long.class, long.class
		};
}