/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package projectService.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.LayoutFriendlyURL;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.LayoutFriendlyURLLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import projectService.model.Project;
import projectService.service.ProjectLocalServiceUtil;
import projectService.service.base.ProjectLocalServiceBaseImpl;
import projectService.service.persistence.ProjectUtil;

/**
 * The implementation of the project local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link projectService.service.ProjectLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectLocalServiceBaseImpl
 * @see projectService.service.ProjectLocalServiceUtil
 */
public class ProjectLocalServiceImpl extends ProjectLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link projectService.service.ProjectLocalServiceUtil} to access the project local service.
	 */

	/**
	 * returns all projects as a HashMap
	 */
	public Map<Long,Project> getAllProjectsAsMap(){
		HashMap<Long,Project> result = new HashMap<Long,Project>();
		for(Project p : ProjectUtil.findAll()){
			result.put(p.getPrimaryKey(), p);
		}
		return result;
	}

	/**
	 * returns all exisitng projects
	 */
	public List<Project> getAllProjects(){
		return ProjectUtil.findAll();
	}

	/**
	 * returns the project corresponding to the given layoutRef
	 */
	public Project getProjectByLayoutIdRef(long layoutRef){
		return ProjectUtil.fetchByLayoutRef(layoutRef);
	}

	/**
	 * creates a new project with the next possible primary key.
	 * Does not persist the project.
	 */
	public Project createProjectWithAutomatedDbId(long groupId, long companyId, long userId, String title, String description,
			boolean isPublished, String titleImgRef, String url, long layoutRef, long titleFileRef){
		int nextDbId = (int)CounterLocalServiceUtil.increment(Project.class.getName());
		Project nextProject = ProjectLocalServiceUtil.createProject(nextDbId);
		nextProject.setGroupId(groupId);
		nextProject.setCompanyId(companyId);
		nextProject.setTitle(title);
		nextProject.setPageUrl(url);
		nextProject.setLayoutRef(layoutRef);
		nextProject.setTitleImgRef(titleImgRef);
		nextProject.setTitleFileRef(titleFileRef);
		nextProject.setDescription(description);
		nextProject.setIsPublished(isPublished);
		nextProject.setUuid(PortalUUIDUtil.generate());
		nextProject.setProjectToken(null);
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			nextProject.setUserName(user.getScreenName());
		} catch (PortalException e) {
			//TODO
			e.printStackTrace();
		}
		return nextProject;
	}

	/**
	 * deletes the project with projectId and the corresponding layout.
	 */
	public Project deleteProjectAndLayoutOnCascade(long projectId){
		try {
			Project p = deleteProject(projectId);
			LayoutLocalServiceUtil.deleteLayout(p.getLayoutRef());
			//Delete friendly url
			List<LayoutFriendlyURL> urls = LayoutFriendlyURLLocalServiceUtil
					.getLayoutFriendlyURLs(0, LayoutFriendlyURLLocalServiceUtil.getLayoutFriendlyURLsCount());
			for(LayoutFriendlyURL l : urls){
				if(p.getPageUrl().equals(l.getFriendlyURL())){
					 LayoutFriendlyURLLocalServiceUtil.deleteLayoutFriendlyURL(l);
				}
			}
			//delete title pic file
			DLAppLocalServiceUtil.deleteFileEntry(p.getTitleFileRef());
			return p;
		} catch (PortalException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * deletes the project and the corresponding layout
	 */
	public Project deleteProjectAndLayoutOnCascade(Project projectToDelete){
		return deleteProjectAndLayoutOnCascade(projectToDelete.getPrimaryKey());
	}

	/**
	 * persists the Project after validating it
	 */
	public void persistProjectAndPerformTypeChecks(Project p){
		//TODO add to assetentry
		//TODO add validation
		p.persist();
	}

	@Deprecated
	public Project createProjectWithAutomatedDbId(long groupId, long companyId, long userId, String title, String description, boolean isPublished){
		int nextDbId = (int)CounterLocalServiceUtil.increment(Project.class.getName());
		Project nextProject = ProjectLocalServiceUtil.createProject(nextDbId);
		nextProject.setGroupId(groupId);
		nextProject.setCompanyId(companyId);
		nextProject.setTitle(title);
		nextProject.setDescription(description);
		nextProject.setIsPublished(isPublished);
		nextProject.setUuid(PortalUUIDUtil.generate());
		nextProject.setProjectToken(title + StringPool.DOLLAR + userId + StringPool.DOLLAR + nextProject.getUuid());
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			nextProject.setUserName(user.getScreenName());
		} catch (PortalException e) {
			//TODO
			e.printStackTrace();
		}
		return nextProject;
	}

}
