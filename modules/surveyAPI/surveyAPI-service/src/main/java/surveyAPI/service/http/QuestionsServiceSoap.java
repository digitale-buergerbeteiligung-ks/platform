/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import surveyAPI.service.QuestionsServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link QuestionsServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link surveyAPI.model.QuestionsSoap}.
 * If the method in the service utility returns a
 * {@link surveyAPI.model.Questions}, that is translated to a
 * {@link surveyAPI.model.QuestionsSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsServiceHttp
 * @see surveyAPI.model.QuestionsSoap
 * @see QuestionsServiceUtil
 * @generated
 */
@ProviderType
public class QuestionsServiceSoap {
	public static org.json.JSONObject deleteQuestion(long surveyID, long qID)
		throws RemoteException {
		try {
			org.json.JSONObject returnValue = QuestionsServiceUtil.deleteQuestion(surveyID,
					qID);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static org.json.JSONObject editQuestion(long qID,
		java.lang.String qText) throws RemoteException {
		try {
			org.json.JSONObject returnValue = QuestionsServiceUtil.editQuestion(qID,
					qText);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static org.json.JSONObject addNewQuestion(long surveyID,
		java.lang.String QName, java.lang.String QText, java.lang.String Type,
		boolean required) throws RemoteException {
		try {
			org.json.JSONObject returnValue = QuestionsServiceUtil.addNewQuestion(surveyID,
					QName, QText, Type, required);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static org.json.JSONObject addQuestionNChoices(
		java.lang.String quesNChoices) throws RemoteException {
		try {
			org.json.JSONObject returnValue = QuestionsServiceUtil.addQuestionNChoices(quesNChoices);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(QuestionsServiceSoap.class);
}