/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.jsonwebservice.JSONWebService;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.service.QuestionsLocalServiceUtil;
import surveyAPI.service.base.QuestionsServiceBaseImpl;

/**
 * The implementation of the questions remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.QuestionsService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsServiceBaseImpl
 * @see surveyAPI.service.QuestionsServiceUtil
 */
@ProviderType
public class QuestionsServiceImpl extends QuestionsServiceBaseImpl {

	@JSONWebService (method = "DELETE")
	public JSONObject deleteQuestion(long surveyID, long qID) throws JSONException{
		return QuestionsLocalServiceUtil.deleteQuestion(surveyID, qID);
	}
	
	@JSONWebService(method = "PUT")
	public JSONObject editQuestion(long qID, String qText) throws JSONException{
		return QuestionsLocalServiceUtil.editQuestion(qID, qText);
	}
	
	//--TODO Add input validation for QName and QText
	@JSONWebService(method = "POST")
	public JSONObject addNewQuestion(long surveyID, String QName, String QText, String Type,boolean required) throws JSONException{
		return QuestionsLocalServiceUtil.quesNChoiceUtil(surveyID, QName, QText, required, null, Type);
	}
	
	@JSONWebService(method = "POST")
	public JSONObject addQuestionNChoices(String quesNChoices)throws JSONException{
		return QuestionsLocalServiceUtil.addQuestionNChoices(quesNChoices);
	}
}