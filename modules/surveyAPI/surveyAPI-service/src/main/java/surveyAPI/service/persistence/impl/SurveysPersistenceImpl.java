/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchSurveysException;

import surveyAPI.model.Surveys;

import surveyAPI.model.impl.SurveysImpl;
import surveyAPI.model.impl.SurveysModelImpl;

import surveyAPI.service.persistence.SurveysPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the surveys service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SurveysPersistence
 * @see surveyAPI.service.persistence.SurveysUtil
 * @generated
 */
@ProviderType
public class SurveysPersistenceImpl extends BasePersistenceImpl<Surveys>
	implements SurveysPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SurveysUtil} to access the surveys persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SurveysImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SurveysModelImpl.ENTITY_CACHE_ENABLED,
			SurveysModelImpl.FINDER_CACHE_ENABLED, SurveysImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SurveysModelImpl.ENTITY_CACHE_ENABLED,
			SurveysModelImpl.FINDER_CACHE_ENABLED, SurveysImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SurveysModelImpl.ENTITY_CACHE_ENABLED,
			SurveysModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public SurveysPersistenceImpl() {
		setModelClass(Surveys.class);
	}

	/**
	 * Caches the surveys in the entity cache if it is enabled.
	 *
	 * @param surveys the surveys
	 */
	@Override
	public void cacheResult(Surveys surveys) {
		entityCache.putResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
			SurveysImpl.class, surveys.getPrimaryKey(), surveys);

		surveys.resetOriginalValues();
	}

	/**
	 * Caches the surveyses in the entity cache if it is enabled.
	 *
	 * @param surveyses the surveyses
	 */
	@Override
	public void cacheResult(List<Surveys> surveyses) {
		for (Surveys surveys : surveyses) {
			if (entityCache.getResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
						SurveysImpl.class, surveys.getPrimaryKey()) == null) {
				cacheResult(surveys);
			}
			else {
				surveys.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all surveyses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SurveysImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the surveys.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Surveys surveys) {
		entityCache.removeResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
			SurveysImpl.class, surveys.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Surveys> surveyses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Surveys surveys : surveyses) {
			entityCache.removeResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
				SurveysImpl.class, surveys.getPrimaryKey());
		}
	}

	/**
	 * Creates a new surveys with the primary key. Does not add the surveys to the database.
	 *
	 * @param SurveyID the primary key for the new surveys
	 * @return the new surveys
	 */
	@Override
	public Surveys create(long SurveyID) {
		Surveys surveys = new SurveysImpl();

		surveys.setNew(true);
		surveys.setPrimaryKey(SurveyID);

		return surveys;
	}

	/**
	 * Removes the surveys with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param SurveyID the primary key of the surveys
	 * @return the surveys that was removed
	 * @throws NoSuchSurveysException if a surveys with the primary key could not be found
	 */
	@Override
	public Surveys remove(long SurveyID) throws NoSuchSurveysException {
		return remove((Serializable)SurveyID);
	}

	/**
	 * Removes the surveys with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the surveys
	 * @return the surveys that was removed
	 * @throws NoSuchSurveysException if a surveys with the primary key could not be found
	 */
	@Override
	public Surveys remove(Serializable primaryKey)
		throws NoSuchSurveysException {
		Session session = null;

		try {
			session = openSession();

			Surveys surveys = (Surveys)session.get(SurveysImpl.class, primaryKey);

			if (surveys == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSurveysException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(surveys);
		}
		catch (NoSuchSurveysException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Surveys removeImpl(Surveys surveys) {
		surveys = toUnwrappedModel(surveys);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(surveys)) {
				surveys = (Surveys)session.get(SurveysImpl.class,
						surveys.getPrimaryKeyObj());
			}

			if (surveys != null) {
				session.delete(surveys);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (surveys != null) {
			clearCache(surveys);
		}

		return surveys;
	}

	@Override
	public Surveys updateImpl(Surveys surveys) {
		surveys = toUnwrappedModel(surveys);

		boolean isNew = surveys.isNew();

		Session session = null;

		try {
			session = openSession();

			if (surveys.isNew()) {
				session.save(surveys);

				surveys.setNew(false);
			}
			else {
				surveys = (Surveys)session.merge(surveys);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
			SurveysImpl.class, surveys.getPrimaryKey(), surveys, false);

		surveys.resetOriginalValues();

		return surveys;
	}

	protected Surveys toUnwrappedModel(Surveys surveys) {
		if (surveys instanceof SurveysImpl) {
			return surveys;
		}

		SurveysImpl surveysImpl = new SurveysImpl();

		surveysImpl.setNew(surveys.isNew());
		surveysImpl.setPrimaryKey(surveys.getPrimaryKey());

		surveysImpl.setSurveyID(surveys.getSurveyID());
		surveysImpl.setSurveyName(surveys.getSurveyName());
		surveysImpl.setDateOfCreation(surveys.getDateOfCreation());
		surveysImpl.setQuestionIds(surveys.getQuestionIds());

		return surveysImpl;
	}

	/**
	 * Returns the surveys with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the surveys
	 * @return the surveys
	 * @throws NoSuchSurveysException if a surveys with the primary key could not be found
	 */
	@Override
	public Surveys findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSurveysException {
		Surveys surveys = fetchByPrimaryKey(primaryKey);

		if (surveys == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSurveysException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return surveys;
	}

	/**
	 * Returns the surveys with the primary key or throws a {@link NoSuchSurveysException} if it could not be found.
	 *
	 * @param SurveyID the primary key of the surveys
	 * @return the surveys
	 * @throws NoSuchSurveysException if a surveys with the primary key could not be found
	 */
	@Override
	public Surveys findByPrimaryKey(long SurveyID)
		throws NoSuchSurveysException {
		return findByPrimaryKey((Serializable)SurveyID);
	}

	/**
	 * Returns the surveys with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the surveys
	 * @return the surveys, or <code>null</code> if a surveys with the primary key could not be found
	 */
	@Override
	public Surveys fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
				SurveysImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Surveys surveys = (Surveys)serializable;

		if (surveys == null) {
			Session session = null;

			try {
				session = openSession();

				surveys = (Surveys)session.get(SurveysImpl.class, primaryKey);

				if (surveys != null) {
					cacheResult(surveys);
				}
				else {
					entityCache.putResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
						SurveysImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
					SurveysImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return surveys;
	}

	/**
	 * Returns the surveys with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param SurveyID the primary key of the surveys
	 * @return the surveys, or <code>null</code> if a surveys with the primary key could not be found
	 */
	@Override
	public Surveys fetchByPrimaryKey(long SurveyID) {
		return fetchByPrimaryKey((Serializable)SurveyID);
	}

	@Override
	public Map<Serializable, Surveys> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Surveys> map = new HashMap<Serializable, Surveys>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Surveys surveys = fetchByPrimaryKey(primaryKey);

			if (surveys != null) {
				map.put(primaryKey, surveys);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
					SurveysImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Surveys)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SURVEYS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Surveys surveys : (List<Surveys>)q.list()) {
				map.put(surveys.getPrimaryKeyObj(), surveys);

				cacheResult(surveys);

				uncachedPrimaryKeys.remove(surveys.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(SurveysModelImpl.ENTITY_CACHE_ENABLED,
					SurveysImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the surveyses.
	 *
	 * @return the surveyses
	 */
	@Override
	public List<Surveys> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the surveyses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of surveyses
	 * @param end the upper bound of the range of surveyses (not inclusive)
	 * @return the range of surveyses
	 */
	@Override
	public List<Surveys> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the surveyses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of surveyses
	 * @param end the upper bound of the range of surveyses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of surveyses
	 */
	@Override
	public List<Surveys> findAll(int start, int end,
		OrderByComparator<Surveys> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the surveyses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of surveyses
	 * @param end the upper bound of the range of surveyses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of surveyses
	 */
	@Override
	public List<Surveys> findAll(int start, int end,
		OrderByComparator<Surveys> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Surveys> list = null;

		if (retrieveFromCache) {
			list = (List<Surveys>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SURVEYS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SURVEYS;

				if (pagination) {
					sql = sql.concat(SurveysModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Surveys>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Surveys>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the surveyses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Surveys surveys : findAll()) {
			remove(surveys);
		}
	}

	/**
	 * Returns the number of surveyses.
	 *
	 * @return the number of surveyses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SURVEYS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SurveysModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the surveys persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(SurveysImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SURVEYS = "SELECT surveys FROM Surveys surveys";
	private static final String _SQL_SELECT_SURVEYS_WHERE_PKS_IN = "SELECT surveys FROM Surveys surveys WHERE SurveyID IN (";
	private static final String _SQL_COUNT_SURVEYS = "SELECT COUNT(surveys) FROM Surveys surveys";
	private static final String _ORDER_BY_ENTITY_ALIAS = "surveys.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Surveys exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(SurveysPersistenceImpl.class);
}