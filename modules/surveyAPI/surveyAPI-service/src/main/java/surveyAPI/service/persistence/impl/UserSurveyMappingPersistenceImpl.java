/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchUserSurveyMappingException;

import surveyAPI.model.UserSurveyMapping;

import surveyAPI.model.impl.UserSurveyMappingImpl;
import surveyAPI.model.impl.UserSurveyMappingModelImpl;

import surveyAPI.service.persistence.UserSurveyMappingPK;
import surveyAPI.service.persistence.UserSurveyMappingPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the user survey mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserSurveyMappingPersistence
 * @see surveyAPI.service.persistence.UserSurveyMappingUtil
 * @generated
 */
@ProviderType
public class UserSurveyMappingPersistenceImpl extends BasePersistenceImpl<UserSurveyMapping>
	implements UserSurveyMappingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserSurveyMappingUtil} to access the user survey mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserSurveyMappingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
			UserSurveyMappingModelImpl.FINDER_CACHE_ENABLED,
			UserSurveyMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
			UserSurveyMappingModelImpl.FINDER_CACHE_ENABLED,
			UserSurveyMappingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
			UserSurveyMappingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public UserSurveyMappingPersistenceImpl() {
		setModelClass(UserSurveyMapping.class);
	}

	/**
	 * Caches the user survey mapping in the entity cache if it is enabled.
	 *
	 * @param userSurveyMapping the user survey mapping
	 */
	@Override
	public void cacheResult(UserSurveyMapping userSurveyMapping) {
		entityCache.putResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
			UserSurveyMappingImpl.class, userSurveyMapping.getPrimaryKey(),
			userSurveyMapping);

		userSurveyMapping.resetOriginalValues();
	}

	/**
	 * Caches the user survey mappings in the entity cache if it is enabled.
	 *
	 * @param userSurveyMappings the user survey mappings
	 */
	@Override
	public void cacheResult(List<UserSurveyMapping> userSurveyMappings) {
		for (UserSurveyMapping userSurveyMapping : userSurveyMappings) {
			if (entityCache.getResult(
						UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
						UserSurveyMappingImpl.class,
						userSurveyMapping.getPrimaryKey()) == null) {
				cacheResult(userSurveyMapping);
			}
			else {
				userSurveyMapping.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user survey mappings.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UserSurveyMappingImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user survey mapping.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserSurveyMapping userSurveyMapping) {
		entityCache.removeResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
			UserSurveyMappingImpl.class, userSurveyMapping.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserSurveyMapping> userSurveyMappings) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserSurveyMapping userSurveyMapping : userSurveyMappings) {
			entityCache.removeResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
				UserSurveyMappingImpl.class, userSurveyMapping.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user survey mapping with the primary key. Does not add the user survey mapping to the database.
	 *
	 * @param userSurveyMappingPK the primary key for the new user survey mapping
	 * @return the new user survey mapping
	 */
	@Override
	public UserSurveyMapping create(UserSurveyMappingPK userSurveyMappingPK) {
		UserSurveyMapping userSurveyMapping = new UserSurveyMappingImpl();

		userSurveyMapping.setNew(true);
		userSurveyMapping.setPrimaryKey(userSurveyMappingPK);

		return userSurveyMapping;
	}

	/**
	 * Removes the user survey mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userSurveyMappingPK the primary key of the user survey mapping
	 * @return the user survey mapping that was removed
	 * @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	 */
	@Override
	public UserSurveyMapping remove(UserSurveyMappingPK userSurveyMappingPK)
		throws NoSuchUserSurveyMappingException {
		return remove((Serializable)userSurveyMappingPK);
	}

	/**
	 * Removes the user survey mapping with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user survey mapping
	 * @return the user survey mapping that was removed
	 * @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	 */
	@Override
	public UserSurveyMapping remove(Serializable primaryKey)
		throws NoSuchUserSurveyMappingException {
		Session session = null;

		try {
			session = openSession();

			UserSurveyMapping userSurveyMapping = (UserSurveyMapping)session.get(UserSurveyMappingImpl.class,
					primaryKey);

			if (userSurveyMapping == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserSurveyMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userSurveyMapping);
		}
		catch (NoSuchUserSurveyMappingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserSurveyMapping removeImpl(UserSurveyMapping userSurveyMapping) {
		userSurveyMapping = toUnwrappedModel(userSurveyMapping);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userSurveyMapping)) {
				userSurveyMapping = (UserSurveyMapping)session.get(UserSurveyMappingImpl.class,
						userSurveyMapping.getPrimaryKeyObj());
			}

			if (userSurveyMapping != null) {
				session.delete(userSurveyMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userSurveyMapping != null) {
			clearCache(userSurveyMapping);
		}

		return userSurveyMapping;
	}

	@Override
	public UserSurveyMapping updateImpl(UserSurveyMapping userSurveyMapping) {
		userSurveyMapping = toUnwrappedModel(userSurveyMapping);

		boolean isNew = userSurveyMapping.isNew();

		Session session = null;

		try {
			session = openSession();

			if (userSurveyMapping.isNew()) {
				session.save(userSurveyMapping);

				userSurveyMapping.setNew(false);
			}
			else {
				userSurveyMapping = (UserSurveyMapping)session.merge(userSurveyMapping);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
			UserSurveyMappingImpl.class, userSurveyMapping.getPrimaryKey(),
			userSurveyMapping, false);

		userSurveyMapping.resetOriginalValues();

		return userSurveyMapping;
	}

	protected UserSurveyMapping toUnwrappedModel(
		UserSurveyMapping userSurveyMapping) {
		if (userSurveyMapping instanceof UserSurveyMappingImpl) {
			return userSurveyMapping;
		}

		UserSurveyMappingImpl userSurveyMappingImpl = new UserSurveyMappingImpl();

		userSurveyMappingImpl.setNew(userSurveyMapping.isNew());
		userSurveyMappingImpl.setPrimaryKey(userSurveyMapping.getPrimaryKey());

		userSurveyMappingImpl.setUserID(userSurveyMapping.getUserID());
		userSurveyMappingImpl.setSurveyID(userSurveyMapping.getSurveyID());
		userSurveyMappingImpl.setDateOfSurvey(userSurveyMapping.getDateOfSurvey());

		return userSurveyMappingImpl;
	}

	/**
	 * Returns the user survey mapping with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user survey mapping
	 * @return the user survey mapping
	 * @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	 */
	@Override
	public UserSurveyMapping findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserSurveyMappingException {
		UserSurveyMapping userSurveyMapping = fetchByPrimaryKey(primaryKey);

		if (userSurveyMapping == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserSurveyMappingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userSurveyMapping;
	}

	/**
	 * Returns the user survey mapping with the primary key or throws a {@link NoSuchUserSurveyMappingException} if it could not be found.
	 *
	 * @param userSurveyMappingPK the primary key of the user survey mapping
	 * @return the user survey mapping
	 * @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	 */
	@Override
	public UserSurveyMapping findByPrimaryKey(
		UserSurveyMappingPK userSurveyMappingPK)
		throws NoSuchUserSurveyMappingException {
		return findByPrimaryKey((Serializable)userSurveyMappingPK);
	}

	/**
	 * Returns the user survey mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user survey mapping
	 * @return the user survey mapping, or <code>null</code> if a user survey mapping with the primary key could not be found
	 */
	@Override
	public UserSurveyMapping fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
				UserSurveyMappingImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		UserSurveyMapping userSurveyMapping = (UserSurveyMapping)serializable;

		if (userSurveyMapping == null) {
			Session session = null;

			try {
				session = openSession();

				userSurveyMapping = (UserSurveyMapping)session.get(UserSurveyMappingImpl.class,
						primaryKey);

				if (userSurveyMapping != null) {
					cacheResult(userSurveyMapping);
				}
				else {
					entityCache.putResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
						UserSurveyMappingImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UserSurveyMappingModelImpl.ENTITY_CACHE_ENABLED,
					UserSurveyMappingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userSurveyMapping;
	}

	/**
	 * Returns the user survey mapping with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userSurveyMappingPK the primary key of the user survey mapping
	 * @return the user survey mapping, or <code>null</code> if a user survey mapping with the primary key could not be found
	 */
	@Override
	public UserSurveyMapping fetchByPrimaryKey(
		UserSurveyMappingPK userSurveyMappingPK) {
		return fetchByPrimaryKey((Serializable)userSurveyMappingPK);
	}

	@Override
	public Map<Serializable, UserSurveyMapping> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, UserSurveyMapping> map = new HashMap<Serializable, UserSurveyMapping>();

		for (Serializable primaryKey : primaryKeys) {
			UserSurveyMapping userSurveyMapping = fetchByPrimaryKey(primaryKey);

			if (userSurveyMapping != null) {
				map.put(primaryKey, userSurveyMapping);
			}
		}

		return map;
	}

	/**
	 * Returns all the user survey mappings.
	 *
	 * @return the user survey mappings
	 */
	@Override
	public List<UserSurveyMapping> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user survey mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user survey mappings
	 * @param end the upper bound of the range of user survey mappings (not inclusive)
	 * @return the range of user survey mappings
	 */
	@Override
	public List<UserSurveyMapping> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user survey mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user survey mappings
	 * @param end the upper bound of the range of user survey mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user survey mappings
	 */
	@Override
	public List<UserSurveyMapping> findAll(int start, int end,
		OrderByComparator<UserSurveyMapping> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user survey mappings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user survey mappings
	 * @param end the upper bound of the range of user survey mappings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of user survey mappings
	 */
	@Override
	public List<UserSurveyMapping> findAll(int start, int end,
		OrderByComparator<UserSurveyMapping> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserSurveyMapping> list = null;

		if (retrieveFromCache) {
			list = (List<UserSurveyMapping>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERSURVEYMAPPING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERSURVEYMAPPING;

				if (pagination) {
					sql = sql.concat(UserSurveyMappingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserSurveyMapping>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserSurveyMapping>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user survey mappings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (UserSurveyMapping userSurveyMapping : findAll()) {
			remove(userSurveyMapping);
		}
	}

	/**
	 * Returns the number of user survey mappings.
	 *
	 * @return the number of user survey mappings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERSURVEYMAPPING);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UserSurveyMappingModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the user survey mapping persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UserSurveyMappingImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USERSURVEYMAPPING = "SELECT userSurveyMapping FROM UserSurveyMapping userSurveyMapping";
	private static final String _SQL_COUNT_USERSURVEYMAPPING = "SELECT COUNT(userSurveyMapping) FROM UserSurveyMapping userSurveyMapping";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userSurveyMapping.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserSurveyMapping exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(UserSurveyMappingPersistenceImpl.class);
}