/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import java.util.List;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.model.Choices;
import surveyAPI.service.base.ChoicesLocalServiceBaseImpl;

/**
 * The implementation of the choices local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.ChoicesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChoicesLocalServiceBaseImpl
 * @see surveyAPI.service.ChoicesLocalServiceUtil
 */
@ProviderType
public class ChoicesLocalServiceImpl extends ChoicesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link surveyAPI.service.ChoicesLocalServiceUtil} to access the choices local service.
	 */

	public List<Choices> findByCTEXT(String choiceText) {
		return this.choicesPersistence.findByCTEXT(choiceText);
	}
}