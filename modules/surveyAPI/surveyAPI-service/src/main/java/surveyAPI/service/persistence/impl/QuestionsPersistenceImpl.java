/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchQuestionsException;

import surveyAPI.model.Questions;

import surveyAPI.model.impl.QuestionsImpl;
import surveyAPI.model.impl.QuestionsModelImpl;

import surveyAPI.service.persistence.QuestionsPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the questions service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsPersistence
 * @see surveyAPI.service.persistence.QuestionsUtil
 * @generated
 */
@ProviderType
public class QuestionsPersistenceImpl extends BasePersistenceImpl<Questions>
	implements QuestionsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuestionsUtil} to access the questions persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuestionsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionsModelImpl.FINDER_CACHE_ENABLED, QuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionsModelImpl.FINDER_CACHE_ENABLED, QuestionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public QuestionsPersistenceImpl() {
		setModelClass(Questions.class);
	}

	/**
	 * Caches the questions in the entity cache if it is enabled.
	 *
	 * @param questions the questions
	 */
	@Override
	public void cacheResult(Questions questions) {
		entityCache.putResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionsImpl.class, questions.getPrimaryKey(), questions);

		questions.resetOriginalValues();
	}

	/**
	 * Caches the questionses in the entity cache if it is enabled.
	 *
	 * @param questionses the questionses
	 */
	@Override
	public void cacheResult(List<Questions> questionses) {
		for (Questions questions : questionses) {
			if (entityCache.getResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
						QuestionsImpl.class, questions.getPrimaryKey()) == null) {
				cacheResult(questions);
			}
			else {
				questions.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all questionses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(QuestionsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the questions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Questions questions) {
		entityCache.removeResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionsImpl.class, questions.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Questions> questionses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Questions questions : questionses) {
			entityCache.removeResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
				QuestionsImpl.class, questions.getPrimaryKey());
		}
	}

	/**
	 * Creates a new questions with the primary key. Does not add the questions to the database.
	 *
	 * @param QuestionID the primary key for the new questions
	 * @return the new questions
	 */
	@Override
	public Questions create(long QuestionID) {
		Questions questions = new QuestionsImpl();

		questions.setNew(true);
		questions.setPrimaryKey(QuestionID);

		return questions;
	}

	/**
	 * Removes the questions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param QuestionID the primary key of the questions
	 * @return the questions that was removed
	 * @throws NoSuchQuestionsException if a questions with the primary key could not be found
	 */
	@Override
	public Questions remove(long QuestionID) throws NoSuchQuestionsException {
		return remove((Serializable)QuestionID);
	}

	/**
	 * Removes the questions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the questions
	 * @return the questions that was removed
	 * @throws NoSuchQuestionsException if a questions with the primary key could not be found
	 */
	@Override
	public Questions remove(Serializable primaryKey)
		throws NoSuchQuestionsException {
		Session session = null;

		try {
			session = openSession();

			Questions questions = (Questions)session.get(QuestionsImpl.class,
					primaryKey);

			if (questions == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuestionsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(questions);
		}
		catch (NoSuchQuestionsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Questions removeImpl(Questions questions) {
		questions = toUnwrappedModel(questions);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(questions)) {
				questions = (Questions)session.get(QuestionsImpl.class,
						questions.getPrimaryKeyObj());
			}

			if (questions != null) {
				session.delete(questions);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (questions != null) {
			clearCache(questions);
		}

		return questions;
	}

	@Override
	public Questions updateImpl(Questions questions) {
		questions = toUnwrappedModel(questions);

		boolean isNew = questions.isNew();

		Session session = null;

		try {
			session = openSession();

			if (questions.isNew()) {
				session.save(questions);

				questions.setNew(false);
			}
			else {
				questions = (Questions)session.merge(questions);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionsImpl.class, questions.getPrimaryKey(), questions, false);

		questions.resetOriginalValues();

		return questions;
	}

	protected Questions toUnwrappedModel(Questions questions) {
		if (questions instanceof QuestionsImpl) {
			return questions;
		}

		QuestionsImpl questionsImpl = new QuestionsImpl();

		questionsImpl.setNew(questions.isNew());
		questionsImpl.setPrimaryKey(questions.getPrimaryKey());

		questionsImpl.setQuestionID(questions.getQuestionID());
		questionsImpl.setResponseType(questions.getResponseType());
		questionsImpl.setQuestionName(questions.getQuestionName());
		questionsImpl.setQuestionText(questions.getQuestionText());
		questionsImpl.setDateOfCreation(questions.getDateOfCreation());
		questionsImpl.setRequired(questions.isRequired());

		return questionsImpl;
	}

	/**
	 * Returns the questions with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the questions
	 * @return the questions
	 * @throws NoSuchQuestionsException if a questions with the primary key could not be found
	 */
	@Override
	public Questions findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuestionsException {
		Questions questions = fetchByPrimaryKey(primaryKey);

		if (questions == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuestionsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return questions;
	}

	/**
	 * Returns the questions with the primary key or throws a {@link NoSuchQuestionsException} if it could not be found.
	 *
	 * @param QuestionID the primary key of the questions
	 * @return the questions
	 * @throws NoSuchQuestionsException if a questions with the primary key could not be found
	 */
	@Override
	public Questions findByPrimaryKey(long QuestionID)
		throws NoSuchQuestionsException {
		return findByPrimaryKey((Serializable)QuestionID);
	}

	/**
	 * Returns the questions with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the questions
	 * @return the questions, or <code>null</code> if a questions with the primary key could not be found
	 */
	@Override
	public Questions fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
				QuestionsImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Questions questions = (Questions)serializable;

		if (questions == null) {
			Session session = null;

			try {
				session = openSession();

				questions = (Questions)session.get(QuestionsImpl.class,
						primaryKey);

				if (questions != null) {
					cacheResult(questions);
				}
				else {
					entityCache.putResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
						QuestionsImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
					QuestionsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return questions;
	}

	/**
	 * Returns the questions with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param QuestionID the primary key of the questions
	 * @return the questions, or <code>null</code> if a questions with the primary key could not be found
	 */
	@Override
	public Questions fetchByPrimaryKey(long QuestionID) {
		return fetchByPrimaryKey((Serializable)QuestionID);
	}

	@Override
	public Map<Serializable, Questions> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Questions> map = new HashMap<Serializable, Questions>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Questions questions = fetchByPrimaryKey(primaryKey);

			if (questions != null) {
				map.put(primaryKey, questions);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
					QuestionsImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Questions)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_QUESTIONS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Questions questions : (List<Questions>)q.list()) {
				map.put(questions.getPrimaryKeyObj(), questions);

				cacheResult(questions);

				uncachedPrimaryKeys.remove(questions.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(QuestionsModelImpl.ENTITY_CACHE_ENABLED,
					QuestionsImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the questionses.
	 *
	 * @return the questionses
	 */
	@Override
	public List<Questions> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the questionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of questionses
	 * @param end the upper bound of the range of questionses (not inclusive)
	 * @return the range of questionses
	 */
	@Override
	public List<Questions> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the questionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of questionses
	 * @param end the upper bound of the range of questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of questionses
	 */
	@Override
	public List<Questions> findAll(int start, int end,
		OrderByComparator<Questions> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the questionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of questionses
	 * @param end the upper bound of the range of questionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of questionses
	 */
	@Override
	public List<Questions> findAll(int start, int end,
		OrderByComparator<Questions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Questions> list = null;

		if (retrieveFromCache) {
			list = (List<Questions>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_QUESTIONS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUESTIONS;

				if (pagination) {
					sql = sql.concat(QuestionsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Questions>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Questions>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the questionses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Questions questions : findAll()) {
			remove(questions);
		}
	}

	/**
	 * Returns the number of questionses.
	 *
	 * @return the number of questionses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUESTIONS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return QuestionsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the questions persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(QuestionsImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_QUESTIONS = "SELECT questions FROM Questions questions";
	private static final String _SQL_SELECT_QUESTIONS_WHERE_PKS_IN = "SELECT questions FROM Questions questions WHERE QuestionID IN (";
	private static final String _SQL_COUNT_QUESTIONS = "SELECT COUNT(questions) FROM Questions questions";
	private static final String _ORDER_BY_ENTITY_ALIAS = "questions.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Questions exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(QuestionsPersistenceImpl.class);
}