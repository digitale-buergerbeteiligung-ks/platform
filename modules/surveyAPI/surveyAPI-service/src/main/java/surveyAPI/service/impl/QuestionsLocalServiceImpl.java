/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.model.Choices;
import surveyAPI.model.QuestionOptions;
import surveyAPI.model.Questions;
import surveyAPI.model.Surveys;
import surveyAPI.model.impl.ChoicesImpl;
import surveyAPI.model.impl.QuestionOptionsImpl;
import surveyAPI.model.impl.QuestionsImpl;
import surveyAPI.service.ChoicesLocalServiceUtil;
import surveyAPI.service.QuestionOptionsLocalServiceUtil;
import surveyAPI.service.QuestionsLocalServiceUtil;
import surveyAPI.service.SurveysLocalServiceUtil;
import surveyAPI.service.base.QuestionsLocalServiceBaseImpl;

/**
 * The implementation of the questions local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.QuestionsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsLocalServiceBaseImpl
 * @see surveyAPI.service.QuestionsLocalServiceUtil
 */
@ProviderType
public class QuestionsLocalServiceImpl extends QuestionsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link surveyAPI.service.QuestionsLocalServiceUtil} to access the questions local service.
	 */
	public JSONObject quesNChoiceUtil(long surveyID, String QName, String QText, boolean required, JSONArray choiceArr, String type) throws JSONException{
		Surveys thisSurvey = SurveysLocalServiceUtil.fetchSurveys(surveyID);
		if(thisSurvey!=null){
			
			Questions newQuestion = new QuestionsImpl();
			newQuestion.setQuestionName(QName);
			newQuestion.setQuestionText(QText);
			newQuestion.setDateOfCreation(new Date(System.currentTimeMillis()));
			newQuestion.setResponseType(type);
			newQuestion.setRequired(required);
			
			QuestionsLocalServiceUtil.addQuestions(newQuestion);
			long QuestionID = newQuestion.getPrimaryKey();
			String qIds = thisSurvey.getQuestionIds();
			if(qIds!=null && !qIds.isEmpty()){
				qIds = qIds+QuestionID+",";
			}else{
				qIds = QuestionID+",";
			}
			
			thisSurvey.setQuestionIds(qIds);
			SurveysLocalServiceUtil.updateSurveys(thisSurvey);
			
			
			if(choiceArr!=null && choiceArr.length()!=0){
				int numChoices = choiceArr.length();
				
				for(int i=0;i<numChoices;i++){
						Choices newChoice = new ChoicesImpl();
						newChoice.setChoiceText(choiceArr.getString(i));
						ChoicesLocalServiceUtil.addChoices(newChoice);
						
						QuestionOptions newOption = new QuestionOptionsImpl();
						newOption.setChoiceID(newChoice.getPrimaryKey());
						newOption.setQuestionID(QuestionID);
						QuestionOptionsLocalServiceUtil.addQuestionOptions(newOption);
					}
				}
			return (new JSONObject()).put("message","success");
		}else{
			return (new JSONObject()).put("message","Survey ID doesn't exist");
		}
	}
	
	public JSONObject deleteQuestion(long surveyID, long qID) throws JSONException{
		try{
			Surveys toBeUpdatedSurvey = SurveysLocalServiceUtil.getSurveys(surveyID);
			String qIds = toBeUpdatedSurvey.getQuestionIds();
			qIds = qIds.replaceAll(qID+",", "");
			toBeUpdatedSurvey.setQuestionIds(qIds);
			SurveysLocalServiceUtil.updateSurveys(toBeUpdatedSurvey);	
			try{
				QuestionsLocalServiceUtil.deleteQuestions(qID);
				return new JSONObject().put("message", "success");
			}catch(PortalException ex){
				return new JSONObject().put("message", "Invalid question ID");
			}
		}catch(PortalException ex){
			return new JSONObject().put("message", "Invalid Survey ID");
		}
	}
	
	public JSONObject editQuestion(long qID, String qText) throws JSONException{
		try{
			Questions toBeUpdatedQn = QuestionsLocalServiceUtil.getQuestions(qID);
			toBeUpdatedQn.setQuestionText(qText);
			QuestionsLocalServiceUtil.updateQuestions(toBeUpdatedQn);
			return new JSONObject().put("message", "Success");
		}catch(PortalException ex){
			return new JSONObject().put("message", "Incorrect QuestionID");
		}
	}
	
	public JSONObject addQuestionNChoices(String quesNChoices)throws JSONException{
		JSONObject inputJson = new JSONObject(quesNChoices);
		JSONArray choiceArray = inputJson.getJSONArray("Choices");
		return quesNChoiceUtil(inputJson.getLong("SurveyID"), inputJson.getString("QName"),inputJson.getString("QText"), inputJson.getBoolean("Required"), choiceArray, inputJson.getString("Type"));
	}
}