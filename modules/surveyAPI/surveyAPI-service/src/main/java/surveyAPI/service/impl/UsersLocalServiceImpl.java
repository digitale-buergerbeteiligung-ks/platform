/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.SpringLayout.Constraints;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.model.QuestionOptions;
import surveyAPI.model.Responses;
import surveyAPI.model.UserSurveyMapping;
import surveyAPI.model.Users;
import surveyAPI.model.impl.ResponsesImpl;
import surveyAPI.model.impl.UserSurveyMappingImpl;
import surveyAPI.model.impl.UsersImpl;
import surveyAPI.service.ChoicesLocalServiceUtil;
import surveyAPI.service.QuestionOptionsLocalServiceUtil;
import surveyAPI.service.ResponsesLocalServiceUtil;
import surveyAPI.service.SurveysLocalServiceUtil;
import surveyAPI.service.UserSurveyMappingLocalServiceUtil;
import surveyAPI.service.UsersLocalServiceUtil;
import surveyAPI.service.base.UsersLocalServiceBaseImpl;
import surveyAPI.service.persistence.UserSurveyMappingPK;

/**
 * The implementation of the users local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.UsersLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UsersLocalServiceBaseImpl
 * @see surveyAPI.service.UsersLocalServiceUtil
 */
@ProviderType
public class UsersLocalServiceImpl extends UsersLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link surveyAPI.service.UsersLocalServiceUtil} to access the users local service.
	 */
	
	public List<Users> findByUName(String UName){
		return this.usersPersistence.findByUName(UName);
	}
	
	/**
	 * 
	 * @param jsonInput 
	 * {
	 * 		"SurveyID":"",
	 * 		ResChoices: [
	 * 			{"QuestionID":"","ChoiceId":""},
	 * 			{"QuestionID":"","ChoiceId":""}
	 * 		],
	 * 		ResTexts: {
	 * 			"QuestionId":"text", //Here QuestionId is of type long, this is 1:"hello"
	 * 			"QuestionId":"text"
	 * 		},
	 * 		"UserEmail" : emailAddress
	 * }
	 * @return
	 * @throws JSONException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public JSONObject addUserResponse(String jsonInput) throws JSONException{
		//Convert input string to JSONObject 
		JSONObject inpJson = new JSONObject(jsonInput);
		
		//validate survey id
		try{
			long surveyId = inpJson.getLong("SurveyID");
			SurveysLocalServiceUtil.getSurveys(surveyId);
			JSONArray quesNChoices = inpJson.getJSONArray("ResChoices");
			
			List optionIds = new ArrayList();
			Map<Long, Boolean> questionWithText = new HashMap<Long, Boolean>();
			
			for(int i=0;i<quesNChoices.length();i++){
				JSONObject quesChoicePair = quesNChoices.getJSONObject(i);
				
				//Validating the question id - choice id pair
				List<QuestionOptions> optionObject = QuestionOptionsLocalServiceUtil.findByQuesIdNChoiceId(quesChoicePair.getLong("QuestionID"), quesChoicePair.getLong("ChoiceId"));
				if(!optionObject.isEmpty()){
					//Option object contains only one row because the query 
					//select * from QuestionOptions where quesId=*something* AND choiceId=*something* return a single row
					//Pair is unique
					optionIds.add(optionObject.get(0).getOptionID());//hence the .get(0) from the list object, we need the first object only
					boolean allowText = ChoicesLocalServiceUtil.getChoices(quesChoicePair.getLong("ChoiceId")).getChoiceText().equals("�ndern");
					//System.out.println(allowText);
					questionWithText.put(quesChoicePair.getLong("QuestionID"), allowText); 
				}else{
					return new JSONObject().put("error", "Invalid Ques Id and choice Id pair");
				}
			}
			
			Users newUser = new UsersImpl();
			
			//TODO:add validation for email
			String uEmail = inpJson.getString("UserEmail");
			long uId;
			
			//user email is a compulsory parameter
			if(uEmail!=null && !uEmail.isEmpty()){
				//checks if the user already exists
				if(UsersLocalServiceUtil.findByUName(uEmail).isEmpty()){
					newUser.setEmail(uEmail);
					newUser.setUserName(uEmail);
					
					//ADDING a new user here
					UsersLocalServiceUtil.addUsers(newUser);
					uId = newUser.getUserID();
				}else{
					uId = UsersLocalServiceUtil.findByUName(uEmail).get(0).getUserID();
				}
			}else{
				return new JSONObject().put("error", "Missing parameter Email");
			}
			
			//Adding entry mapping survey id and user id here
			UserSurveyMapping user_Survey_Map = new UserSurveyMappingImpl();
			user_Survey_Map.setPrimaryKey(new UserSurveyMappingPK(uId, surveyId));
			user_Survey_Map.setDateOfSurvey(new Date(System.currentTimeMillis()));
			
			if(UserSurveyMappingLocalServiceUtil.fetchUserSurveyMapping(new UserSurveyMappingPK(uId, surveyId))!=null){
				UserSurveyMappingLocalServiceUtil.deleteUserSurveyMapping(new UserSurveyMappingPK(uId, surveyId));
			}
			
			UserSurveyMappingLocalServiceUtil.addUserSurveyMapping(user_Survey_Map);
			//Mapping user id to option id
			Iterator iter = optionIds.iterator();
			while(iter.hasNext()){
				long optionId = (long) iter.next();
				
				Responses nextResponse = new ResponsesImpl();
				nextResponse.setOptionID(optionId);
				nextResponse.setUserID(uId);
				
				//checking if allows for text response
				long qId =QuestionOptionsLocalServiceUtil.getQuestionOptions(optionId).getQuestionID();
				if( questionWithText.get(qId)){
					//checking if user has provided a text response
					try{
						nextResponse.setResponseText(inpJson.getJSONObject("ResTexts").getString(qId+""));
					}catch(JSONException ex){
						return new JSONObject().put("error", ex.toString());
					}
				}
				ResponsesLocalServiceUtil.addResponses(nextResponse);
			}
		}catch(PortalException ex){
			return new JSONObject().put("error", "Invalid surveyId");
		}		
		return null;
	}
}