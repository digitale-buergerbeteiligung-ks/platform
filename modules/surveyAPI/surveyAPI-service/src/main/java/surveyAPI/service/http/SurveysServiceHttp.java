/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import surveyAPI.service.SurveysServiceUtil;

/**
 * Provides the HTTP utility for the
 * {@link SurveysServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SurveysServiceSoap
 * @see HttpPrincipal
 * @see SurveysServiceUtil
 * @generated
 */
@ProviderType
public class SurveysServiceHttp {
	public static org.json.JSONObject getAllQuestionIds(
		HttpPrincipal httpPrincipal) throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"getAllQuestionIds", _getAllQuestionIdsParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject getAllQuestionIds(
		HttpPrincipal httpPrincipal, long surveyID)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"getAllQuestionIds", _getAllQuestionIdsParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, surveyID);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject getSurvey(HttpPrincipal httpPrincipal,
		long surveyId) throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"getSurvey", _getSurveyParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey, surveyId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject addNewSurvey(HttpPrincipal httpPrincipal)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"addNewSurvey", _addNewSurveyParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject addNewSurvey(
		HttpPrincipal httpPrincipal, java.lang.String name)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"addNewSurvey", _addNewSurveyParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey, name);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject addNewSurvey(
		HttpPrincipal httpPrincipal, java.lang.String name,
		java.lang.String jsonInput) throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"addNewSurvey", _addNewSurveyParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey, name,
					jsonInput);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject deleteSurvey(HttpPrincipal httpPrincipal)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"deleteSurvey", _deleteSurveyParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject deleteSurvey(
		HttpPrincipal httpPrincipal, long surveyID)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(SurveysServiceUtil.class,
					"deleteSurvey", _deleteSurveyParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(methodKey, surveyID);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(SurveysServiceHttp.class);
	private static final Class<?>[] _getAllQuestionIdsParameterTypes0 = new Class[] {
			
		};
	private static final Class<?>[] _getAllQuestionIdsParameterTypes1 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getSurveyParameterTypes2 = new Class[] {
			long.class
		};
	private static final Class<?>[] _addNewSurveyParameterTypes3 = new Class[] {  };
	private static final Class<?>[] _addNewSurveyParameterTypes4 = new Class[] {
			java.lang.String.class
		};
	private static final Class<?>[] _addNewSurveyParameterTypes5 = new Class[] {
			java.lang.String.class, java.lang.String.class
		};
	private static final Class<?>[] _deleteSurveyParameterTypes6 = new Class[] {  };
	private static final Class<?>[] _deleteSurveyParameterTypes7 = new Class[] {
			long.class
		};
}