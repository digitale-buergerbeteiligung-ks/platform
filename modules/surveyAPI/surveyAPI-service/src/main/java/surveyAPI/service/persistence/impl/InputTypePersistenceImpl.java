/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchInputTypeException;

import surveyAPI.model.InputType;

import surveyAPI.model.impl.InputTypeImpl;
import surveyAPI.model.impl.InputTypeModelImpl;

import surveyAPI.service.persistence.InputTypePersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the input type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InputTypePersistence
 * @see surveyAPI.service.persistence.InputTypeUtil
 * @generated
 */
@ProviderType
public class InputTypePersistenceImpl extends BasePersistenceImpl<InputType>
	implements InputTypePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link InputTypeUtil} to access the input type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = InputTypeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
			InputTypeModelImpl.FINDER_CACHE_ENABLED, InputTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
			InputTypeModelImpl.FINDER_CACHE_ENABLED, InputTypeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
			InputTypeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public InputTypePersistenceImpl() {
		setModelClass(InputType.class);
	}

	/**
	 * Caches the input type in the entity cache if it is enabled.
	 *
	 * @param inputType the input type
	 */
	@Override
	public void cacheResult(InputType inputType) {
		entityCache.putResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
			InputTypeImpl.class, inputType.getPrimaryKey(), inputType);

		inputType.resetOriginalValues();
	}

	/**
	 * Caches the input types in the entity cache if it is enabled.
	 *
	 * @param inputTypes the input types
	 */
	@Override
	public void cacheResult(List<InputType> inputTypes) {
		for (InputType inputType : inputTypes) {
			if (entityCache.getResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
						InputTypeImpl.class, inputType.getPrimaryKey()) == null) {
				cacheResult(inputType);
			}
			else {
				inputType.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all input types.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(InputTypeImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the input type.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(InputType inputType) {
		entityCache.removeResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
			InputTypeImpl.class, inputType.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<InputType> inputTypes) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (InputType inputType : inputTypes) {
			entityCache.removeResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
				InputTypeImpl.class, inputType.getPrimaryKey());
		}
	}

	/**
	 * Creates a new input type with the primary key. Does not add the input type to the database.
	 *
	 * @param TypeID the primary key for the new input type
	 * @return the new input type
	 */
	@Override
	public InputType create(int TypeID) {
		InputType inputType = new InputTypeImpl();

		inputType.setNew(true);
		inputType.setPrimaryKey(TypeID);

		return inputType;
	}

	/**
	 * Removes the input type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param TypeID the primary key of the input type
	 * @return the input type that was removed
	 * @throws NoSuchInputTypeException if a input type with the primary key could not be found
	 */
	@Override
	public InputType remove(int TypeID) throws NoSuchInputTypeException {
		return remove((Serializable)TypeID);
	}

	/**
	 * Removes the input type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the input type
	 * @return the input type that was removed
	 * @throws NoSuchInputTypeException if a input type with the primary key could not be found
	 */
	@Override
	public InputType remove(Serializable primaryKey)
		throws NoSuchInputTypeException {
		Session session = null;

		try {
			session = openSession();

			InputType inputType = (InputType)session.get(InputTypeImpl.class,
					primaryKey);

			if (inputType == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchInputTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(inputType);
		}
		catch (NoSuchInputTypeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected InputType removeImpl(InputType inputType) {
		inputType = toUnwrappedModel(inputType);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(inputType)) {
				inputType = (InputType)session.get(InputTypeImpl.class,
						inputType.getPrimaryKeyObj());
			}

			if (inputType != null) {
				session.delete(inputType);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (inputType != null) {
			clearCache(inputType);
		}

		return inputType;
	}

	@Override
	public InputType updateImpl(InputType inputType) {
		inputType = toUnwrappedModel(inputType);

		boolean isNew = inputType.isNew();

		Session session = null;

		try {
			session = openSession();

			if (inputType.isNew()) {
				session.save(inputType);

				inputType.setNew(false);
			}
			else {
				inputType = (InputType)session.merge(inputType);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		entityCache.putResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
			InputTypeImpl.class, inputType.getPrimaryKey(), inputType, false);

		inputType.resetOriginalValues();

		return inputType;
	}

	protected InputType toUnwrappedModel(InputType inputType) {
		if (inputType instanceof InputTypeImpl) {
			return inputType;
		}

		InputTypeImpl inputTypeImpl = new InputTypeImpl();

		inputTypeImpl.setNew(inputType.isNew());
		inputTypeImpl.setPrimaryKey(inputType.getPrimaryKey());

		inputTypeImpl.setTypeID(inputType.getTypeID());
		inputTypeImpl.setTypeDescription(inputType.getTypeDescription());

		return inputTypeImpl;
	}

	/**
	 * Returns the input type with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the input type
	 * @return the input type
	 * @throws NoSuchInputTypeException if a input type with the primary key could not be found
	 */
	@Override
	public InputType findByPrimaryKey(Serializable primaryKey)
		throws NoSuchInputTypeException {
		InputType inputType = fetchByPrimaryKey(primaryKey);

		if (inputType == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchInputTypeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return inputType;
	}

	/**
	 * Returns the input type with the primary key or throws a {@link NoSuchInputTypeException} if it could not be found.
	 *
	 * @param TypeID the primary key of the input type
	 * @return the input type
	 * @throws NoSuchInputTypeException if a input type with the primary key could not be found
	 */
	@Override
	public InputType findByPrimaryKey(int TypeID)
		throws NoSuchInputTypeException {
		return findByPrimaryKey((Serializable)TypeID);
	}

	/**
	 * Returns the input type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the input type
	 * @return the input type, or <code>null</code> if a input type with the primary key could not be found
	 */
	@Override
	public InputType fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
				InputTypeImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		InputType inputType = (InputType)serializable;

		if (inputType == null) {
			Session session = null;

			try {
				session = openSession();

				inputType = (InputType)session.get(InputTypeImpl.class,
						primaryKey);

				if (inputType != null) {
					cacheResult(inputType);
				}
				else {
					entityCache.putResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
						InputTypeImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
					InputTypeImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return inputType;
	}

	/**
	 * Returns the input type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param TypeID the primary key of the input type
	 * @return the input type, or <code>null</code> if a input type with the primary key could not be found
	 */
	@Override
	public InputType fetchByPrimaryKey(int TypeID) {
		return fetchByPrimaryKey((Serializable)TypeID);
	}

	@Override
	public Map<Serializable, InputType> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, InputType> map = new HashMap<Serializable, InputType>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			InputType inputType = fetchByPrimaryKey(primaryKey);

			if (inputType != null) {
				map.put(primaryKey, inputType);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
					InputTypeImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (InputType)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_INPUTTYPE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (InputType inputType : (List<InputType>)q.list()) {
				map.put(inputType.getPrimaryKeyObj(), inputType);

				cacheResult(inputType);

				uncachedPrimaryKeys.remove(inputType.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(InputTypeModelImpl.ENTITY_CACHE_ENABLED,
					InputTypeImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the input types.
	 *
	 * @return the input types
	 */
	@Override
	public List<InputType> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the input types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of input types
	 * @param end the upper bound of the range of input types (not inclusive)
	 * @return the range of input types
	 */
	@Override
	public List<InputType> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the input types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of input types
	 * @param end the upper bound of the range of input types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of input types
	 */
	@Override
	public List<InputType> findAll(int start, int end,
		OrderByComparator<InputType> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the input types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of input types
	 * @param end the upper bound of the range of input types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of input types
	 */
	@Override
	public List<InputType> findAll(int start, int end,
		OrderByComparator<InputType> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<InputType> list = null;

		if (retrieveFromCache) {
			list = (List<InputType>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_INPUTTYPE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_INPUTTYPE;

				if (pagination) {
					sql = sql.concat(InputTypeModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<InputType>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<InputType>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the input types from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (InputType inputType : findAll()) {
			remove(inputType);
		}
	}

	/**
	 * Returns the number of input types.
	 *
	 * @return the number of input types
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_INPUTTYPE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return InputTypeModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the input type persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(InputTypeImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_INPUTTYPE = "SELECT inputType FROM InputType inputType";
	private static final String _SQL_SELECT_INPUTTYPE_WHERE_PKS_IN = "SELECT inputType FROM InputType inputType WHERE TypeID IN (";
	private static final String _SQL_COUNT_INPUTTYPE = "SELECT COUNT(inputType) FROM InputType inputType";
	private static final String _ORDER_BY_ENTITY_ALIAS = "inputType.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No InputType exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(InputTypePersistenceImpl.class);
}