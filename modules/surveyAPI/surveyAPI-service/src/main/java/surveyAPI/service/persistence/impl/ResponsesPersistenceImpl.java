/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchResponsesException;

import surveyAPI.model.Responses;

import surveyAPI.model.impl.ResponsesImpl;
import surveyAPI.model.impl.ResponsesModelImpl;

import surveyAPI.service.persistence.ResponsesPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the responses service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResponsesPersistence
 * @see surveyAPI.service.persistence.ResponsesUtil
 * @generated
 */
@ProviderType
public class ResponsesPersistenceImpl extends BasePersistenceImpl<Responses>
	implements ResponsesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ResponsesUtil} to access the responses persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ResponsesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesModelImpl.FINDER_CACHE_ENABLED, ResponsesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesModelImpl.FINDER_CACHE_ENABLED, ResponsesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UID = new FinderPath(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesModelImpl.FINDER_CACHE_ENABLED, ResponsesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UID = new FinderPath(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesModelImpl.FINDER_CACHE_ENABLED, ResponsesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUID",
			new String[] { Long.class.getName() },
			ResponsesModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UID = new FinderPath(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the responseses where UserID = &#63;.
	 *
	 * @param UserID the user ID
	 * @return the matching responseses
	 */
	@Override
	public List<Responses> findByUID(long UserID) {
		return findByUID(UserID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the responseses where UserID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param UserID the user ID
	 * @param start the lower bound of the range of responseses
	 * @param end the upper bound of the range of responseses (not inclusive)
	 * @return the range of matching responseses
	 */
	@Override
	public List<Responses> findByUID(long UserID, int start, int end) {
		return findByUID(UserID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the responseses where UserID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param UserID the user ID
	 * @param start the lower bound of the range of responseses
	 * @param end the upper bound of the range of responseses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching responseses
	 */
	@Override
	public List<Responses> findByUID(long UserID, int start, int end,
		OrderByComparator<Responses> orderByComparator) {
		return findByUID(UserID, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the responseses where UserID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param UserID the user ID
	 * @param start the lower bound of the range of responseses
	 * @param end the upper bound of the range of responseses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching responseses
	 */
	@Override
	public List<Responses> findByUID(long UserID, int start, int end,
		OrderByComparator<Responses> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UID;
			finderArgs = new Object[] { UserID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UID;
			finderArgs = new Object[] { UserID, start, end, orderByComparator };
		}

		List<Responses> list = null;

		if (retrieveFromCache) {
			list = (List<Responses>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Responses responses : list) {
					if ((UserID != responses.getUserID())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_RESPONSES_WHERE);

			query.append(_FINDER_COLUMN_UID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ResponsesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(UserID);

				if (!pagination) {
					list = (List<Responses>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Responses>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first responses in the ordered set where UserID = &#63;.
	 *
	 * @param UserID the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching responses
	 * @throws NoSuchResponsesException if a matching responses could not be found
	 */
	@Override
	public Responses findByUID_First(long UserID,
		OrderByComparator<Responses> orderByComparator)
		throws NoSuchResponsesException {
		Responses responses = fetchByUID_First(UserID, orderByComparator);

		if (responses != null) {
			return responses;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("UserID=");
		msg.append(UserID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchResponsesException(msg.toString());
	}

	/**
	 * Returns the first responses in the ordered set where UserID = &#63;.
	 *
	 * @param UserID the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching responses, or <code>null</code> if a matching responses could not be found
	 */
	@Override
	public Responses fetchByUID_First(long UserID,
		OrderByComparator<Responses> orderByComparator) {
		List<Responses> list = findByUID(UserID, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last responses in the ordered set where UserID = &#63;.
	 *
	 * @param UserID the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching responses
	 * @throws NoSuchResponsesException if a matching responses could not be found
	 */
	@Override
	public Responses findByUID_Last(long UserID,
		OrderByComparator<Responses> orderByComparator)
		throws NoSuchResponsesException {
		Responses responses = fetchByUID_Last(UserID, orderByComparator);

		if (responses != null) {
			return responses;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("UserID=");
		msg.append(UserID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchResponsesException(msg.toString());
	}

	/**
	 * Returns the last responses in the ordered set where UserID = &#63;.
	 *
	 * @param UserID the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching responses, or <code>null</code> if a matching responses could not be found
	 */
	@Override
	public Responses fetchByUID_Last(long UserID,
		OrderByComparator<Responses> orderByComparator) {
		int count = countByUID(UserID);

		if (count == 0) {
			return null;
		}

		List<Responses> list = findByUID(UserID, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the responseses before and after the current responses in the ordered set where UserID = &#63;.
	 *
	 * @param ResponseID the primary key of the current responses
	 * @param UserID the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next responses
	 * @throws NoSuchResponsesException if a responses with the primary key could not be found
	 */
	@Override
	public Responses[] findByUID_PrevAndNext(long ResponseID, long UserID,
		OrderByComparator<Responses> orderByComparator)
		throws NoSuchResponsesException {
		Responses responses = findByPrimaryKey(ResponseID);

		Session session = null;

		try {
			session = openSession();

			Responses[] array = new ResponsesImpl[3];

			array[0] = getByUID_PrevAndNext(session, responses, UserID,
					orderByComparator, true);

			array[1] = responses;

			array[2] = getByUID_PrevAndNext(session, responses, UserID,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Responses getByUID_PrevAndNext(Session session,
		Responses responses, long UserID,
		OrderByComparator<Responses> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_RESPONSES_WHERE);

		query.append(_FINDER_COLUMN_UID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ResponsesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(UserID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(responses);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Responses> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the responseses where UserID = &#63; from the database.
	 *
	 * @param UserID the user ID
	 */
	@Override
	public void removeByUID(long UserID) {
		for (Responses responses : findByUID(UserID, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(responses);
		}
	}

	/**
	 * Returns the number of responseses where UserID = &#63;.
	 *
	 * @param UserID the user ID
	 * @return the number of matching responseses
	 */
	@Override
	public int countByUID(long UserID) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UID;

		Object[] finderArgs = new Object[] { UserID };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_RESPONSES_WHERE);

			query.append(_FINDER_COLUMN_UID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(UserID);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UID_USERID_2 = "responses.UserID = ?";

	public ResponsesPersistenceImpl() {
		setModelClass(Responses.class);
	}

	/**
	 * Caches the responses in the entity cache if it is enabled.
	 *
	 * @param responses the responses
	 */
	@Override
	public void cacheResult(Responses responses) {
		entityCache.putResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesImpl.class, responses.getPrimaryKey(), responses);

		responses.resetOriginalValues();
	}

	/**
	 * Caches the responseses in the entity cache if it is enabled.
	 *
	 * @param responseses the responseses
	 */
	@Override
	public void cacheResult(List<Responses> responseses) {
		for (Responses responses : responseses) {
			if (entityCache.getResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
						ResponsesImpl.class, responses.getPrimaryKey()) == null) {
				cacheResult(responses);
			}
			else {
				responses.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all responseses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ResponsesImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the responses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Responses responses) {
		entityCache.removeResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesImpl.class, responses.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Responses> responseses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Responses responses : responseses) {
			entityCache.removeResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
				ResponsesImpl.class, responses.getPrimaryKey());
		}
	}

	/**
	 * Creates a new responses with the primary key. Does not add the responses to the database.
	 *
	 * @param ResponseID the primary key for the new responses
	 * @return the new responses
	 */
	@Override
	public Responses create(long ResponseID) {
		Responses responses = new ResponsesImpl();

		responses.setNew(true);
		responses.setPrimaryKey(ResponseID);

		return responses;
	}

	/**
	 * Removes the responses with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ResponseID the primary key of the responses
	 * @return the responses that was removed
	 * @throws NoSuchResponsesException if a responses with the primary key could not be found
	 */
	@Override
	public Responses remove(long ResponseID) throws NoSuchResponsesException {
		return remove((Serializable)ResponseID);
	}

	/**
	 * Removes the responses with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the responses
	 * @return the responses that was removed
	 * @throws NoSuchResponsesException if a responses with the primary key could not be found
	 */
	@Override
	public Responses remove(Serializable primaryKey)
		throws NoSuchResponsesException {
		Session session = null;

		try {
			session = openSession();

			Responses responses = (Responses)session.get(ResponsesImpl.class,
					primaryKey);

			if (responses == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchResponsesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(responses);
		}
		catch (NoSuchResponsesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Responses removeImpl(Responses responses) {
		responses = toUnwrappedModel(responses);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(responses)) {
				responses = (Responses)session.get(ResponsesImpl.class,
						responses.getPrimaryKeyObj());
			}

			if (responses != null) {
				session.delete(responses);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (responses != null) {
			clearCache(responses);
		}

		return responses;
	}

	@Override
	public Responses updateImpl(Responses responses) {
		responses = toUnwrappedModel(responses);

		boolean isNew = responses.isNew();

		ResponsesModelImpl responsesModelImpl = (ResponsesModelImpl)responses;

		Session session = null;

		try {
			session = openSession();

			if (responses.isNew()) {
				session.save(responses);

				responses.setNew(false);
			}
			else {
				responses = (Responses)session.merge(responses);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ResponsesModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { responsesModelImpl.getUserID() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((responsesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						responsesModelImpl.getOriginalUserID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UID,
					args);

				args = new Object[] { responsesModelImpl.getUserID() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UID,
					args);
			}
		}

		entityCache.putResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
			ResponsesImpl.class, responses.getPrimaryKey(), responses, false);

		responses.resetOriginalValues();

		return responses;
	}

	protected Responses toUnwrappedModel(Responses responses) {
		if (responses instanceof ResponsesImpl) {
			return responses;
		}

		ResponsesImpl responsesImpl = new ResponsesImpl();

		responsesImpl.setNew(responses.isNew());
		responsesImpl.setPrimaryKey(responses.getPrimaryKey());

		responsesImpl.setResponseID(responses.getResponseID());
		responsesImpl.setUserID(responses.getUserID());
		responsesImpl.setOptionID(responses.getOptionID());
		responsesImpl.setResponseText(responses.getResponseText());

		return responsesImpl;
	}

	/**
	 * Returns the responses with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the responses
	 * @return the responses
	 * @throws NoSuchResponsesException if a responses with the primary key could not be found
	 */
	@Override
	public Responses findByPrimaryKey(Serializable primaryKey)
		throws NoSuchResponsesException {
		Responses responses = fetchByPrimaryKey(primaryKey);

		if (responses == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchResponsesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return responses;
	}

	/**
	 * Returns the responses with the primary key or throws a {@link NoSuchResponsesException} if it could not be found.
	 *
	 * @param ResponseID the primary key of the responses
	 * @return the responses
	 * @throws NoSuchResponsesException if a responses with the primary key could not be found
	 */
	@Override
	public Responses findByPrimaryKey(long ResponseID)
		throws NoSuchResponsesException {
		return findByPrimaryKey((Serializable)ResponseID);
	}

	/**
	 * Returns the responses with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the responses
	 * @return the responses, or <code>null</code> if a responses with the primary key could not be found
	 */
	@Override
	public Responses fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
				ResponsesImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Responses responses = (Responses)serializable;

		if (responses == null) {
			Session session = null;

			try {
				session = openSession();

				responses = (Responses)session.get(ResponsesImpl.class,
						primaryKey);

				if (responses != null) {
					cacheResult(responses);
				}
				else {
					entityCache.putResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
						ResponsesImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
					ResponsesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return responses;
	}

	/**
	 * Returns the responses with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ResponseID the primary key of the responses
	 * @return the responses, or <code>null</code> if a responses with the primary key could not be found
	 */
	@Override
	public Responses fetchByPrimaryKey(long ResponseID) {
		return fetchByPrimaryKey((Serializable)ResponseID);
	}

	@Override
	public Map<Serializable, Responses> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Responses> map = new HashMap<Serializable, Responses>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Responses responses = fetchByPrimaryKey(primaryKey);

			if (responses != null) {
				map.put(primaryKey, responses);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
					ResponsesImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Responses)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_RESPONSES_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Responses responses : (List<Responses>)q.list()) {
				map.put(responses.getPrimaryKeyObj(), responses);

				cacheResult(responses);

				uncachedPrimaryKeys.remove(responses.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ResponsesModelImpl.ENTITY_CACHE_ENABLED,
					ResponsesImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the responseses.
	 *
	 * @return the responseses
	 */
	@Override
	public List<Responses> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the responseses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of responseses
	 * @param end the upper bound of the range of responseses (not inclusive)
	 * @return the range of responseses
	 */
	@Override
	public List<Responses> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the responseses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of responseses
	 * @param end the upper bound of the range of responseses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of responseses
	 */
	@Override
	public List<Responses> findAll(int start, int end,
		OrderByComparator<Responses> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the responseses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of responseses
	 * @param end the upper bound of the range of responseses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of responseses
	 */
	@Override
	public List<Responses> findAll(int start, int end,
		OrderByComparator<Responses> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Responses> list = null;

		if (retrieveFromCache) {
			list = (List<Responses>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_RESPONSES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_RESPONSES;

				if (pagination) {
					sql = sql.concat(ResponsesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Responses>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Responses>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the responseses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Responses responses : findAll()) {
			remove(responses);
		}
	}

	/**
	 * Returns the number of responseses.
	 *
	 * @return the number of responseses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_RESPONSES);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ResponsesModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the responses persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ResponsesImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_RESPONSES = "SELECT responses FROM Responses responses";
	private static final String _SQL_SELECT_RESPONSES_WHERE_PKS_IN = "SELECT responses FROM Responses responses WHERE ResponseID IN (";
	private static final String _SQL_SELECT_RESPONSES_WHERE = "SELECT responses FROM Responses responses WHERE ";
	private static final String _SQL_COUNT_RESPONSES = "SELECT COUNT(responses) FROM Responses responses";
	private static final String _SQL_COUNT_RESPONSES_WHERE = "SELECT COUNT(responses) FROM Responses responses WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "responses.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Responses exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Responses exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ResponsesPersistenceImpl.class);
}