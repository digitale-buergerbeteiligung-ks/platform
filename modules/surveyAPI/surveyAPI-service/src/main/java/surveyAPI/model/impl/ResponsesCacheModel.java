/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import surveyAPI.model.Responses;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Responses in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Responses
 * @generated
 */
@ProviderType
public class ResponsesCacheModel implements CacheModel<Responses>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ResponsesCacheModel)) {
			return false;
		}

		ResponsesCacheModel responsesCacheModel = (ResponsesCacheModel)obj;

		if (ResponseID == responsesCacheModel.ResponseID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ResponseID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{ResponseID=");
		sb.append(ResponseID);
		sb.append(", UserID=");
		sb.append(UserID);
		sb.append(", OptionID=");
		sb.append(OptionID);
		sb.append(", ResponseText=");
		sb.append(ResponseText);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Responses toEntityModel() {
		ResponsesImpl responsesImpl = new ResponsesImpl();

		responsesImpl.setResponseID(ResponseID);
		responsesImpl.setUserID(UserID);
		responsesImpl.setOptionID(OptionID);

		if (ResponseText == null) {
			responsesImpl.setResponseText(StringPool.BLANK);
		}
		else {
			responsesImpl.setResponseText(ResponseText);
		}

		responsesImpl.resetOriginalValues();

		return responsesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ResponseID = objectInput.readLong();

		UserID = objectInput.readLong();

		OptionID = objectInput.readLong();
		ResponseText = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ResponseID);

		objectOutput.writeLong(UserID);

		objectOutput.writeLong(OptionID);

		if (ResponseText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ResponseText);
		}
	}

	public long ResponseID;
	public long UserID;
	public long OptionID;
	public String ResponseText;
}