/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import surveyAPI.model.UserSurveyMapping;

import surveyAPI.service.persistence.UserSurveyMappingPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserSurveyMapping in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see UserSurveyMapping
 * @generated
 */
@ProviderType
public class UserSurveyMappingCacheModel implements CacheModel<UserSurveyMapping>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserSurveyMappingCacheModel)) {
			return false;
		}

		UserSurveyMappingCacheModel userSurveyMappingCacheModel = (UserSurveyMappingCacheModel)obj;

		if (userSurveyMappingPK.equals(
					userSurveyMappingCacheModel.userSurveyMappingPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userSurveyMappingPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{UserID=");
		sb.append(UserID);
		sb.append(", SurveyID=");
		sb.append(SurveyID);
		sb.append(", DateOfSurvey=");
		sb.append(DateOfSurvey);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserSurveyMapping toEntityModel() {
		UserSurveyMappingImpl userSurveyMappingImpl = new UserSurveyMappingImpl();

		userSurveyMappingImpl.setUserID(UserID);
		userSurveyMappingImpl.setSurveyID(SurveyID);

		if (DateOfSurvey == Long.MIN_VALUE) {
			userSurveyMappingImpl.setDateOfSurvey(null);
		}
		else {
			userSurveyMappingImpl.setDateOfSurvey(new Date(DateOfSurvey));
		}

		userSurveyMappingImpl.resetOriginalValues();

		return userSurveyMappingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		UserID = objectInput.readLong();

		SurveyID = objectInput.readLong();
		DateOfSurvey = objectInput.readLong();

		userSurveyMappingPK = new UserSurveyMappingPK(UserID, SurveyID);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(UserID);

		objectOutput.writeLong(SurveyID);
		objectOutput.writeLong(DateOfSurvey);
	}

	public long UserID;
	public long SurveyID;
	public long DateOfSurvey;
	public transient UserSurveyMappingPK userSurveyMappingPK;
}