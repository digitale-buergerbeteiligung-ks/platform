/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import surveyAPI.model.Questions;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Questions in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Questions
 * @generated
 */
@ProviderType
public class QuestionsCacheModel implements CacheModel<Questions>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuestionsCacheModel)) {
			return false;
		}

		QuestionsCacheModel questionsCacheModel = (QuestionsCacheModel)obj;

		if (QuestionID == questionsCacheModel.QuestionID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, QuestionID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{QuestionID=");
		sb.append(QuestionID);
		sb.append(", ResponseType=");
		sb.append(ResponseType);
		sb.append(", QuestionName=");
		sb.append(QuestionName);
		sb.append(", QuestionText=");
		sb.append(QuestionText);
		sb.append(", DateOfCreation=");
		sb.append(DateOfCreation);
		sb.append(", Required=");
		sb.append(Required);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Questions toEntityModel() {
		QuestionsImpl questionsImpl = new QuestionsImpl();

		questionsImpl.setQuestionID(QuestionID);

		if (ResponseType == null) {
			questionsImpl.setResponseType(StringPool.BLANK);
		}
		else {
			questionsImpl.setResponseType(ResponseType);
		}

		if (QuestionName == null) {
			questionsImpl.setQuestionName(StringPool.BLANK);
		}
		else {
			questionsImpl.setQuestionName(QuestionName);
		}

		if (QuestionText == null) {
			questionsImpl.setQuestionText(StringPool.BLANK);
		}
		else {
			questionsImpl.setQuestionText(QuestionText);
		}

		if (DateOfCreation == Long.MIN_VALUE) {
			questionsImpl.setDateOfCreation(null);
		}
		else {
			questionsImpl.setDateOfCreation(new Date(DateOfCreation));
		}

		questionsImpl.setRequired(Required);

		questionsImpl.resetOriginalValues();

		return questionsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		QuestionID = objectInput.readLong();
		ResponseType = objectInput.readUTF();
		QuestionName = objectInput.readUTF();
		QuestionText = objectInput.readUTF();
		DateOfCreation = objectInput.readLong();

		Required = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(QuestionID);

		if (ResponseType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ResponseType);
		}

		if (QuestionName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(QuestionName);
		}

		if (QuestionText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(QuestionText);
		}

		objectOutput.writeLong(DateOfCreation);

		objectOutput.writeBoolean(Required);
	}

	public long QuestionID;
	public String ResponseType;
	public String QuestionName;
	public String QuestionText;
	public long DateOfCreation;
	public boolean Required;
}