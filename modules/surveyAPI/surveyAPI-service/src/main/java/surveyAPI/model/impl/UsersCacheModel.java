/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import surveyAPI.model.Users;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Users in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Users
 * @generated
 */
@ProviderType
public class UsersCacheModel implements CacheModel<Users>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UsersCacheModel)) {
			return false;
		}

		UsersCacheModel usersCacheModel = (UsersCacheModel)obj;

		if (UserID == usersCacheModel.UserID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, UserID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{UserID=");
		sb.append(UserID);
		sb.append(", UserName=");
		sb.append(UserName);
		sb.append(", Email=");
		sb.append(Email);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Users toEntityModel() {
		UsersImpl usersImpl = new UsersImpl();

		usersImpl.setUserID(UserID);

		if (UserName == null) {
			usersImpl.setUserName(StringPool.BLANK);
		}
		else {
			usersImpl.setUserName(UserName);
		}

		if (Email == null) {
			usersImpl.setEmail(StringPool.BLANK);
		}
		else {
			usersImpl.setEmail(Email);
		}

		usersImpl.resetOriginalValues();

		return usersImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		UserID = objectInput.readLong();
		UserName = objectInput.readUTF();
		Email = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(UserID);

		if (UserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(UserName);
		}

		if (Email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Email);
		}
	}

	public long UserID;
	public String UserName;
	public String Email;
}