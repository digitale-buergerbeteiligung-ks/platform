/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import surveyAPI.model.InputType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing InputType in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see InputType
 * @generated
 */
@ProviderType
public class InputTypeCacheModel implements CacheModel<InputType>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InputTypeCacheModel)) {
			return false;
		}

		InputTypeCacheModel inputTypeCacheModel = (InputTypeCacheModel)obj;

		if (TypeID == inputTypeCacheModel.TypeID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, TypeID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{TypeID=");
		sb.append(TypeID);
		sb.append(", TypeDescription=");
		sb.append(TypeDescription);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public InputType toEntityModel() {
		InputTypeImpl inputTypeImpl = new InputTypeImpl();

		inputTypeImpl.setTypeID(TypeID);

		if (TypeDescription == null) {
			inputTypeImpl.setTypeDescription(StringPool.BLANK);
		}
		else {
			inputTypeImpl.setTypeDescription(TypeDescription);
		}

		inputTypeImpl.resetOriginalValues();

		return inputTypeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		TypeID = objectInput.readInt();
		TypeDescription = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(TypeID);

		if (TypeDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(TypeDescription);
		}
	}

	public int TypeID;
	public String TypeDescription;
}