/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import surveyAPI.model.QuestionOptions;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing QuestionOptions in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see QuestionOptions
 * @generated
 */
@ProviderType
public class QuestionOptionsCacheModel implements CacheModel<QuestionOptions>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuestionOptionsCacheModel)) {
			return false;
		}

		QuestionOptionsCacheModel questionOptionsCacheModel = (QuestionOptionsCacheModel)obj;

		if (OptionID == questionOptionsCacheModel.OptionID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, OptionID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{OptionID=");
		sb.append(OptionID);
		sb.append(", QuestionID=");
		sb.append(QuestionID);
		sb.append(", ChoiceID=");
		sb.append(ChoiceID);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public QuestionOptions toEntityModel() {
		QuestionOptionsImpl questionOptionsImpl = new QuestionOptionsImpl();

		questionOptionsImpl.setOptionID(OptionID);
		questionOptionsImpl.setQuestionID(QuestionID);
		questionOptionsImpl.setChoiceID(ChoiceID);

		questionOptionsImpl.resetOriginalValues();

		return questionOptionsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		OptionID = objectInput.readLong();

		QuestionID = objectInput.readLong();

		ChoiceID = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(OptionID);

		objectOutput.writeLong(QuestionID);

		objectOutput.writeLong(ChoiceID);
	}

	public long OptionID;
	public long QuestionID;
	public long ChoiceID;
}