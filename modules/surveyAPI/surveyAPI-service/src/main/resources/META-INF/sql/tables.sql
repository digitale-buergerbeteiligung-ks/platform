create table SURVEY_Choices (
	ChoiceID LONG not null primary key,
	ChoiceText VARCHAR(75) null
);

create table SURVEY_InputType (
	TypeID INTEGER not null primary key,
	TypeDescription VARCHAR(75) null
);

create table SURVEY_QuestionOptions (
	OptionID LONG not null primary key,
	QuestionID LONG,
	ChoiceID LONG
);

create table SURVEY_Questions (
	QuestionID LONG not null primary key,
	ResponseType VARCHAR(75) null,
	QuestionName VARCHAR(75) null,
	QuestionText VARCHAR(75) null,
	DateOfCreation DATE null,
	Required BOOLEAN
);

create table SURVEY_Responses (
	ResponseID LONG not null primary key,
	UserID LONG,
	OptionID LONG,
	ResponseText VARCHAR(75) null
);

create table SURVEY_Surveys (
	SurveyID LONG not null primary key,
	SurveyName VARCHAR(75) null,
	DateOfCreation DATE null,
	QuestionIds VARCHAR(75) null
);

create table SURVEY_UserSurveyMapping (
	UserID LONG not null,
	SurveyID LONG not null,
	DateOfSurvey DATE null,
	primary key (UserID, SurveyID)
);

create table SURVEY_Users (
	UserID LONG not null primary key,
	UserName VARCHAR(75) null,
	Email VARCHAR(75) null
);