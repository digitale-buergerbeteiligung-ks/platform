create index IX_DBA72075 on SURVEY_Choices (ChoiceText[$COLUMN_LENGTH:75$]);

create index IX_CA8A3329 on SURVEY_QuestionOptions (ChoiceID);
create index IX_42649D3E on SURVEY_QuestionOptions (QuestionID, ChoiceID);

create index IX_D121974D on SURVEY_Responses (UserID);

create index IX_DA5F3713 on SURVEY_Users (UserName[$COLUMN_LENGTH:75$]);