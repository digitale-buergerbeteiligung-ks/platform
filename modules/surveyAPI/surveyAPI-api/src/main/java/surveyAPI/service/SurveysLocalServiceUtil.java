/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Surveys. This utility wraps
 * {@link surveyAPI.service.impl.SurveysLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see SurveysLocalService
 * @see surveyAPI.service.base.SurveysLocalServiceBaseImpl
 * @see surveyAPI.service.impl.SurveysLocalServiceImpl
 * @generated
 */
@ProviderType
public class SurveysLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link surveyAPI.service.impl.SurveysLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of surveyses.
	*
	* @return the number of surveyses
	*/
	public static int getSurveysesCount() {
		return getService().getSurveysesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the surveyses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of surveyses
	* @param end the upper bound of the range of surveyses (not inclusive)
	* @return the range of surveyses
	*/
	public static java.util.List<surveyAPI.model.Surveys> getSurveyses(
		int start, int end) {
		return getService().getSurveyses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static org.json.JSONObject addNewSurvey(java.lang.String name)
		throws org.json.JSONException {
		return getService().addNewSurvey(name);
	}

	public static org.json.JSONObject addNewSurvey(java.lang.String name,
		java.lang.String jsonInput) throws org.json.JSONException {
		return getService().addNewSurvey(name, jsonInput);
	}

	public static org.json.JSONObject deleteSurvey(long surveyID)
		throws org.json.JSONException {
		return getService().deleteSurvey(surveyID);
	}

	public static org.json.JSONObject getAllQuestionIds(long surveyID)
		throws org.json.JSONException {
		return getService().getAllQuestionIds(surveyID);
	}

	public static org.json.JSONObject getSurvey(long surveyId)
		throws org.json.JSONException {
		return getService().getSurvey(surveyId);
	}

	/**
	* Adds the surveys to the database. Also notifies the appropriate model listeners.
	*
	* @param surveys the surveys
	* @return the surveys that was added
	*/
	public static surveyAPI.model.Surveys addSurveys(
		surveyAPI.model.Surveys surveys) {
		return getService().addSurveys(surveys);
	}

	/**
	* Creates a new surveys with the primary key. Does not add the surveys to the database.
	*
	* @param SurveyID the primary key for the new surveys
	* @return the new surveys
	*/
	public static surveyAPI.model.Surveys createSurveys(long SurveyID) {
		return getService().createSurveys(SurveyID);
	}

	/**
	* Deletes the surveys with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys that was removed
	* @throws PortalException if a surveys with the primary key could not be found
	*/
	public static surveyAPI.model.Surveys deleteSurveys(long SurveyID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteSurveys(SurveyID);
	}

	/**
	* Deletes the surveys from the database. Also notifies the appropriate model listeners.
	*
	* @param surveys the surveys
	* @return the surveys that was removed
	*/
	public static surveyAPI.model.Surveys deleteSurveys(
		surveyAPI.model.Surveys surveys) {
		return getService().deleteSurveys(surveys);
	}

	public static surveyAPI.model.Surveys fetchSurveys(long SurveyID) {
		return getService().fetchSurveys(SurveyID);
	}

	/**
	* Returns the surveys with the primary key.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys
	* @throws PortalException if a surveys with the primary key could not be found
	*/
	public static surveyAPI.model.Surveys getSurveys(long SurveyID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getSurveys(SurveyID);
	}

	/**
	* Updates the surveys in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param surveys the surveys
	* @return the surveys that was updated
	*/
	public static surveyAPI.model.Surveys updateSurveys(
		surveyAPI.model.Surveys surveys) {
		return getService().updateSurveys(surveys);
	}

	public static SurveysLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SurveysLocalService, SurveysLocalService> _serviceTracker =
		ServiceTrackerFactory.open(SurveysLocalService.class);
}