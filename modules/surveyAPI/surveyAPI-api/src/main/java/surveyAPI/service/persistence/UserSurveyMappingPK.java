/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class UserSurveyMappingPK implements Comparable<UserSurveyMappingPK>,
	Serializable {
	public long UserID;
	public long SurveyID;

	public UserSurveyMappingPK() {
	}

	public UserSurveyMappingPK(long UserID, long SurveyID) {
		this.UserID = UserID;
		this.SurveyID = SurveyID;
	}

	public long getUserID() {
		return UserID;
	}

	public void setUserID(long UserID) {
		this.UserID = UserID;
	}

	public long getSurveyID() {
		return SurveyID;
	}

	public void setSurveyID(long SurveyID) {
		this.SurveyID = SurveyID;
	}

	@Override
	public int compareTo(UserSurveyMappingPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (UserID < pk.UserID) {
			value = -1;
		}
		else if (UserID > pk.UserID) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (SurveyID < pk.SurveyID) {
			value = -1;
		}
		else if (SurveyID > pk.SurveyID) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserSurveyMappingPK)) {
			return false;
		}

		UserSurveyMappingPK pk = (UserSurveyMappingPK)obj;

		if ((UserID == pk.UserID) && (SurveyID == pk.SurveyID)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, UserID);
		hashCode = HashUtil.hash(hashCode, SurveyID);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("UserID");
		sb.append(StringPool.EQUAL);
		sb.append(UserID);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("SurveyID");
		sb.append(StringPool.EQUAL);
		sb.append(SurveyID);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}