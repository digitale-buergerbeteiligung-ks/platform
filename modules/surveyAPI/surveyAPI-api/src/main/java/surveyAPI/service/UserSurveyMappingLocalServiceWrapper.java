/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserSurveyMappingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserSurveyMappingLocalService
 * @generated
 */
@ProviderType
public class UserSurveyMappingLocalServiceWrapper
	implements UserSurveyMappingLocalService,
		ServiceWrapper<UserSurveyMappingLocalService> {
	public UserSurveyMappingLocalServiceWrapper(
		UserSurveyMappingLocalService userSurveyMappingLocalService) {
		_userSurveyMappingLocalService = userSurveyMappingLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _userSurveyMappingLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userSurveyMappingLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _userSurveyMappingLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userSurveyMappingLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userSurveyMappingLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of user survey mappings.
	*
	* @return the number of user survey mappings
	*/
	@Override
	public int getUserSurveyMappingsCount() {
		return _userSurveyMappingLocalService.getUserSurveyMappingsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _userSurveyMappingLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userSurveyMappingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _userSurveyMappingLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _userSurveyMappingLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns a range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @return the range of user survey mappings
	*/
	@Override
	public java.util.List<surveyAPI.model.UserSurveyMapping> getUserSurveyMappings(
		int start, int end) {
		return _userSurveyMappingLocalService.getUserSurveyMappings(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userSurveyMappingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _userSurveyMappingLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Adds the user survey mapping to the database. Also notifies the appropriate model listeners.
	*
	* @param userSurveyMapping the user survey mapping
	* @return the user survey mapping that was added
	*/
	@Override
	public surveyAPI.model.UserSurveyMapping addUserSurveyMapping(
		surveyAPI.model.UserSurveyMapping userSurveyMapping) {
		return _userSurveyMappingLocalService.addUserSurveyMapping(userSurveyMapping);
	}

	/**
	* Creates a new user survey mapping with the primary key. Does not add the user survey mapping to the database.
	*
	* @param userSurveyMappingPK the primary key for the new user survey mapping
	* @return the new user survey mapping
	*/
	@Override
	public surveyAPI.model.UserSurveyMapping createUserSurveyMapping(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK) {
		return _userSurveyMappingLocalService.createUserSurveyMapping(userSurveyMappingPK);
	}

	/**
	* Deletes the user survey mapping from the database. Also notifies the appropriate model listeners.
	*
	* @param userSurveyMapping the user survey mapping
	* @return the user survey mapping that was removed
	*/
	@Override
	public surveyAPI.model.UserSurveyMapping deleteUserSurveyMapping(
		surveyAPI.model.UserSurveyMapping userSurveyMapping) {
		return _userSurveyMappingLocalService.deleteUserSurveyMapping(userSurveyMapping);
	}

	/**
	* Deletes the user survey mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping that was removed
	* @throws PortalException if a user survey mapping with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.UserSurveyMapping deleteUserSurveyMapping(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userSurveyMappingLocalService.deleteUserSurveyMapping(userSurveyMappingPK);
	}

	@Override
	public surveyAPI.model.UserSurveyMapping fetchUserSurveyMapping(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK) {
		return _userSurveyMappingLocalService.fetchUserSurveyMapping(userSurveyMappingPK);
	}

	/**
	* Returns the user survey mapping with the primary key.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping
	* @throws PortalException if a user survey mapping with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.UserSurveyMapping getUserSurveyMapping(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userSurveyMappingLocalService.getUserSurveyMapping(userSurveyMappingPK);
	}

	/**
	* Updates the user survey mapping in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userSurveyMapping the user survey mapping
	* @return the user survey mapping that was updated
	*/
	@Override
	public surveyAPI.model.UserSurveyMapping updateUserSurveyMapping(
		surveyAPI.model.UserSurveyMapping userSurveyMapping) {
		return _userSurveyMappingLocalService.updateUserSurveyMapping(userSurveyMapping);
	}

	@Override
	public UserSurveyMappingLocalService getWrappedService() {
		return _userSurveyMappingLocalService;
	}

	@Override
	public void setWrappedService(
		UserSurveyMappingLocalService userSurveyMappingLocalService) {
		_userSurveyMappingLocalService = userSurveyMappingLocalService;
	}

	private UserSurveyMappingLocalService _userSurveyMappingLocalService;
}