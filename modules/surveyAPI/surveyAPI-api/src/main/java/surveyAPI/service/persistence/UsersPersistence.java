/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchUsersException;

import surveyAPI.model.Users;

/**
 * The persistence interface for the users service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.UsersPersistenceImpl
 * @see UsersUtil
 * @generated
 */
@ProviderType
public interface UsersPersistence extends BasePersistence<Users> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UsersUtil} to access the users persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the userses where UserName = &#63;.
	*
	* @param UserName the user name
	* @return the matching userses
	*/
	public java.util.List<Users> findByUName(java.lang.String UserName);

	/**
	* Returns a range of all the userses where UserName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserName the user name
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @return the range of matching userses
	*/
	public java.util.List<Users> findByUName(java.lang.String UserName,
		int start, int end);

	/**
	* Returns an ordered range of all the userses where UserName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserName the user name
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching userses
	*/
	public java.util.List<Users> findByUName(java.lang.String UserName,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator);

	/**
	* Returns an ordered range of all the userses where UserName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserName the user name
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching userses
	*/
	public java.util.List<Users> findByUName(java.lang.String UserName,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching users
	* @throws NoSuchUsersException if a matching users could not be found
	*/
	public Users findByUName_First(java.lang.String UserName,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator)
		throws NoSuchUsersException;

	/**
	* Returns the first users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching users, or <code>null</code> if a matching users could not be found
	*/
	public Users fetchByUName_First(java.lang.String UserName,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator);

	/**
	* Returns the last users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching users
	* @throws NoSuchUsersException if a matching users could not be found
	*/
	public Users findByUName_Last(java.lang.String UserName,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator)
		throws NoSuchUsersException;

	/**
	* Returns the last users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching users, or <code>null</code> if a matching users could not be found
	*/
	public Users fetchByUName_Last(java.lang.String UserName,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator);

	/**
	* Returns the userses before and after the current users in the ordered set where UserName = &#63;.
	*
	* @param UserID the primary key of the current users
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next users
	* @throws NoSuchUsersException if a users with the primary key could not be found
	*/
	public Users[] findByUName_PrevAndNext(long UserID,
		java.lang.String UserName,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator)
		throws NoSuchUsersException;

	/**
	* Removes all the userses where UserName = &#63; from the database.
	*
	* @param UserName the user name
	*/
	public void removeByUName(java.lang.String UserName);

	/**
	* Returns the number of userses where UserName = &#63;.
	*
	* @param UserName the user name
	* @return the number of matching userses
	*/
	public int countByUName(java.lang.String UserName);

	/**
	* Caches the users in the entity cache if it is enabled.
	*
	* @param users the users
	*/
	public void cacheResult(Users users);

	/**
	* Caches the userses in the entity cache if it is enabled.
	*
	* @param userses the userses
	*/
	public void cacheResult(java.util.List<Users> userses);

	/**
	* Creates a new users with the primary key. Does not add the users to the database.
	*
	* @param UserID the primary key for the new users
	* @return the new users
	*/
	public Users create(long UserID);

	/**
	* Removes the users with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param UserID the primary key of the users
	* @return the users that was removed
	* @throws NoSuchUsersException if a users with the primary key could not be found
	*/
	public Users remove(long UserID) throws NoSuchUsersException;

	public Users updateImpl(Users users);

	/**
	* Returns the users with the primary key or throws a {@link NoSuchUsersException} if it could not be found.
	*
	* @param UserID the primary key of the users
	* @return the users
	* @throws NoSuchUsersException if a users with the primary key could not be found
	*/
	public Users findByPrimaryKey(long UserID) throws NoSuchUsersException;

	/**
	* Returns the users with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param UserID the primary key of the users
	* @return the users, or <code>null</code> if a users with the primary key could not be found
	*/
	public Users fetchByPrimaryKey(long UserID);

	@Override
	public java.util.Map<java.io.Serializable, Users> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the userses.
	*
	* @return the userses
	*/
	public java.util.List<Users> findAll();

	/**
	* Returns a range of all the userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @return the range of userses
	*/
	public java.util.List<Users> findAll(int start, int end);

	/**
	* Returns an ordered range of all the userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of userses
	*/
	public java.util.List<Users> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator);

	/**
	* Returns an ordered range of all the userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of userses
	*/
	public java.util.List<Users> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Users> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the userses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of userses.
	*
	* @return the number of userses
	*/
	public int countAll();
}