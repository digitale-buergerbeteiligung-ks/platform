/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ChoicesLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ChoicesLocalService
 * @generated
 */
@ProviderType
public class ChoicesLocalServiceWrapper implements ChoicesLocalService,
	ServiceWrapper<ChoicesLocalService> {
	public ChoicesLocalServiceWrapper(ChoicesLocalService choicesLocalService) {
		_choicesLocalService = choicesLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _choicesLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _choicesLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _choicesLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _choicesLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _choicesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of choiceses.
	*
	* @return the number of choiceses
	*/
	@Override
	public int getChoicesesCount() {
		return _choicesLocalService.getChoicesesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _choicesLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _choicesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _choicesLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _choicesLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<surveyAPI.model.Choices> findByCTEXT(
		java.lang.String choiceText) {
		return _choicesLocalService.findByCTEXT(choiceText);
	}

	/**
	* Returns a range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @return the range of choiceses
	*/
	@Override
	public java.util.List<surveyAPI.model.Choices> getChoiceses(int start,
		int end) {
		return _choicesLocalService.getChoiceses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _choicesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _choicesLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the choices to the database. Also notifies the appropriate model listeners.
	*
	* @param choices the choices
	* @return the choices that was added
	*/
	@Override
	public surveyAPI.model.Choices addChoices(surveyAPI.model.Choices choices) {
		return _choicesLocalService.addChoices(choices);
	}

	/**
	* Creates a new choices with the primary key. Does not add the choices to the database.
	*
	* @param ChoiceID the primary key for the new choices
	* @return the new choices
	*/
	@Override
	public surveyAPI.model.Choices createChoices(long ChoiceID) {
		return _choicesLocalService.createChoices(ChoiceID);
	}

	/**
	* Deletes the choices with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices that was removed
	* @throws PortalException if a choices with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.Choices deleteChoices(long ChoiceID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _choicesLocalService.deleteChoices(ChoiceID);
	}

	/**
	* Deletes the choices from the database. Also notifies the appropriate model listeners.
	*
	* @param choices the choices
	* @return the choices that was removed
	*/
	@Override
	public surveyAPI.model.Choices deleteChoices(
		surveyAPI.model.Choices choices) {
		return _choicesLocalService.deleteChoices(choices);
	}

	@Override
	public surveyAPI.model.Choices fetchChoices(long ChoiceID) {
		return _choicesLocalService.fetchChoices(ChoiceID);
	}

	/**
	* Returns the choices with the primary key.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices
	* @throws PortalException if a choices with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.Choices getChoices(long ChoiceID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _choicesLocalService.getChoices(ChoiceID);
	}

	/**
	* Updates the choices in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param choices the choices
	* @return the choices that was updated
	*/
	@Override
	public surveyAPI.model.Choices updateChoices(
		surveyAPI.model.Choices choices) {
		return _choicesLocalService.updateChoices(choices);
	}

	@Override
	public ChoicesLocalService getWrappedService() {
		return _choicesLocalService;
	}

	@Override
	public void setWrappedService(ChoicesLocalService choicesLocalService) {
		_choicesLocalService = choicesLocalService;
	}

	private ChoicesLocalService _choicesLocalService;
}