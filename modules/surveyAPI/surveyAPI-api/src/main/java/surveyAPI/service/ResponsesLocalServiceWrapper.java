/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResponsesLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ResponsesLocalService
 * @generated
 */
@ProviderType
public class ResponsesLocalServiceWrapper implements ResponsesLocalService,
	ServiceWrapper<ResponsesLocalService> {
	public ResponsesLocalServiceWrapper(
		ResponsesLocalService responsesLocalService) {
		_responsesLocalService = responsesLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _responsesLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _responsesLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _responsesLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _responsesLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _responsesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of responseses.
	*
	* @return the number of responseses
	*/
	@Override
	public int getResponsesesCount() {
		return _responsesLocalService.getResponsesesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _responsesLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _responsesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _responsesLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _responsesLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @return the range of responseses
	*/
	@Override
	public java.util.List<surveyAPI.model.Responses> getResponseses(int start,
		int end) {
		return _responsesLocalService.getResponseses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _responsesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _responsesLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the responses to the database. Also notifies the appropriate model listeners.
	*
	* @param responses the responses
	* @return the responses that was added
	*/
	@Override
	public surveyAPI.model.Responses addResponses(
		surveyAPI.model.Responses responses) {
		return _responsesLocalService.addResponses(responses);
	}

	/**
	* Creates a new responses with the primary key. Does not add the responses to the database.
	*
	* @param ResponseID the primary key for the new responses
	* @return the new responses
	*/
	@Override
	public surveyAPI.model.Responses createResponses(long ResponseID) {
		return _responsesLocalService.createResponses(ResponseID);
	}

	/**
	* Deletes the responses with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses that was removed
	* @throws PortalException if a responses with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.Responses deleteResponses(long ResponseID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _responsesLocalService.deleteResponses(ResponseID);
	}

	/**
	* Deletes the responses from the database. Also notifies the appropriate model listeners.
	*
	* @param responses the responses
	* @return the responses that was removed
	*/
	@Override
	public surveyAPI.model.Responses deleteResponses(
		surveyAPI.model.Responses responses) {
		return _responsesLocalService.deleteResponses(responses);
	}

	@Override
	public surveyAPI.model.Responses fetchResponses(long ResponseID) {
		return _responsesLocalService.fetchResponses(ResponseID);
	}

	/**
	* Returns the responses with the primary key.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses
	* @throws PortalException if a responses with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.Responses getResponses(long ResponseID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _responsesLocalService.getResponses(ResponseID);
	}

	/**
	* Updates the responses in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param responses the responses
	* @return the responses that was updated
	*/
	@Override
	public surveyAPI.model.Responses updateResponses(
		surveyAPI.model.Responses responses) {
		return _responsesLocalService.updateResponses(responses);
	}

	@Override
	public ResponsesLocalService getWrappedService() {
		return _responsesLocalService;
	}

	@Override
	public void setWrappedService(ResponsesLocalService responsesLocalService) {
		_responsesLocalService = responsesLocalService;
	}

	private ResponsesLocalService _responsesLocalService;
}