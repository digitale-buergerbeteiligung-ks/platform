/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Questions. This utility wraps
 * {@link surveyAPI.service.impl.QuestionsServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsService
 * @see surveyAPI.service.base.QuestionsServiceBaseImpl
 * @see surveyAPI.service.impl.QuestionsServiceImpl
 * @generated
 */
@ProviderType
public class QuestionsServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link surveyAPI.service.impl.QuestionsServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static org.json.JSONObject addNewQuestion(long surveyID,
		java.lang.String QName, java.lang.String QText, java.lang.String Type,
		boolean required) throws org.json.JSONException {
		return getService()
				   .addNewQuestion(surveyID, QName, QText, Type, required);
	}

	public static org.json.JSONObject addQuestionNChoices(
		java.lang.String quesNChoices) throws org.json.JSONException {
		return getService().addQuestionNChoices(quesNChoices);
	}

	public static org.json.JSONObject deleteQuestion(long surveyID, long qID)
		throws org.json.JSONException {
		return getService().deleteQuestion(surveyID, qID);
	}

	public static org.json.JSONObject editQuestion(long qID,
		java.lang.String qText) throws org.json.JSONException {
		return getService().editQuestion(qID, qText);
	}

	public static QuestionsService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuestionsService, QuestionsService> _serviceTracker =
		ServiceTrackerFactory.open(QuestionsService.class);
}