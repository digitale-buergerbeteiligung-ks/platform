/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link QuestionOptionsLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see QuestionOptionsLocalService
 * @generated
 */
@ProviderType
public class QuestionOptionsLocalServiceWrapper
	implements QuestionOptionsLocalService,
		ServiceWrapper<QuestionOptionsLocalService> {
	public QuestionOptionsLocalServiceWrapper(
		QuestionOptionsLocalService questionOptionsLocalService) {
		_questionOptionsLocalService = questionOptionsLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _questionOptionsLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _questionOptionsLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _questionOptionsLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _questionOptionsLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _questionOptionsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of question optionses.
	*
	* @return the number of question optionses
	*/
	@Override
	public int getQuestionOptionsesCount() {
		return _questionOptionsLocalService.getQuestionOptionsesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _questionOptionsLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _questionOptionsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _questionOptionsLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _questionOptionsLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<surveyAPI.model.QuestionOptions> findByCHID(
		long ChoiceID) {
		return _questionOptionsLocalService.findByCHID(ChoiceID);
	}

	@Override
	public java.util.List<surveyAPI.model.QuestionOptions> findByQID(
		long QuestionID) {
		return _questionOptionsLocalService.findByQID(QuestionID);
	}

	@Override
	public java.util.List<surveyAPI.model.QuestionOptions> findByQuesIdNChoiceId(
		long QuestionID, long ChoiceID) {
		return _questionOptionsLocalService.findByQuesIdNChoiceId(QuestionID,
			ChoiceID);
	}

	/**
	* Returns a range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of question optionses
	*/
	@Override
	public java.util.List<surveyAPI.model.QuestionOptions> getQuestionOptionses(
		int start, int end) {
		return _questionOptionsLocalService.getQuestionOptionses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _questionOptionsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _questionOptionsLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* Adds the question options to the database. Also notifies the appropriate model listeners.
	*
	* @param questionOptions the question options
	* @return the question options that was added
	*/
	@Override
	public surveyAPI.model.QuestionOptions addQuestionOptions(
		surveyAPI.model.QuestionOptions questionOptions) {
		return _questionOptionsLocalService.addQuestionOptions(questionOptions);
	}

	/**
	* Creates a new question options with the primary key. Does not add the question options to the database.
	*
	* @param OptionID the primary key for the new question options
	* @return the new question options
	*/
	@Override
	public surveyAPI.model.QuestionOptions createQuestionOptions(long OptionID) {
		return _questionOptionsLocalService.createQuestionOptions(OptionID);
	}

	/**
	* Deletes the question options with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param OptionID the primary key of the question options
	* @return the question options that was removed
	* @throws PortalException if a question options with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.QuestionOptions deleteQuestionOptions(long OptionID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _questionOptionsLocalService.deleteQuestionOptions(OptionID);
	}

	/**
	* Deletes the question options from the database. Also notifies the appropriate model listeners.
	*
	* @param questionOptions the question options
	* @return the question options that was removed
	*/
	@Override
	public surveyAPI.model.QuestionOptions deleteQuestionOptions(
		surveyAPI.model.QuestionOptions questionOptions) {
		return _questionOptionsLocalService.deleteQuestionOptions(questionOptions);
	}

	@Override
	public surveyAPI.model.QuestionOptions fetchQuestionOptions(long OptionID) {
		return _questionOptionsLocalService.fetchQuestionOptions(OptionID);
	}

	/**
	* Returns the question options with the primary key.
	*
	* @param OptionID the primary key of the question options
	* @return the question options
	* @throws PortalException if a question options with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.QuestionOptions getQuestionOptions(long OptionID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _questionOptionsLocalService.getQuestionOptions(OptionID);
	}

	/**
	* Updates the question options in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param questionOptions the question options
	* @return the question options that was updated
	*/
	@Override
	public surveyAPI.model.QuestionOptions updateQuestionOptions(
		surveyAPI.model.QuestionOptions questionOptions) {
		return _questionOptionsLocalService.updateQuestionOptions(questionOptions);
	}

	@Override
	public QuestionOptionsLocalService getWrappedService() {
		return _questionOptionsLocalService;
	}

	@Override
	public void setWrappedService(
		QuestionOptionsLocalService questionOptionsLocalService) {
		_questionOptionsLocalService = questionOptionsLocalService;
	}

	private QuestionOptionsLocalService _questionOptionsLocalService;
}