/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Surveys. This utility wraps
 * {@link surveyAPI.service.impl.SurveysServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see SurveysService
 * @see surveyAPI.service.base.SurveysServiceBaseImpl
 * @see surveyAPI.service.impl.SurveysServiceImpl
 * @generated
 */
@ProviderType
public class SurveysServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link surveyAPI.service.impl.SurveysServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* JSON Web Service to add a new survey
	*/
	public static org.json.JSONObject addNewSurvey()
		throws org.json.JSONException {
		return getService().addNewSurvey();
	}

	/**
	* This just adds a survey and returns the survey ID, user can later add questions
	*
	* @param name
	* @return
	* @throws JSONException
	*/
	public static org.json.JSONObject addNewSurvey(java.lang.String name)
		throws org.json.JSONException {
		return getService().addNewSurvey(name);
	}

	/**
	* This adds a Survey with all the questions and choices
	*
	* @param name
	* @param jsonInput - Json array of json objects
	[
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"},
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"}
	]
	* @return
	* @throws JSONException
	*/
	public static org.json.JSONObject addNewSurvey(java.lang.String name,
		java.lang.String jsonInput) throws org.json.JSONException {
		return getService().addNewSurvey(name, jsonInput);
	}

	/**
	* JSON Web service to delete an existing survey based on survey ID
	*/
	public static org.json.JSONObject deleteSurvey()
		throws org.json.JSONException {
		return getService().deleteSurvey();
	}

	public static org.json.JSONObject deleteSurvey(long surveyID)
		throws org.json.JSONException {
		return getService().deleteSurvey(surveyID);
	}

	/**
	* JSON Web service to fetch all the question ids associated with a given survey id
	*/
	public static org.json.JSONObject getAllQuestionIds()
		throws org.json.JSONException {
		return getService().getAllQuestionIds();
	}

	public static org.json.JSONObject getAllQuestionIds(long surveyID)
		throws org.json.JSONException {
		return getService().getAllQuestionIds(surveyID);
	}

	/**
	* JSON Web service to get full survey
	*
	* @return JSONObject {Questions:JSONArray, Choices:JSONArray, QuestionIds:JSONArray}
	*/
	public static org.json.JSONObject getSurvey(long surveyId)
		throws org.json.JSONException {
		return getService().getSurvey(surveyId);
	}

	public static SurveysService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SurveysService, SurveysService> _serviceTracker =
		ServiceTrackerFactory.open(SurveysService.class);
}