/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.Responses;

import java.util.List;

/**
 * The persistence utility for the responses service. This utility wraps {@link surveyAPI.service.persistence.impl.ResponsesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResponsesPersistence
 * @see surveyAPI.service.persistence.impl.ResponsesPersistenceImpl
 * @generated
 */
@ProviderType
public class ResponsesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Responses responses) {
		getPersistence().clearCache(responses);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Responses> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Responses> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Responses> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Responses> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Responses update(Responses responses) {
		return getPersistence().update(responses);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Responses update(Responses responses,
		ServiceContext serviceContext) {
		return getPersistence().update(responses, serviceContext);
	}

	/**
	* Returns all the responseses where UserID = &#63;.
	*
	* @param UserID the user ID
	* @return the matching responseses
	*/
	public static List<Responses> findByUID(long UserID) {
		return getPersistence().findByUID(UserID);
	}

	/**
	* Returns a range of all the responseses where UserID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserID the user ID
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @return the range of matching responseses
	*/
	public static List<Responses> findByUID(long UserID, int start, int end) {
		return getPersistence().findByUID(UserID, start, end);
	}

	/**
	* Returns an ordered range of all the responseses where UserID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserID the user ID
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching responseses
	*/
	public static List<Responses> findByUID(long UserID, int start, int end,
		OrderByComparator<Responses> orderByComparator) {
		return getPersistence().findByUID(UserID, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the responseses where UserID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserID the user ID
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching responseses
	*/
	public static List<Responses> findByUID(long UserID, int start, int end,
		OrderByComparator<Responses> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUID(UserID, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching responses
	* @throws NoSuchResponsesException if a matching responses could not be found
	*/
	public static Responses findByUID_First(long UserID,
		OrderByComparator<Responses> orderByComparator)
		throws surveyAPI.exception.NoSuchResponsesException {
		return getPersistence().findByUID_First(UserID, orderByComparator);
	}

	/**
	* Returns the first responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching responses, or <code>null</code> if a matching responses could not be found
	*/
	public static Responses fetchByUID_First(long UserID,
		OrderByComparator<Responses> orderByComparator) {
		return getPersistence().fetchByUID_First(UserID, orderByComparator);
	}

	/**
	* Returns the last responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching responses
	* @throws NoSuchResponsesException if a matching responses could not be found
	*/
	public static Responses findByUID_Last(long UserID,
		OrderByComparator<Responses> orderByComparator)
		throws surveyAPI.exception.NoSuchResponsesException {
		return getPersistence().findByUID_Last(UserID, orderByComparator);
	}

	/**
	* Returns the last responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching responses, or <code>null</code> if a matching responses could not be found
	*/
	public static Responses fetchByUID_Last(long UserID,
		OrderByComparator<Responses> orderByComparator) {
		return getPersistence().fetchByUID_Last(UserID, orderByComparator);
	}

	/**
	* Returns the responseses before and after the current responses in the ordered set where UserID = &#63;.
	*
	* @param ResponseID the primary key of the current responses
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next responses
	* @throws NoSuchResponsesException if a responses with the primary key could not be found
	*/
	public static Responses[] findByUID_PrevAndNext(long ResponseID,
		long UserID, OrderByComparator<Responses> orderByComparator)
		throws surveyAPI.exception.NoSuchResponsesException {
		return getPersistence()
				   .findByUID_PrevAndNext(ResponseID, UserID, orderByComparator);
	}

	/**
	* Removes all the responseses where UserID = &#63; from the database.
	*
	* @param UserID the user ID
	*/
	public static void removeByUID(long UserID) {
		getPersistence().removeByUID(UserID);
	}

	/**
	* Returns the number of responseses where UserID = &#63;.
	*
	* @param UserID the user ID
	* @return the number of matching responseses
	*/
	public static int countByUID(long UserID) {
		return getPersistence().countByUID(UserID);
	}

	/**
	* Caches the responses in the entity cache if it is enabled.
	*
	* @param responses the responses
	*/
	public static void cacheResult(Responses responses) {
		getPersistence().cacheResult(responses);
	}

	/**
	* Caches the responseses in the entity cache if it is enabled.
	*
	* @param responseses the responseses
	*/
	public static void cacheResult(List<Responses> responseses) {
		getPersistence().cacheResult(responseses);
	}

	/**
	* Creates a new responses with the primary key. Does not add the responses to the database.
	*
	* @param ResponseID the primary key for the new responses
	* @return the new responses
	*/
	public static Responses create(long ResponseID) {
		return getPersistence().create(ResponseID);
	}

	/**
	* Removes the responses with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses that was removed
	* @throws NoSuchResponsesException if a responses with the primary key could not be found
	*/
	public static Responses remove(long ResponseID)
		throws surveyAPI.exception.NoSuchResponsesException {
		return getPersistence().remove(ResponseID);
	}

	public static Responses updateImpl(Responses responses) {
		return getPersistence().updateImpl(responses);
	}

	/**
	* Returns the responses with the primary key or throws a {@link NoSuchResponsesException} if it could not be found.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses
	* @throws NoSuchResponsesException if a responses with the primary key could not be found
	*/
	public static Responses findByPrimaryKey(long ResponseID)
		throws surveyAPI.exception.NoSuchResponsesException {
		return getPersistence().findByPrimaryKey(ResponseID);
	}

	/**
	* Returns the responses with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses, or <code>null</code> if a responses with the primary key could not be found
	*/
	public static Responses fetchByPrimaryKey(long ResponseID) {
		return getPersistence().fetchByPrimaryKey(ResponseID);
	}

	public static java.util.Map<java.io.Serializable, Responses> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the responseses.
	*
	* @return the responseses
	*/
	public static List<Responses> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @return the range of responseses
	*/
	public static List<Responses> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of responseses
	*/
	public static List<Responses> findAll(int start, int end,
		OrderByComparator<Responses> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of responseses
	*/
	public static List<Responses> findAll(int start, int end,
		OrderByComparator<Responses> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the responseses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of responseses.
	*
	* @return the number of responseses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ResponsesPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ResponsesPersistence, ResponsesPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ResponsesPersistence.class);
}