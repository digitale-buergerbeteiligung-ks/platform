/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.spring.osgi.OSGiBeanProperties;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Provides the remote service interface for Questions. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsServiceUtil
 * @see surveyAPI.service.base.QuestionsServiceBaseImpl
 * @see surveyAPI.service.impl.QuestionsServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@OSGiBeanProperties(property =  {
	"json.web.service.context.name=survey", "json.web.service.context.path=Questions"}, service = QuestionsService.class)
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface QuestionsService extends BaseService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuestionsServiceUtil} to access the questions remote service. Add custom service methods to {@link surveyAPI.service.impl.QuestionsServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@JSONWebService(method = "POST")
	public JSONObject addNewQuestion(long surveyID, java.lang.String QName,
		java.lang.String QText, java.lang.String Type, boolean required)
		throws JSONException;

	@JSONWebService(method = "POST")
	public JSONObject addQuestionNChoices(java.lang.String quesNChoices)
		throws JSONException;

	@JSONWebService(method = "DELETE")
	public JSONObject deleteQuestion(long surveyID, long qID)
		throws JSONException;

	@JSONWebService(method = "PUT")
	public JSONObject editQuestion(long qID, java.lang.String qText)
		throws JSONException;
}