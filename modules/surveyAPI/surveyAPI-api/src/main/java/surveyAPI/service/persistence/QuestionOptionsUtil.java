/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.QuestionOptions;

import java.util.List;

/**
 * The persistence utility for the question options service. This utility wraps {@link surveyAPI.service.persistence.impl.QuestionOptionsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionOptionsPersistence
 * @see surveyAPI.service.persistence.impl.QuestionOptionsPersistenceImpl
 * @generated
 */
@ProviderType
public class QuestionOptionsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(QuestionOptions questionOptions) {
		getPersistence().clearCache(questionOptions);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<QuestionOptions> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<QuestionOptions> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<QuestionOptions> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static QuestionOptions update(QuestionOptions questionOptions) {
		return getPersistence().update(questionOptions);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static QuestionOptions update(QuestionOptions questionOptions,
		ServiceContext serviceContext) {
		return getPersistence().update(questionOptions, serviceContext);
	}

	/**
	* Returns all the question optionses where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @return the matching question optionses
	*/
	public static List<QuestionOptions> findByQID(long QuestionID) {
		return getPersistence().findByQID(QuestionID);
	}

	/**
	* Returns a range of all the question optionses where QuestionID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of matching question optionses
	*/
	public static List<QuestionOptions> findByQID(long QuestionID, int start,
		int end) {
		return getPersistence().findByQID(QuestionID, start, end);
	}

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching question optionses
	*/
	public static List<QuestionOptions> findByQID(long QuestionID, int start,
		int end, OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence()
				   .findByQID(QuestionID, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching question optionses
	*/
	public static List<QuestionOptions> findByQID(long QuestionID, int start,
		int end, OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByQID(QuestionID, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public static QuestionOptions findByQID_First(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence().findByQID_First(QuestionID, orderByComparator);
	}

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public static QuestionOptions fetchByQID_First(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence().fetchByQID_First(QuestionID, orderByComparator);
	}

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public static QuestionOptions findByQID_Last(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence().findByQID_Last(QuestionID, orderByComparator);
	}

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public static QuestionOptions fetchByQID_Last(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence().fetchByQID_Last(QuestionID, orderByComparator);
	}

	/**
	* Returns the question optionses before and after the current question options in the ordered set where QuestionID = &#63;.
	*
	* @param OptionID the primary key of the current question options
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public static QuestionOptions[] findByQID_PrevAndNext(long OptionID,
		long QuestionID, OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence()
				   .findByQID_PrevAndNext(OptionID, QuestionID,
			orderByComparator);
	}

	/**
	* Removes all the question optionses where QuestionID = &#63; from the database.
	*
	* @param QuestionID the question ID
	*/
	public static void removeByQID(long QuestionID) {
		getPersistence().removeByQID(QuestionID);
	}

	/**
	* Returns the number of question optionses where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @return the number of matching question optionses
	*/
	public static int countByQID(long QuestionID) {
		return getPersistence().countByQID(QuestionID);
	}

	/**
	* Returns all the question optionses where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @return the matching question optionses
	*/
	public static List<QuestionOptions> findByCHID(long ChoiceID) {
		return getPersistence().findByCHID(ChoiceID);
	}

	/**
	* Returns a range of all the question optionses where ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of matching question optionses
	*/
	public static List<QuestionOptions> findByCHID(long ChoiceID, int start,
		int end) {
		return getPersistence().findByCHID(ChoiceID, start, end);
	}

	/**
	* Returns an ordered range of all the question optionses where ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching question optionses
	*/
	public static List<QuestionOptions> findByCHID(long ChoiceID, int start,
		int end, OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence()
				   .findByCHID(ChoiceID, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the question optionses where ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching question optionses
	*/
	public static List<QuestionOptions> findByCHID(long ChoiceID, int start,
		int end, OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCHID(ChoiceID, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public static QuestionOptions findByCHID_First(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence().findByCHID_First(ChoiceID, orderByComparator);
	}

	/**
	* Returns the first question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public static QuestionOptions fetchByCHID_First(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence().fetchByCHID_First(ChoiceID, orderByComparator);
	}

	/**
	* Returns the last question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public static QuestionOptions findByCHID_Last(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence().findByCHID_Last(ChoiceID, orderByComparator);
	}

	/**
	* Returns the last question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public static QuestionOptions fetchByCHID_Last(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence().fetchByCHID_Last(ChoiceID, orderByComparator);
	}

	/**
	* Returns the question optionses before and after the current question options in the ordered set where ChoiceID = &#63;.
	*
	* @param OptionID the primary key of the current question options
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public static QuestionOptions[] findByCHID_PrevAndNext(long OptionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence()
				   .findByCHID_PrevAndNext(OptionID, ChoiceID, orderByComparator);
	}

	/**
	* Removes all the question optionses where ChoiceID = &#63; from the database.
	*
	* @param ChoiceID the choice ID
	*/
	public static void removeByCHID(long ChoiceID) {
		getPersistence().removeByCHID(ChoiceID);
	}

	/**
	* Returns the number of question optionses where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @return the number of matching question optionses
	*/
	public static int countByCHID(long ChoiceID) {
		return getPersistence().countByCHID(ChoiceID);
	}

	/**
	* Returns all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @return the matching question optionses
	*/
	public static List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID) {
		return getPersistence().findByQuesIdNChoiceId(QuestionID, ChoiceID);
	}

	/**
	* Returns a range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of matching question optionses
	*/
	public static List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID, int start, int end) {
		return getPersistence()
				   .findByQuesIdNChoiceId(QuestionID, ChoiceID, start, end);
	}

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching question optionses
	*/
	public static List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence()
				   .findByQuesIdNChoiceId(QuestionID, ChoiceID, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching question optionses
	*/
	public static List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByQuesIdNChoiceId(QuestionID, ChoiceID, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public static QuestionOptions findByQuesIdNChoiceId_First(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence()
				   .findByQuesIdNChoiceId_First(QuestionID, ChoiceID,
			orderByComparator);
	}

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public static QuestionOptions fetchByQuesIdNChoiceId_First(
		long QuestionID, long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence()
				   .fetchByQuesIdNChoiceId_First(QuestionID, ChoiceID,
			orderByComparator);
	}

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public static QuestionOptions findByQuesIdNChoiceId_Last(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence()
				   .findByQuesIdNChoiceId_Last(QuestionID, ChoiceID,
			orderByComparator);
	}

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public static QuestionOptions fetchByQuesIdNChoiceId_Last(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence()
				   .fetchByQuesIdNChoiceId_Last(QuestionID, ChoiceID,
			orderByComparator);
	}

	/**
	* Returns the question optionses before and after the current question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param OptionID the primary key of the current question options
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public static QuestionOptions[] findByQuesIdNChoiceId_PrevAndNext(
		long OptionID, long QuestionID, long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence()
				   .findByQuesIdNChoiceId_PrevAndNext(OptionID, QuestionID,
			ChoiceID, orderByComparator);
	}

	/**
	* Removes all the question optionses where QuestionID = &#63; and ChoiceID = &#63; from the database.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	*/
	public static void removeByQuesIdNChoiceId(long QuestionID, long ChoiceID) {
		getPersistence().removeByQuesIdNChoiceId(QuestionID, ChoiceID);
	}

	/**
	* Returns the number of question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @return the number of matching question optionses
	*/
	public static int countByQuesIdNChoiceId(long QuestionID, long ChoiceID) {
		return getPersistence().countByQuesIdNChoiceId(QuestionID, ChoiceID);
	}

	/**
	* Caches the question options in the entity cache if it is enabled.
	*
	* @param questionOptions the question options
	*/
	public static void cacheResult(QuestionOptions questionOptions) {
		getPersistence().cacheResult(questionOptions);
	}

	/**
	* Caches the question optionses in the entity cache if it is enabled.
	*
	* @param questionOptionses the question optionses
	*/
	public static void cacheResult(List<QuestionOptions> questionOptionses) {
		getPersistence().cacheResult(questionOptionses);
	}

	/**
	* Creates a new question options with the primary key. Does not add the question options to the database.
	*
	* @param OptionID the primary key for the new question options
	* @return the new question options
	*/
	public static QuestionOptions create(long OptionID) {
		return getPersistence().create(OptionID);
	}

	/**
	* Removes the question options with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param OptionID the primary key of the question options
	* @return the question options that was removed
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public static QuestionOptions remove(long OptionID)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence().remove(OptionID);
	}

	public static QuestionOptions updateImpl(QuestionOptions questionOptions) {
		return getPersistence().updateImpl(questionOptions);
	}

	/**
	* Returns the question options with the primary key or throws a {@link NoSuchQuestionOptionsException} if it could not be found.
	*
	* @param OptionID the primary key of the question options
	* @return the question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public static QuestionOptions findByPrimaryKey(long OptionID)
		throws surveyAPI.exception.NoSuchQuestionOptionsException {
		return getPersistence().findByPrimaryKey(OptionID);
	}

	/**
	* Returns the question options with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param OptionID the primary key of the question options
	* @return the question options, or <code>null</code> if a question options with the primary key could not be found
	*/
	public static QuestionOptions fetchByPrimaryKey(long OptionID) {
		return getPersistence().fetchByPrimaryKey(OptionID);
	}

	public static java.util.Map<java.io.Serializable, QuestionOptions> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the question optionses.
	*
	* @return the question optionses
	*/
	public static List<QuestionOptions> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of question optionses
	*/
	public static List<QuestionOptions> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of question optionses
	*/
	public static List<QuestionOptions> findAll(int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of question optionses
	*/
	public static List<QuestionOptions> findAll(int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the question optionses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of question optionses.
	*
	* @return the number of question optionses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static QuestionOptionsPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuestionOptionsPersistence, QuestionOptionsPersistence> _serviceTracker =
		ServiceTrackerFactory.open(QuestionOptionsPersistence.class);
}