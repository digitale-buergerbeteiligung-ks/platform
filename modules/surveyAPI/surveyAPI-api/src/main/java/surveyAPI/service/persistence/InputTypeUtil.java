/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.InputType;

import java.util.List;

/**
 * The persistence utility for the input type service. This utility wraps {@link surveyAPI.service.persistence.impl.InputTypePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InputTypePersistence
 * @see surveyAPI.service.persistence.impl.InputTypePersistenceImpl
 * @generated
 */
@ProviderType
public class InputTypeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(InputType inputType) {
		getPersistence().clearCache(inputType);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<InputType> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<InputType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<InputType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<InputType> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static InputType update(InputType inputType) {
		return getPersistence().update(inputType);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static InputType update(InputType inputType,
		ServiceContext serviceContext) {
		return getPersistence().update(inputType, serviceContext);
	}

	/**
	* Caches the input type in the entity cache if it is enabled.
	*
	* @param inputType the input type
	*/
	public static void cacheResult(InputType inputType) {
		getPersistence().cacheResult(inputType);
	}

	/**
	* Caches the input types in the entity cache if it is enabled.
	*
	* @param inputTypes the input types
	*/
	public static void cacheResult(List<InputType> inputTypes) {
		getPersistence().cacheResult(inputTypes);
	}

	/**
	* Creates a new input type with the primary key. Does not add the input type to the database.
	*
	* @param TypeID the primary key for the new input type
	* @return the new input type
	*/
	public static InputType create(int TypeID) {
		return getPersistence().create(TypeID);
	}

	/**
	* Removes the input type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param TypeID the primary key of the input type
	* @return the input type that was removed
	* @throws NoSuchInputTypeException if a input type with the primary key could not be found
	*/
	public static InputType remove(int TypeID)
		throws surveyAPI.exception.NoSuchInputTypeException {
		return getPersistence().remove(TypeID);
	}

	public static InputType updateImpl(InputType inputType) {
		return getPersistence().updateImpl(inputType);
	}

	/**
	* Returns the input type with the primary key or throws a {@link NoSuchInputTypeException} if it could not be found.
	*
	* @param TypeID the primary key of the input type
	* @return the input type
	* @throws NoSuchInputTypeException if a input type with the primary key could not be found
	*/
	public static InputType findByPrimaryKey(int TypeID)
		throws surveyAPI.exception.NoSuchInputTypeException {
		return getPersistence().findByPrimaryKey(TypeID);
	}

	/**
	* Returns the input type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param TypeID the primary key of the input type
	* @return the input type, or <code>null</code> if a input type with the primary key could not be found
	*/
	public static InputType fetchByPrimaryKey(int TypeID) {
		return getPersistence().fetchByPrimaryKey(TypeID);
	}

	public static java.util.Map<java.io.Serializable, InputType> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the input types.
	*
	* @return the input types
	*/
	public static List<InputType> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @return the range of input types
	*/
	public static List<InputType> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of input types
	*/
	public static List<InputType> findAll(int start, int end,
		OrderByComparator<InputType> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of input types
	*/
	public static List<InputType> findAll(int start, int end,
		OrderByComparator<InputType> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the input types from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of input types.
	*
	* @return the number of input types
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static InputTypePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<InputTypePersistence, InputTypePersistence> _serviceTracker =
		ServiceTrackerFactory.open(InputTypePersistence.class);
}