/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchQuestionsException;

import surveyAPI.model.Questions;

/**
 * The persistence interface for the questions service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.QuestionsPersistenceImpl
 * @see QuestionsUtil
 * @generated
 */
@ProviderType
public interface QuestionsPersistence extends BasePersistence<Questions> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuestionsUtil} to access the questions persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the questions in the entity cache if it is enabled.
	*
	* @param questions the questions
	*/
	public void cacheResult(Questions questions);

	/**
	* Caches the questionses in the entity cache if it is enabled.
	*
	* @param questionses the questionses
	*/
	public void cacheResult(java.util.List<Questions> questionses);

	/**
	* Creates a new questions with the primary key. Does not add the questions to the database.
	*
	* @param QuestionID the primary key for the new questions
	* @return the new questions
	*/
	public Questions create(long QuestionID);

	/**
	* Removes the questions with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions that was removed
	* @throws NoSuchQuestionsException if a questions with the primary key could not be found
	*/
	public Questions remove(long QuestionID) throws NoSuchQuestionsException;

	public Questions updateImpl(Questions questions);

	/**
	* Returns the questions with the primary key or throws a {@link NoSuchQuestionsException} if it could not be found.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions
	* @throws NoSuchQuestionsException if a questions with the primary key could not be found
	*/
	public Questions findByPrimaryKey(long QuestionID)
		throws NoSuchQuestionsException;

	/**
	* Returns the questions with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions, or <code>null</code> if a questions with the primary key could not be found
	*/
	public Questions fetchByPrimaryKey(long QuestionID);

	@Override
	public java.util.Map<java.io.Serializable, Questions> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the questionses.
	*
	* @return the questionses
	*/
	public java.util.List<Questions> findAll();

	/**
	* Returns a range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @return the range of questionses
	*/
	public java.util.List<Questions> findAll(int start, int end);

	/**
	* Returns an ordered range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questionses
	*/
	public java.util.List<Questions> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Questions> orderByComparator);

	/**
	* Returns an ordered range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of questionses
	*/
	public java.util.List<Questions> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Questions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the questionses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of questionses.
	*
	* @return the number of questionses
	*/
	public int countAll();
}