/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.Questions;

import java.util.List;

/**
 * The persistence utility for the questions service. This utility wraps {@link surveyAPI.service.persistence.impl.QuestionsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsPersistence
 * @see surveyAPI.service.persistence.impl.QuestionsPersistenceImpl
 * @generated
 */
@ProviderType
public class QuestionsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Questions questions) {
		getPersistence().clearCache(questions);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Questions> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Questions> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Questions> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Questions> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Questions update(Questions questions) {
		return getPersistence().update(questions);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Questions update(Questions questions,
		ServiceContext serviceContext) {
		return getPersistence().update(questions, serviceContext);
	}

	/**
	* Caches the questions in the entity cache if it is enabled.
	*
	* @param questions the questions
	*/
	public static void cacheResult(Questions questions) {
		getPersistence().cacheResult(questions);
	}

	/**
	* Caches the questionses in the entity cache if it is enabled.
	*
	* @param questionses the questionses
	*/
	public static void cacheResult(List<Questions> questionses) {
		getPersistence().cacheResult(questionses);
	}

	/**
	* Creates a new questions with the primary key. Does not add the questions to the database.
	*
	* @param QuestionID the primary key for the new questions
	* @return the new questions
	*/
	public static Questions create(long QuestionID) {
		return getPersistence().create(QuestionID);
	}

	/**
	* Removes the questions with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions that was removed
	* @throws NoSuchQuestionsException if a questions with the primary key could not be found
	*/
	public static Questions remove(long QuestionID)
		throws surveyAPI.exception.NoSuchQuestionsException {
		return getPersistence().remove(QuestionID);
	}

	public static Questions updateImpl(Questions questions) {
		return getPersistence().updateImpl(questions);
	}

	/**
	* Returns the questions with the primary key or throws a {@link NoSuchQuestionsException} if it could not be found.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions
	* @throws NoSuchQuestionsException if a questions with the primary key could not be found
	*/
	public static Questions findByPrimaryKey(long QuestionID)
		throws surveyAPI.exception.NoSuchQuestionsException {
		return getPersistence().findByPrimaryKey(QuestionID);
	}

	/**
	* Returns the questions with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions, or <code>null</code> if a questions with the primary key could not be found
	*/
	public static Questions fetchByPrimaryKey(long QuestionID) {
		return getPersistence().fetchByPrimaryKey(QuestionID);
	}

	public static java.util.Map<java.io.Serializable, Questions> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the questionses.
	*
	* @return the questionses
	*/
	public static List<Questions> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @return the range of questionses
	*/
	public static List<Questions> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of questionses
	*/
	public static List<Questions> findAll(int start, int end,
		OrderByComparator<Questions> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of questionses
	*/
	public static List<Questions> findAll(int start, int end,
		OrderByComparator<Questions> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the questionses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of questionses.
	*
	* @return the number of questionses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static QuestionsPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuestionsPersistence, QuestionsPersistence> _serviceTracker =
		ServiceTrackerFactory.open(QuestionsPersistence.class);
}