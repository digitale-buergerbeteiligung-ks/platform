/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchChoicesException;

import surveyAPI.model.Choices;

/**
 * The persistence interface for the choices service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.ChoicesPersistenceImpl
 * @see ChoicesUtil
 * @generated
 */
@ProviderType
public interface ChoicesPersistence extends BasePersistence<Choices> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ChoicesUtil} to access the choices persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the choiceses where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @return the matching choiceses
	*/
	public java.util.List<Choices> findByCTEXT(java.lang.String ChoiceText);

	/**
	* Returns a range of all the choiceses where ChoiceText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceText the choice text
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @return the range of matching choiceses
	*/
	public java.util.List<Choices> findByCTEXT(java.lang.String ChoiceText,
		int start, int end);

	/**
	* Returns an ordered range of all the choiceses where ChoiceText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceText the choice text
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching choiceses
	*/
	public java.util.List<Choices> findByCTEXT(java.lang.String ChoiceText,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator);

	/**
	* Returns an ordered range of all the choiceses where ChoiceText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceText the choice text
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching choiceses
	*/
	public java.util.List<Choices> findByCTEXT(java.lang.String ChoiceText,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching choices
	* @throws NoSuchChoicesException if a matching choices could not be found
	*/
	public Choices findByCTEXT_First(java.lang.String ChoiceText,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator)
		throws NoSuchChoicesException;

	/**
	* Returns the first choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching choices, or <code>null</code> if a matching choices could not be found
	*/
	public Choices fetchByCTEXT_First(java.lang.String ChoiceText,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator);

	/**
	* Returns the last choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching choices
	* @throws NoSuchChoicesException if a matching choices could not be found
	*/
	public Choices findByCTEXT_Last(java.lang.String ChoiceText,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator)
		throws NoSuchChoicesException;

	/**
	* Returns the last choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching choices, or <code>null</code> if a matching choices could not be found
	*/
	public Choices fetchByCTEXT_Last(java.lang.String ChoiceText,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator);

	/**
	* Returns the choiceses before and after the current choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceID the primary key of the current choices
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next choices
	* @throws NoSuchChoicesException if a choices with the primary key could not be found
	*/
	public Choices[] findByCTEXT_PrevAndNext(long ChoiceID,
		java.lang.String ChoiceText,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator)
		throws NoSuchChoicesException;

	/**
	* Removes all the choiceses where ChoiceText = &#63; from the database.
	*
	* @param ChoiceText the choice text
	*/
	public void removeByCTEXT(java.lang.String ChoiceText);

	/**
	* Returns the number of choiceses where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @return the number of matching choiceses
	*/
	public int countByCTEXT(java.lang.String ChoiceText);

	/**
	* Caches the choices in the entity cache if it is enabled.
	*
	* @param choices the choices
	*/
	public void cacheResult(Choices choices);

	/**
	* Caches the choiceses in the entity cache if it is enabled.
	*
	* @param choiceses the choiceses
	*/
	public void cacheResult(java.util.List<Choices> choiceses);

	/**
	* Creates a new choices with the primary key. Does not add the choices to the database.
	*
	* @param ChoiceID the primary key for the new choices
	* @return the new choices
	*/
	public Choices create(long ChoiceID);

	/**
	* Removes the choices with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices that was removed
	* @throws NoSuchChoicesException if a choices with the primary key could not be found
	*/
	public Choices remove(long ChoiceID) throws NoSuchChoicesException;

	public Choices updateImpl(Choices choices);

	/**
	* Returns the choices with the primary key or throws a {@link NoSuchChoicesException} if it could not be found.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices
	* @throws NoSuchChoicesException if a choices with the primary key could not be found
	*/
	public Choices findByPrimaryKey(long ChoiceID)
		throws NoSuchChoicesException;

	/**
	* Returns the choices with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices, or <code>null</code> if a choices with the primary key could not be found
	*/
	public Choices fetchByPrimaryKey(long ChoiceID);

	@Override
	public java.util.Map<java.io.Serializable, Choices> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the choiceses.
	*
	* @return the choiceses
	*/
	public java.util.List<Choices> findAll();

	/**
	* Returns a range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @return the range of choiceses
	*/
	public java.util.List<Choices> findAll(int start, int end);

	/**
	* Returns an ordered range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of choiceses
	*/
	public java.util.List<Choices> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator);

	/**
	* Returns an ordered range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of choiceses
	*/
	public java.util.List<Choices> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Choices> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the choiceses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of choiceses.
	*
	* @return the number of choiceses
	*/
	public int countAll();
}