/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link QuestionOptions}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionOptions
 * @generated
 */
@ProviderType
public class QuestionOptionsWrapper implements QuestionOptions,
	ModelWrapper<QuestionOptions> {
	public QuestionOptionsWrapper(QuestionOptions questionOptions) {
		_questionOptions = questionOptions;
	}

	@Override
	public Class<?> getModelClass() {
		return QuestionOptions.class;
	}

	@Override
	public String getModelClassName() {
		return QuestionOptions.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("OptionID", getOptionID());
		attributes.put("QuestionID", getQuestionID());
		attributes.put("ChoiceID", getChoiceID());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long OptionID = (Long)attributes.get("OptionID");

		if (OptionID != null) {
			setOptionID(OptionID);
		}

		Long QuestionID = (Long)attributes.get("QuestionID");

		if (QuestionID != null) {
			setQuestionID(QuestionID);
		}

		Long ChoiceID = (Long)attributes.get("ChoiceID");

		if (ChoiceID != null) {
			setChoiceID(ChoiceID);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _questionOptions.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _questionOptions.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _questionOptions.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _questionOptions.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.QuestionOptions> toCacheModel() {
		return _questionOptions.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.QuestionOptions questionOptions) {
		return _questionOptions.compareTo(questionOptions);
	}

	@Override
	public int hashCode() {
		return _questionOptions.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _questionOptions.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new QuestionOptionsWrapper((QuestionOptions)_questionOptions.clone());
	}

	@Override
	public java.lang.String toString() {
		return _questionOptions.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _questionOptions.toXmlString();
	}

	/**
	* Returns the choice ID of this question options.
	*
	* @return the choice ID of this question options
	*/
	@Override
	public long getChoiceID() {
		return _questionOptions.getChoiceID();
	}

	/**
	* Returns the option ID of this question options.
	*
	* @return the option ID of this question options
	*/
	@Override
	public long getOptionID() {
		return _questionOptions.getOptionID();
	}

	/**
	* Returns the primary key of this question options.
	*
	* @return the primary key of this question options
	*/
	@Override
	public long getPrimaryKey() {
		return _questionOptions.getPrimaryKey();
	}

	/**
	* Returns the question ID of this question options.
	*
	* @return the question ID of this question options
	*/
	@Override
	public long getQuestionID() {
		return _questionOptions.getQuestionID();
	}

	@Override
	public surveyAPI.model.QuestionOptions toEscapedModel() {
		return new QuestionOptionsWrapper(_questionOptions.toEscapedModel());
	}

	@Override
	public surveyAPI.model.QuestionOptions toUnescapedModel() {
		return new QuestionOptionsWrapper(_questionOptions.toUnescapedModel());
	}

	@Override
	public void persist() {
		_questionOptions.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_questionOptions.setCachedModel(cachedModel);
	}

	/**
	* Sets the choice ID of this question options.
	*
	* @param ChoiceID the choice ID of this question options
	*/
	@Override
	public void setChoiceID(long ChoiceID) {
		_questionOptions.setChoiceID(ChoiceID);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_questionOptions.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_questionOptions.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_questionOptions.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_questionOptions.setNew(n);
	}

	/**
	* Sets the option ID of this question options.
	*
	* @param OptionID the option ID of this question options
	*/
	@Override
	public void setOptionID(long OptionID) {
		_questionOptions.setOptionID(OptionID);
	}

	/**
	* Sets the primary key of this question options.
	*
	* @param primaryKey the primary key of this question options
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_questionOptions.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_questionOptions.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the question ID of this question options.
	*
	* @param QuestionID the question ID of this question options
	*/
	@Override
	public void setQuestionID(long QuestionID) {
		_questionOptions.setQuestionID(QuestionID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuestionOptionsWrapper)) {
			return false;
		}

		QuestionOptionsWrapper questionOptionsWrapper = (QuestionOptionsWrapper)obj;

		if (Objects.equals(_questionOptions,
					questionOptionsWrapper._questionOptions)) {
			return true;
		}

		return false;
	}

	@Override
	public QuestionOptions getWrappedModel() {
		return _questionOptions;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _questionOptions.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _questionOptions.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_questionOptions.resetOriginalValues();
	}

	private final QuestionOptions _questionOptions;
}