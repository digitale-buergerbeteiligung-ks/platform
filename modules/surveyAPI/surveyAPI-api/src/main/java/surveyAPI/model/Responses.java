/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Responses service. Represents a row in the &quot;SURVEY_Responses&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ResponsesModel
 * @see surveyAPI.model.impl.ResponsesImpl
 * @see surveyAPI.model.impl.ResponsesModelImpl
 * @generated
 */
@ImplementationClassName("surveyAPI.model.impl.ResponsesImpl")
@ProviderType
public interface Responses extends ResponsesModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link surveyAPI.model.impl.ResponsesImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Responses, Long> RESPONSE_ID_ACCESSOR = new Accessor<Responses, Long>() {
			@Override
			public Long get(Responses responses) {
				return responses.getResponseID();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Responses> getTypeClass() {
				return Responses.class;
			}
		};
}