/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import surveyAPI.service.persistence.UserSurveyMappingPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.UserSurveyMappingServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.UserSurveyMappingServiceSoap
 * @generated
 */
@ProviderType
public class UserSurveyMappingSoap implements Serializable {
	public static UserSurveyMappingSoap toSoapModel(UserSurveyMapping model) {
		UserSurveyMappingSoap soapModel = new UserSurveyMappingSoap();

		soapModel.setUserID(model.getUserID());
		soapModel.setSurveyID(model.getSurveyID());
		soapModel.setDateOfSurvey(model.getDateOfSurvey());

		return soapModel;
	}

	public static UserSurveyMappingSoap[] toSoapModels(
		UserSurveyMapping[] models) {
		UserSurveyMappingSoap[] soapModels = new UserSurveyMappingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserSurveyMappingSoap[][] toSoapModels(
		UserSurveyMapping[][] models) {
		UserSurveyMappingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserSurveyMappingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserSurveyMappingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserSurveyMappingSoap[] toSoapModels(
		List<UserSurveyMapping> models) {
		List<UserSurveyMappingSoap> soapModels = new ArrayList<UserSurveyMappingSoap>(models.size());

		for (UserSurveyMapping model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserSurveyMappingSoap[soapModels.size()]);
	}

	public UserSurveyMappingSoap() {
	}

	public UserSurveyMappingPK getPrimaryKey() {
		return new UserSurveyMappingPK(_UserID, _SurveyID);
	}

	public void setPrimaryKey(UserSurveyMappingPK pk) {
		setUserID(pk.UserID);
		setSurveyID(pk.SurveyID);
	}

	public long getUserID() {
		return _UserID;
	}

	public void setUserID(long UserID) {
		_UserID = UserID;
	}

	public long getSurveyID() {
		return _SurveyID;
	}

	public void setSurveyID(long SurveyID) {
		_SurveyID = SurveyID;
	}

	public Date getDateOfSurvey() {
		return _DateOfSurvey;
	}

	public void setDateOfSurvey(Date DateOfSurvey) {
		_DateOfSurvey = DateOfSurvey;
	}

	private long _UserID;
	private long _SurveyID;
	private Date _DateOfSurvey;
}