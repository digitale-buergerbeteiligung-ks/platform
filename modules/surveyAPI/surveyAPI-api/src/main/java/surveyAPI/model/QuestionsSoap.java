/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.QuestionsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.QuestionsServiceSoap
 * @generated
 */
@ProviderType
public class QuestionsSoap implements Serializable {
	public static QuestionsSoap toSoapModel(Questions model) {
		QuestionsSoap soapModel = new QuestionsSoap();

		soapModel.setQuestionID(model.getQuestionID());
		soapModel.setResponseType(model.getResponseType());
		soapModel.setQuestionName(model.getQuestionName());
		soapModel.setQuestionText(model.getQuestionText());
		soapModel.setDateOfCreation(model.getDateOfCreation());
		soapModel.setRequired(model.getRequired());

		return soapModel;
	}

	public static QuestionsSoap[] toSoapModels(Questions[] models) {
		QuestionsSoap[] soapModels = new QuestionsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuestionsSoap[][] toSoapModels(Questions[][] models) {
		QuestionsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuestionsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuestionsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuestionsSoap[] toSoapModels(List<Questions> models) {
		List<QuestionsSoap> soapModels = new ArrayList<QuestionsSoap>(models.size());

		for (Questions model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuestionsSoap[soapModels.size()]);
	}

	public QuestionsSoap() {
	}

	public long getPrimaryKey() {
		return _QuestionID;
	}

	public void setPrimaryKey(long pk) {
		setQuestionID(pk);
	}

	public long getQuestionID() {
		return _QuestionID;
	}

	public void setQuestionID(long QuestionID) {
		_QuestionID = QuestionID;
	}

	public String getResponseType() {
		return _ResponseType;
	}

	public void setResponseType(String ResponseType) {
		_ResponseType = ResponseType;
	}

	public String getQuestionName() {
		return _QuestionName;
	}

	public void setQuestionName(String QuestionName) {
		_QuestionName = QuestionName;
	}

	public String getQuestionText() {
		return _QuestionText;
	}

	public void setQuestionText(String QuestionText) {
		_QuestionText = QuestionText;
	}

	public Date getDateOfCreation() {
		return _DateOfCreation;
	}

	public void setDateOfCreation(Date DateOfCreation) {
		_DateOfCreation = DateOfCreation;
	}

	public boolean getRequired() {
		return _Required;
	}

	public boolean isRequired() {
		return _Required;
	}

	public void setRequired(boolean Required) {
		_Required = Required;
	}

	private long _QuestionID;
	private String _ResponseType;
	private String _QuestionName;
	private String _QuestionText;
	private Date _DateOfCreation;
	private boolean _Required;
}