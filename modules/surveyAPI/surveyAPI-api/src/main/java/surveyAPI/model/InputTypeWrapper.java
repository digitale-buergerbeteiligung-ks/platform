/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link InputType}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InputType
 * @generated
 */
@ProviderType
public class InputTypeWrapper implements InputType, ModelWrapper<InputType> {
	public InputTypeWrapper(InputType inputType) {
		_inputType = inputType;
	}

	@Override
	public Class<?> getModelClass() {
		return InputType.class;
	}

	@Override
	public String getModelClassName() {
		return InputType.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("TypeID", getTypeID());
		attributes.put("TypeDescription", getTypeDescription());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer TypeID = (Integer)attributes.get("TypeID");

		if (TypeID != null) {
			setTypeID(TypeID);
		}

		String TypeDescription = (String)attributes.get("TypeDescription");

		if (TypeDescription != null) {
			setTypeDescription(TypeDescription);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _inputType.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _inputType.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _inputType.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _inputType.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.InputType> toCacheModel() {
		return _inputType.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.InputType inputType) {
		return _inputType.compareTo(inputType);
	}

	/**
	* Returns the primary key of this input type.
	*
	* @return the primary key of this input type
	*/
	@Override
	public int getPrimaryKey() {
		return _inputType.getPrimaryKey();
	}

	/**
	* Returns the type ID of this input type.
	*
	* @return the type ID of this input type
	*/
	@Override
	public int getTypeID() {
		return _inputType.getTypeID();
	}

	@Override
	public int hashCode() {
		return _inputType.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _inputType.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new InputTypeWrapper((InputType)_inputType.clone());
	}

	/**
	* Returns the type description of this input type.
	*
	* @return the type description of this input type
	*/
	@Override
	public java.lang.String getTypeDescription() {
		return _inputType.getTypeDescription();
	}

	@Override
	public java.lang.String toString() {
		return _inputType.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _inputType.toXmlString();
	}

	@Override
	public surveyAPI.model.InputType toEscapedModel() {
		return new InputTypeWrapper(_inputType.toEscapedModel());
	}

	@Override
	public surveyAPI.model.InputType toUnescapedModel() {
		return new InputTypeWrapper(_inputType.toUnescapedModel());
	}

	@Override
	public void persist() {
		_inputType.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_inputType.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_inputType.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_inputType.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_inputType.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_inputType.setNew(n);
	}

	/**
	* Sets the primary key of this input type.
	*
	* @param primaryKey the primary key of this input type
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_inputType.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_inputType.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the type description of this input type.
	*
	* @param TypeDescription the type description of this input type
	*/
	@Override
	public void setTypeDescription(java.lang.String TypeDescription) {
		_inputType.setTypeDescription(TypeDescription);
	}

	/**
	* Sets the type ID of this input type.
	*
	* @param TypeID the type ID of this input type
	*/
	@Override
	public void setTypeID(int TypeID) {
		_inputType.setTypeID(TypeID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InputTypeWrapper)) {
			return false;
		}

		InputTypeWrapper inputTypeWrapper = (InputTypeWrapper)obj;

		if (Objects.equals(_inputType, inputTypeWrapper._inputType)) {
			return true;
		}

		return false;
	}

	@Override
	public InputType getWrappedModel() {
		return _inputType;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _inputType.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _inputType.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_inputType.resetOriginalValues();
	}

	private final InputType _inputType;
}