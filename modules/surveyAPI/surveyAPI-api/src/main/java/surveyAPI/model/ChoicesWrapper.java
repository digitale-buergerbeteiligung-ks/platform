/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Choices}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Choices
 * @generated
 */
@ProviderType
public class ChoicesWrapper implements Choices, ModelWrapper<Choices> {
	public ChoicesWrapper(Choices choices) {
		_choices = choices;
	}

	@Override
	public Class<?> getModelClass() {
		return Choices.class;
	}

	@Override
	public String getModelClassName() {
		return Choices.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ChoiceID", getChoiceID());
		attributes.put("ChoiceText", getChoiceText());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ChoiceID = (Long)attributes.get("ChoiceID");

		if (ChoiceID != null) {
			setChoiceID(ChoiceID);
		}

		String ChoiceText = (String)attributes.get("ChoiceText");

		if (ChoiceText != null) {
			setChoiceText(ChoiceText);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _choices.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _choices.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _choices.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _choices.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.Choices> toCacheModel() {
		return _choices.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.Choices choices) {
		return _choices.compareTo(choices);
	}

	@Override
	public int hashCode() {
		return _choices.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _choices.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ChoicesWrapper((Choices)_choices.clone());
	}

	/**
	* Returns the choice text of this choices.
	*
	* @return the choice text of this choices
	*/
	@Override
	public java.lang.String getChoiceText() {
		return _choices.getChoiceText();
	}

	@Override
	public java.lang.String toString() {
		return _choices.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _choices.toXmlString();
	}

	/**
	* Returns the choice ID of this choices.
	*
	* @return the choice ID of this choices
	*/
	@Override
	public long getChoiceID() {
		return _choices.getChoiceID();
	}

	/**
	* Returns the primary key of this choices.
	*
	* @return the primary key of this choices
	*/
	@Override
	public long getPrimaryKey() {
		return _choices.getPrimaryKey();
	}

	@Override
	public surveyAPI.model.Choices toEscapedModel() {
		return new ChoicesWrapper(_choices.toEscapedModel());
	}

	@Override
	public surveyAPI.model.Choices toUnescapedModel() {
		return new ChoicesWrapper(_choices.toUnescapedModel());
	}

	@Override
	public void persist() {
		_choices.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_choices.setCachedModel(cachedModel);
	}

	/**
	* Sets the choice ID of this choices.
	*
	* @param ChoiceID the choice ID of this choices
	*/
	@Override
	public void setChoiceID(long ChoiceID) {
		_choices.setChoiceID(ChoiceID);
	}

	/**
	* Sets the choice text of this choices.
	*
	* @param ChoiceText the choice text of this choices
	*/
	@Override
	public void setChoiceText(java.lang.String ChoiceText) {
		_choices.setChoiceText(ChoiceText);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_choices.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_choices.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_choices.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_choices.setNew(n);
	}

	/**
	* Sets the primary key of this choices.
	*
	* @param primaryKey the primary key of this choices
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_choices.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_choices.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ChoicesWrapper)) {
			return false;
		}

		ChoicesWrapper choicesWrapper = (ChoicesWrapper)obj;

		if (Objects.equals(_choices, choicesWrapper._choices)) {
			return true;
		}

		return false;
	}

	@Override
	public Choices getWrappedModel() {
		return _choices;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _choices.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _choices.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_choices.resetOriginalValues();
	}

	private final Choices _choices;
}