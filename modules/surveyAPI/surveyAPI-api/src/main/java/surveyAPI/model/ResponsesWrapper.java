/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Responses}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Responses
 * @generated
 */
@ProviderType
public class ResponsesWrapper implements Responses, ModelWrapper<Responses> {
	public ResponsesWrapper(Responses responses) {
		_responses = responses;
	}

	@Override
	public Class<?> getModelClass() {
		return Responses.class;
	}

	@Override
	public String getModelClassName() {
		return Responses.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ResponseID", getResponseID());
		attributes.put("UserID", getUserID());
		attributes.put("OptionID", getOptionID());
		attributes.put("ResponseText", getResponseText());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ResponseID = (Long)attributes.get("ResponseID");

		if (ResponseID != null) {
			setResponseID(ResponseID);
		}

		Long UserID = (Long)attributes.get("UserID");

		if (UserID != null) {
			setUserID(UserID);
		}

		Long OptionID = (Long)attributes.get("OptionID");

		if (OptionID != null) {
			setOptionID(OptionID);
		}

		String ResponseText = (String)attributes.get("ResponseText");

		if (ResponseText != null) {
			setResponseText(ResponseText);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _responses.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _responses.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _responses.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _responses.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.Responses> toCacheModel() {
		return _responses.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.Responses responses) {
		return _responses.compareTo(responses);
	}

	@Override
	public int hashCode() {
		return _responses.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _responses.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ResponsesWrapper((Responses)_responses.clone());
	}

	/**
	* Returns the response text of this responses.
	*
	* @return the response text of this responses
	*/
	@Override
	public java.lang.String getResponseText() {
		return _responses.getResponseText();
	}

	@Override
	public java.lang.String toString() {
		return _responses.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _responses.toXmlString();
	}

	/**
	* Returns the option ID of this responses.
	*
	* @return the option ID of this responses
	*/
	@Override
	public long getOptionID() {
		return _responses.getOptionID();
	}

	/**
	* Returns the primary key of this responses.
	*
	* @return the primary key of this responses
	*/
	@Override
	public long getPrimaryKey() {
		return _responses.getPrimaryKey();
	}

	/**
	* Returns the response ID of this responses.
	*
	* @return the response ID of this responses
	*/
	@Override
	public long getResponseID() {
		return _responses.getResponseID();
	}

	/**
	* Returns the user ID of this responses.
	*
	* @return the user ID of this responses
	*/
	@Override
	public long getUserID() {
		return _responses.getUserID();
	}

	@Override
	public surveyAPI.model.Responses toEscapedModel() {
		return new ResponsesWrapper(_responses.toEscapedModel());
	}

	@Override
	public surveyAPI.model.Responses toUnescapedModel() {
		return new ResponsesWrapper(_responses.toUnescapedModel());
	}

	@Override
	public void persist() {
		_responses.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_responses.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_responses.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_responses.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_responses.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_responses.setNew(n);
	}

	/**
	* Sets the option ID of this responses.
	*
	* @param OptionID the option ID of this responses
	*/
	@Override
	public void setOptionID(long OptionID) {
		_responses.setOptionID(OptionID);
	}

	/**
	* Sets the primary key of this responses.
	*
	* @param primaryKey the primary key of this responses
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_responses.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_responses.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the response ID of this responses.
	*
	* @param ResponseID the response ID of this responses
	*/
	@Override
	public void setResponseID(long ResponseID) {
		_responses.setResponseID(ResponseID);
	}

	/**
	* Sets the response text of this responses.
	*
	* @param ResponseText the response text of this responses
	*/
	@Override
	public void setResponseText(java.lang.String ResponseText) {
		_responses.setResponseText(ResponseText);
	}

	/**
	* Sets the user ID of this responses.
	*
	* @param UserID the user ID of this responses
	*/
	@Override
	public void setUserID(long UserID) {
		_responses.setUserID(UserID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ResponsesWrapper)) {
			return false;
		}

		ResponsesWrapper responsesWrapper = (ResponsesWrapper)obj;

		if (Objects.equals(_responses, responsesWrapper._responses)) {
			return true;
		}

		return false;
	}

	@Override
	public Responses getWrappedModel() {
		return _responses;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _responses.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _responses.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_responses.resetOriginalValues();
	}

	private final Responses _responses;
}