/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.InputTypeServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.InputTypeServiceSoap
 * @generated
 */
@ProviderType
public class InputTypeSoap implements Serializable {
	public static InputTypeSoap toSoapModel(InputType model) {
		InputTypeSoap soapModel = new InputTypeSoap();

		soapModel.setTypeID(model.getTypeID());
		soapModel.setTypeDescription(model.getTypeDescription());

		return soapModel;
	}

	public static InputTypeSoap[] toSoapModels(InputType[] models) {
		InputTypeSoap[] soapModels = new InputTypeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InputTypeSoap[][] toSoapModels(InputType[][] models) {
		InputTypeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new InputTypeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new InputTypeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InputTypeSoap[] toSoapModels(List<InputType> models) {
		List<InputTypeSoap> soapModels = new ArrayList<InputTypeSoap>(models.size());

		for (InputType model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InputTypeSoap[soapModels.size()]);
	}

	public InputTypeSoap() {
	}

	public int getPrimaryKey() {
		return _TypeID;
	}

	public void setPrimaryKey(int pk) {
		setTypeID(pk);
	}

	public int getTypeID() {
		return _TypeID;
	}

	public void setTypeID(int TypeID) {
		_TypeID = TypeID;
	}

	public String getTypeDescription() {
		return _TypeDescription;
	}

	public void setTypeDescription(String TypeDescription) {
		_TypeDescription = TypeDescription;
	}

	private int _TypeID;
	private String _TypeDescription;
}