<%@include file="../../init.jsp"%>

<%
Ideas idea = (Ideas)request.getAttribute("IDEA_ideas");
idea = idea.toEscapedModel();
%>
<dl>
		<img src=<%=IdeasLocalServiceUtil.getIdeas(idea.getIdeasId()).getTitleImgRef() %> style="padding:10px;"></img>
        <dt>Idea</dt>
        <dd><%= IdeasLocalServiceUtil.getIdeas(idea.getIdeasId()).getTitle() %></dd>
        <dt>Short description</dt>
        <dd><%= idea.getShortdescription() %></dd>
        <dt>Description</dt>
        <dd><%= idea.getDescription() %></dd>
</dl>