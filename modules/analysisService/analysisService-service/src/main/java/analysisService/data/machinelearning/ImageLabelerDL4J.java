//package analysisService.data.machinelearning;
//import java.io.File;
//import java.io.IOException;
//
//import org.datavec.image.loader.ImageLoader;
//import org.deeplearning4j.nn.graph.ComputationGraph;
//import org.deeplearning4j.nn.modelimport.keras.trainedmodels.TrainedModels;
//import org.deeplearning4j.zoo.PretrainedType;
//import org.deeplearning4j.zoo.ZooModel;
//import org.deeplearning4j.zoo.model.VGG16;
//import org.nd4j.linalg.api.ndarray.INDArray;
//import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
//import org.nd4j.linalg.dataset.api.preprocessor.VGG16ImagePreProcessor;
//
//public class ImageLabelerDL4J {
//
//	private ComputationGraph vgg16;
//
//	public ImageLabelerDL4J(){
//		ZooModel zooModel = new VGG16();
//		try {
//			ComputationGraph vgg16 = (ComputationGraph) zooModel.initPretrained(PretrainedType.IMAGENET);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public String predictFile(File file) throws IOException{
//        // Convert file to INDArray
//        ImageLoader loader = new ImageLoader(224, 224, 3);
//        INDArray image = loader.asMatrix(file);
//
//        // delete the physical file
//        file.delete();
//
//        // Mean subtraction pre-processing step for VGG
//        DataNormalization scaler = new VGG16ImagePreProcessor();
//        scaler.transform(image);
//
//        //Inference returns array of INDArray, index[0] has the predictions
//        INDArray[] output = vgg16.output(false,image);
//
//        // convert 1000 length numeric index of probabilities per label
//        // to sorted return top 5 convert to string using helper function VGG16.decodePredictions
//        // "predictions" is string of our results
//        String predictions = TrainedModels.VGG16.decodePredictions(output[0]);
//
//        // return results along with form to run another inference
//        return predictions;
//	}
//
//
//}
