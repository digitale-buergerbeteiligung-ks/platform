package analysisService.data.mining;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  a class containing utility methods mainly for text analysis
 */
public class Utils {

	    /**
	     * @param doc  list of strings
	     * @param term String represents a term
	     * @return term frequency of term in document
	     */
	    private double tf(List<String> doc, String term) {
	        double result = 0;
	        for (String word : doc) {
	            if (term.equalsIgnoreCase(word))
	                result++;
	        }
	        return result / doc.size();
	    }

	    /**
	     * @param docs list of list of strings represents the dataset
	     * @param term String represents a term
	     * @return the inverse term frequency of term in documents
	     */
	    private double idf(List<List<String>> docs, String term) {
	        double n = 0;
	        for (List<String> doc : docs) {
	            for (String word : doc) {
	                if (term.equalsIgnoreCase(word)) {
	                    n++;
	                    break;
	                }
	            }
	        }
	        return Math.log(docs.size() / n);
	    }

	    /**
	     * @param doc  a text document
	     * @param docs all documents
	     * @param term term
	     * @return the TF-IDF of term
	     */
	    public double tfIdf(List<String> doc, List<List<String>> docs, String term) {
	        return tf(doc, term) * idf(docs, term);

	    }

	    /**
	     * @param input document as a list of strings
	     * @return a bag of words representation of the input
	     */
	    public Map<String,Integer> listToBagOfWords(List<String> input){
	    	//MAP
	    	HashMap<String, ArrayList<String>> mapStep = new HashMap<String, ArrayList<String>>();
	    	for (String word : input) {
	    		if(mapStep.containsKey(word)){
	    			ArrayList<String> tmp = mapStep.get(word);
	    			mapStep.put(word, tmp);
	    		}
	    		else{
	    			mapStep.put(word, new ArrayList<String>());
	    		}
	    	}

	    	//REDUCE
	    	HashMap<String,Integer> result = new HashMap<String,Integer>();
	    	for (Map.Entry<String, ArrayList<String>> entry : mapStep.entrySet())
	    	{
	    	   result.put(entry.getKey(), entry.getValue().size());
	    	}
	    	return result;
	    }

	    //USAGE EXAMPLE
//	    public static void main(String[] args) {
//
//	        List<String> doc1 = Arrays.asList("Lorem", "ipsum", "dolor", "ipsum", "sit", "ipsum");
//	        List<String> doc2 = Arrays.asList("Vituperata", "incorrupte", "at", "ipsum", "pro", "quo");
//	        List<String> doc3 = Arrays.asList("Has", "persius", "disputationi", "id", "simul");
//	        List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);
//
//	        TFIDFCalculator calculator = new TFIDFCalculator();
//	        double tfidf = calculator.tfIdf(doc1, documents, "ipsum");
//	        System.out.println("TF-IDF (ipsum) = " + tfidf);
//
//
//	    }

}
