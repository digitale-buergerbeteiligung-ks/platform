/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.ClickCount;

import analysisService.service.persistence.ClickCountPK;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ClickCount in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ClickCount
 * @generated
 */
@ProviderType
public class ClickCountCacheModel implements CacheModel<ClickCount>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClickCountCacheModel)) {
			return false;
		}

		ClickCountCacheModel clickCountCacheModel = (ClickCountCacheModel)obj;

		if (clickCountPK.equals(clickCountCacheModel.clickCountPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, clickCountPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{GroupID=");
		sb.append(GroupID);
		sb.append(", ButtonID=");
		sb.append(ButtonID);
		sb.append(", Count=");
		sb.append(Count);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ClickCount toEntityModel() {
		ClickCountImpl clickCountImpl = new ClickCountImpl();

		if (GroupID == null) {
			clickCountImpl.setGroupID(StringPool.BLANK);
		}
		else {
			clickCountImpl.setGroupID(GroupID);
		}

		if (ButtonID == null) {
			clickCountImpl.setButtonID(StringPool.BLANK);
		}
		else {
			clickCountImpl.setButtonID(ButtonID);
		}

		clickCountImpl.setCount(Count);

		clickCountImpl.resetOriginalValues();

		return clickCountImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		GroupID = objectInput.readUTF();
		ButtonID = objectInput.readUTF();

		Count = objectInput.readInt();

		clickCountPK = new ClickCountPK(GroupID, ButtonID);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (GroupID == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(GroupID);
		}

		if (ButtonID == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ButtonID);
		}

		objectOutput.writeInt(Count);
	}

	public String GroupID;
	public String ButtonID;
	public int Count;
	public transient ClickCountPK clickCountPK;
}