/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.TagData;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing TagData in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see TagData
 * @generated
 */
@ProviderType
public class TagDataCacheModel implements CacheModel<TagData>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TagDataCacheModel)) {
			return false;
		}

		TagDataCacheModel tagDataCacheModel = (TagDataCacheModel)obj;

		if (TagID == tagDataCacheModel.TagID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, TagID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{TagID=");
		sb.append(TagID);
		sb.append(", Tag=");
		sb.append(Tag);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TagData toEntityModel() {
		TagDataImpl tagDataImpl = new TagDataImpl();

		tagDataImpl.setTagID(TagID);

		if (Tag == null) {
			tagDataImpl.setTag(StringPool.BLANK);
		}
		else {
			tagDataImpl.setTag(Tag);
		}

		tagDataImpl.resetOriginalValues();

		return tagDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		TagID = objectInput.readLong();
		Tag = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(TagID);

		if (Tag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Tag);
		}
	}

	public long TagID;
	public String Tag;
}