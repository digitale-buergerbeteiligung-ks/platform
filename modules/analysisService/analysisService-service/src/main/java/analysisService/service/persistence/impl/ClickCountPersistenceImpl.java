/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchClickCountException;

import analysisService.model.ClickCount;

import analysisService.model.impl.ClickCountImpl;
import analysisService.model.impl.ClickCountModelImpl;

import analysisService.service.persistence.ClickCountPK;
import analysisService.service.persistence.ClickCountPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the click count service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClickCountPersistence
 * @see analysisService.service.persistence.ClickCountUtil
 * @generated
 */
@ProviderType
public class ClickCountPersistenceImpl extends BasePersistenceImpl<ClickCount>
	implements ClickCountPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ClickCountUtil} to access the click count persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ClickCountImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
			ClickCountModelImpl.FINDER_CACHE_ENABLED, ClickCountImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
			ClickCountModelImpl.FINDER_CACHE_ENABLED, ClickCountImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
			ClickCountModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ClickCountPersistenceImpl() {
		setModelClass(ClickCount.class);
	}

	/**
	 * Caches the click count in the entity cache if it is enabled.
	 *
	 * @param clickCount the click count
	 */
	@Override
	public void cacheResult(ClickCount clickCount) {
		entityCache.putResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
			ClickCountImpl.class, clickCount.getPrimaryKey(), clickCount);

		clickCount.resetOriginalValues();
	}

	/**
	 * Caches the click counts in the entity cache if it is enabled.
	 *
	 * @param clickCounts the click counts
	 */
	@Override
	public void cacheResult(List<ClickCount> clickCounts) {
		for (ClickCount clickCount : clickCounts) {
			if (entityCache.getResult(
						ClickCountModelImpl.ENTITY_CACHE_ENABLED,
						ClickCountImpl.class, clickCount.getPrimaryKey()) == null) {
				cacheResult(clickCount);
			}
			else {
				clickCount.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all click counts.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ClickCountImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the click count.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ClickCount clickCount) {
		entityCache.removeResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
			ClickCountImpl.class, clickCount.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ClickCount> clickCounts) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ClickCount clickCount : clickCounts) {
			entityCache.removeResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
				ClickCountImpl.class, clickCount.getPrimaryKey());
		}
	}

	/**
	 * Creates a new click count with the primary key. Does not add the click count to the database.
	 *
	 * @param clickCountPK the primary key for the new click count
	 * @return the new click count
	 */
	@Override
	public ClickCount create(ClickCountPK clickCountPK) {
		ClickCount clickCount = new ClickCountImpl();

		clickCount.setNew(true);
		clickCount.setPrimaryKey(clickCountPK);

		return clickCount;
	}

	/**
	 * Removes the click count with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param clickCountPK the primary key of the click count
	 * @return the click count that was removed
	 * @throws NoSuchClickCountException if a click count with the primary key could not be found
	 */
	@Override
	public ClickCount remove(ClickCountPK clickCountPK)
		throws NoSuchClickCountException {
		return remove((Serializable)clickCountPK);
	}

	/**
	 * Removes the click count with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the click count
	 * @return the click count that was removed
	 * @throws NoSuchClickCountException if a click count with the primary key could not be found
	 */
	@Override
	public ClickCount remove(Serializable primaryKey)
		throws NoSuchClickCountException {
		Session session = null;

		try {
			session = openSession();

			ClickCount clickCount = (ClickCount)session.get(ClickCountImpl.class,
					primaryKey);

			if (clickCount == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchClickCountException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clickCount);
		}
		catch (NoSuchClickCountException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ClickCount removeImpl(ClickCount clickCount) {
		clickCount = toUnwrappedModel(clickCount);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clickCount)) {
				clickCount = (ClickCount)session.get(ClickCountImpl.class,
						clickCount.getPrimaryKeyObj());
			}

			if (clickCount != null) {
				session.delete(clickCount);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clickCount != null) {
			clearCache(clickCount);
		}

		return clickCount;
	}

	@Override
	public ClickCount updateImpl(ClickCount clickCount) {
		clickCount = toUnwrappedModel(clickCount);

		boolean isNew = clickCount.isNew();

		Session session = null;

		try {
			session = openSession();

			if (clickCount.isNew()) {
				session.save(clickCount);

				clickCount.setNew(false);
			}
			else {
				clickCount = (ClickCount)session.merge(clickCount);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
			ClickCountImpl.class, clickCount.getPrimaryKey(), clickCount, false);

		clickCount.resetOriginalValues();

		return clickCount;
	}

	protected ClickCount toUnwrappedModel(ClickCount clickCount) {
		if (clickCount instanceof ClickCountImpl) {
			return clickCount;
		}

		ClickCountImpl clickCountImpl = new ClickCountImpl();

		clickCountImpl.setNew(clickCount.isNew());
		clickCountImpl.setPrimaryKey(clickCount.getPrimaryKey());

		clickCountImpl.setGroupID(clickCount.getGroupID());
		clickCountImpl.setButtonID(clickCount.getButtonID());
		clickCountImpl.setCount(clickCount.getCount());

		return clickCountImpl;
	}

	/**
	 * Returns the click count with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the click count
	 * @return the click count
	 * @throws NoSuchClickCountException if a click count with the primary key could not be found
	 */
	@Override
	public ClickCount findByPrimaryKey(Serializable primaryKey)
		throws NoSuchClickCountException {
		ClickCount clickCount = fetchByPrimaryKey(primaryKey);

		if (clickCount == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchClickCountException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clickCount;
	}

	/**
	 * Returns the click count with the primary key or throws a {@link NoSuchClickCountException} if it could not be found.
	 *
	 * @param clickCountPK the primary key of the click count
	 * @return the click count
	 * @throws NoSuchClickCountException if a click count with the primary key could not be found
	 */
	@Override
	public ClickCount findByPrimaryKey(ClickCountPK clickCountPK)
		throws NoSuchClickCountException {
		return findByPrimaryKey((Serializable)clickCountPK);
	}

	/**
	 * Returns the click count with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the click count
	 * @return the click count, or <code>null</code> if a click count with the primary key could not be found
	 */
	@Override
	public ClickCount fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
				ClickCountImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ClickCount clickCount = (ClickCount)serializable;

		if (clickCount == null) {
			Session session = null;

			try {
				session = openSession();

				clickCount = (ClickCount)session.get(ClickCountImpl.class,
						primaryKey);

				if (clickCount != null) {
					cacheResult(clickCount);
				}
				else {
					entityCache.putResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
						ClickCountImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ClickCountModelImpl.ENTITY_CACHE_ENABLED,
					ClickCountImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clickCount;
	}

	/**
	 * Returns the click count with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param clickCountPK the primary key of the click count
	 * @return the click count, or <code>null</code> if a click count with the primary key could not be found
	 */
	@Override
	public ClickCount fetchByPrimaryKey(ClickCountPK clickCountPK) {
		return fetchByPrimaryKey((Serializable)clickCountPK);
	}

	@Override
	public Map<Serializable, ClickCount> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ClickCount> map = new HashMap<Serializable, ClickCount>();

		for (Serializable primaryKey : primaryKeys) {
			ClickCount clickCount = fetchByPrimaryKey(primaryKey);

			if (clickCount != null) {
				map.put(primaryKey, clickCount);
			}
		}

		return map;
	}

	/**
	 * Returns all the click counts.
	 *
	 * @return the click counts
	 */
	@Override
	public List<ClickCount> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the click counts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of click counts
	 * @param end the upper bound of the range of click counts (not inclusive)
	 * @return the range of click counts
	 */
	@Override
	public List<ClickCount> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the click counts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of click counts
	 * @param end the upper bound of the range of click counts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of click counts
	 */
	@Override
	public List<ClickCount> findAll(int start, int end,
		OrderByComparator<ClickCount> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the click counts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of click counts
	 * @param end the upper bound of the range of click counts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of click counts
	 */
	@Override
	public List<ClickCount> findAll(int start, int end,
		OrderByComparator<ClickCount> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ClickCount> list = null;

		if (retrieveFromCache) {
			list = (List<ClickCount>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CLICKCOUNT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLICKCOUNT;

				if (pagination) {
					sql = sql.concat(ClickCountModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ClickCount>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ClickCount>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the click counts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ClickCount clickCount : findAll()) {
			remove(clickCount);
		}
	}

	/**
	 * Returns the number of click counts.
	 *
	 * @return the number of click counts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLICKCOUNT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ClickCountModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the click count persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ClickCountImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CLICKCOUNT = "SELECT clickCount FROM ClickCount clickCount";
	private static final String _SQL_COUNT_CLICKCOUNT = "SELECT COUNT(clickCount) FROM ClickCount clickCount";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clickCount.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ClickCount exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(ClickCountPersistenceImpl.class);
}