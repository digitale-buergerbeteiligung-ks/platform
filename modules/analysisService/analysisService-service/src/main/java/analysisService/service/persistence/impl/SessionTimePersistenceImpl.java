/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchSessionTimeException;

import analysisService.model.SessionTime;

import analysisService.model.impl.SessionTimeImpl;
import analysisService.model.impl.SessionTimeModelImpl;

import analysisService.service.persistence.SessionTimePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the session time service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SessionTimePersistence
 * @see analysisService.service.persistence.SessionTimeUtil
 * @generated
 */
@ProviderType
public class SessionTimePersistenceImpl extends BasePersistenceImpl<SessionTime>
	implements SessionTimePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SessionTimeUtil} to access the session time persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SessionTimeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
			SessionTimeModelImpl.FINDER_CACHE_ENABLED, SessionTimeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
			SessionTimeModelImpl.FINDER_CACHE_ENABLED, SessionTimeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
			SessionTimeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public SessionTimePersistenceImpl() {
		setModelClass(SessionTime.class);
	}

	/**
	 * Caches the session time in the entity cache if it is enabled.
	 *
	 * @param sessionTime the session time
	 */
	@Override
	public void cacheResult(SessionTime sessionTime) {
		entityCache.putResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
			SessionTimeImpl.class, sessionTime.getPrimaryKey(), sessionTime);

		sessionTime.resetOriginalValues();
	}

	/**
	 * Caches the session times in the entity cache if it is enabled.
	 *
	 * @param sessionTimes the session times
	 */
	@Override
	public void cacheResult(List<SessionTime> sessionTimes) {
		for (SessionTime sessionTime : sessionTimes) {
			if (entityCache.getResult(
						SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
						SessionTimeImpl.class, sessionTime.getPrimaryKey()) == null) {
				cacheResult(sessionTime);
			}
			else {
				sessionTime.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all session times.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SessionTimeImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the session time.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SessionTime sessionTime) {
		entityCache.removeResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
			SessionTimeImpl.class, sessionTime.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<SessionTime> sessionTimes) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SessionTime sessionTime : sessionTimes) {
			entityCache.removeResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
				SessionTimeImpl.class, sessionTime.getPrimaryKey());
		}
	}

	/**
	 * Creates a new session time with the primary key. Does not add the session time to the database.
	 *
	 * @param SessionID the primary key for the new session time
	 * @return the new session time
	 */
	@Override
	public SessionTime create(long SessionID) {
		SessionTime sessionTime = new SessionTimeImpl();

		sessionTime.setNew(true);
		sessionTime.setPrimaryKey(SessionID);

		return sessionTime;
	}

	/**
	 * Removes the session time with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param SessionID the primary key of the session time
	 * @return the session time that was removed
	 * @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	 */
	@Override
	public SessionTime remove(long SessionID) throws NoSuchSessionTimeException {
		return remove((Serializable)SessionID);
	}

	/**
	 * Removes the session time with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the session time
	 * @return the session time that was removed
	 * @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	 */
	@Override
	public SessionTime remove(Serializable primaryKey)
		throws NoSuchSessionTimeException {
		Session session = null;

		try {
			session = openSession();

			SessionTime sessionTime = (SessionTime)session.get(SessionTimeImpl.class,
					primaryKey);

			if (sessionTime == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSessionTimeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(sessionTime);
		}
		catch (NoSuchSessionTimeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SessionTime removeImpl(SessionTime sessionTime) {
		sessionTime = toUnwrappedModel(sessionTime);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(sessionTime)) {
				sessionTime = (SessionTime)session.get(SessionTimeImpl.class,
						sessionTime.getPrimaryKeyObj());
			}

			if (sessionTime != null) {
				session.delete(sessionTime);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (sessionTime != null) {
			clearCache(sessionTime);
		}

		return sessionTime;
	}

	@Override
	public SessionTime updateImpl(SessionTime sessionTime) {
		sessionTime = toUnwrappedModel(sessionTime);

		boolean isNew = sessionTime.isNew();

		Session session = null;

		try {
			session = openSession();

			if (sessionTime.isNew()) {
				session.save(sessionTime);

				sessionTime.setNew(false);
			}
			else {
				sessionTime = (SessionTime)session.merge(sessionTime);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
			SessionTimeImpl.class, sessionTime.getPrimaryKey(), sessionTime,
			false);

		sessionTime.resetOriginalValues();

		return sessionTime;
	}

	protected SessionTime toUnwrappedModel(SessionTime sessionTime) {
		if (sessionTime instanceof SessionTimeImpl) {
			return sessionTime;
		}

		SessionTimeImpl sessionTimeImpl = new SessionTimeImpl();

		sessionTimeImpl.setNew(sessionTime.isNew());
		sessionTimeImpl.setPrimaryKey(sessionTime.getPrimaryKey());

		sessionTimeImpl.setSessionID(sessionTime.getSessionID());
		sessionTimeImpl.setGroupID(sessionTime.getGroupID());
		sessionTimeImpl.setStartTime(sessionTime.getStartTime());
		sessionTimeImpl.setEndTime(sessionTime.getEndTime());

		return sessionTimeImpl;
	}

	/**
	 * Returns the session time with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the session time
	 * @return the session time
	 * @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	 */
	@Override
	public SessionTime findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSessionTimeException {
		SessionTime sessionTime = fetchByPrimaryKey(primaryKey);

		if (sessionTime == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSessionTimeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return sessionTime;
	}

	/**
	 * Returns the session time with the primary key or throws a {@link NoSuchSessionTimeException} if it could not be found.
	 *
	 * @param SessionID the primary key of the session time
	 * @return the session time
	 * @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	 */
	@Override
	public SessionTime findByPrimaryKey(long SessionID)
		throws NoSuchSessionTimeException {
		return findByPrimaryKey((Serializable)SessionID);
	}

	/**
	 * Returns the session time with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the session time
	 * @return the session time, or <code>null</code> if a session time with the primary key could not be found
	 */
	@Override
	public SessionTime fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
				SessionTimeImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		SessionTime sessionTime = (SessionTime)serializable;

		if (sessionTime == null) {
			Session session = null;

			try {
				session = openSession();

				sessionTime = (SessionTime)session.get(SessionTimeImpl.class,
						primaryKey);

				if (sessionTime != null) {
					cacheResult(sessionTime);
				}
				else {
					entityCache.putResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
						SessionTimeImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
					SessionTimeImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return sessionTime;
	}

	/**
	 * Returns the session time with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param SessionID the primary key of the session time
	 * @return the session time, or <code>null</code> if a session time with the primary key could not be found
	 */
	@Override
	public SessionTime fetchByPrimaryKey(long SessionID) {
		return fetchByPrimaryKey((Serializable)SessionID);
	}

	@Override
	public Map<Serializable, SessionTime> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, SessionTime> map = new HashMap<Serializable, SessionTime>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			SessionTime sessionTime = fetchByPrimaryKey(primaryKey);

			if (sessionTime != null) {
				map.put(primaryKey, sessionTime);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
					SessionTimeImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (SessionTime)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SESSIONTIME_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (SessionTime sessionTime : (List<SessionTime>)q.list()) {
				map.put(sessionTime.getPrimaryKeyObj(), sessionTime);

				cacheResult(sessionTime);

				uncachedPrimaryKeys.remove(sessionTime.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(SessionTimeModelImpl.ENTITY_CACHE_ENABLED,
					SessionTimeImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the session times.
	 *
	 * @return the session times
	 */
	@Override
	public List<SessionTime> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the session times.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of session times
	 * @param end the upper bound of the range of session times (not inclusive)
	 * @return the range of session times
	 */
	@Override
	public List<SessionTime> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the session times.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of session times
	 * @param end the upper bound of the range of session times (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of session times
	 */
	@Override
	public List<SessionTime> findAll(int start, int end,
		OrderByComparator<SessionTime> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the session times.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of session times
	 * @param end the upper bound of the range of session times (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of session times
	 */
	@Override
	public List<SessionTime> findAll(int start, int end,
		OrderByComparator<SessionTime> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SessionTime> list = null;

		if (retrieveFromCache) {
			list = (List<SessionTime>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SESSIONTIME);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SESSIONTIME;

				if (pagination) {
					sql = sql.concat(SessionTimeModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<SessionTime>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SessionTime>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the session times from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (SessionTime sessionTime : findAll()) {
			remove(sessionTime);
		}
	}

	/**
	 * Returns the number of session times.
	 *
	 * @return the number of session times
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SESSIONTIME);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SessionTimeModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the session time persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(SessionTimeImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SESSIONTIME = "SELECT sessionTime FROM SessionTime sessionTime";
	private static final String _SQL_SELECT_SESSIONTIME_WHERE_PKS_IN = "SELECT sessionTime FROM SessionTime sessionTime WHERE SessionID IN (";
	private static final String _SQL_COUNT_SESSIONTIME = "SELECT COUNT(sessionTime) FROM SessionTime sessionTime";
	private static final String _ORDER_BY_ENTITY_ALIAS = "sessionTime.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SessionTime exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(SessionTimePersistenceImpl.class);
}