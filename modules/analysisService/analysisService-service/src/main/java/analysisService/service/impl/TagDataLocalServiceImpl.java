/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.impl;

import java.util.List;

import analysisService.exception.NoSuchTagDataException;
import analysisService.model.TagData;
import analysisService.model.impl.TagDataImpl;
import analysisService.service.TagDataLocalServiceUtil;
import analysisService.service.base.TagDataLocalServiceBaseImpl;
import analysisService.service.persistence.TagDataUtil;

/**
 * The implementation of the tag data local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link analysisService.service.TagDataLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TagDataLocalServiceBaseImpl
 * @see analysisService.service.TagDataLocalServiceUtil
 */
public class TagDataLocalServiceImpl extends TagDataLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link analysisService.service.TagDataLocalServiceUtil} to access the tag data local service.
	 */
	/**
	 * adds Tag String to the Tag table
	 * auto increments the Tag ID - PK
	 */
	public long addNewTag(String tag){
		TagData newTagData = new TagDataImpl();
		newTagData.setTag(tag);

		TagDataLocalServiceUtil.addTagData(newTagData);
		return newTagData.getPrimaryKey();
	}

	/**
	 * Can be used to check for duplicate strings in the Tag table
	 * returns true if new tag is already present
	 */
	public boolean findTagData(String tag){
		try{
			TagDataUtil.findByTagName(tag);
		}catch(NoSuchTagDataException ex){
			return false;
		}
		return true;
	}

	public long getTagIdByTagName (String name){
		List<TagData> allTags = TagDataUtil.findAll();
		for(TagData d : allTags){
			if(d.getTag().equals(name)){
				return d.getPrimaryKey();
			}
		}
		return -1;
	}
}