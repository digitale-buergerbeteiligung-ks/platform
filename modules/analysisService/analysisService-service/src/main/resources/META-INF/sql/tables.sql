create table ANALYSIS_Analysis (
	dataId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	input VARCHAR(75) null,
	result VARCHAR(75) null,
	algorithmApplied VARCHAR(75) null,
	timeExecuted LONG
);

create table ANALYSIS_ClickCount (
	GroupID VARCHAR(75) not null,
	ButtonID VARCHAR(75) not null,
	Count INTEGER,
	primary key (GroupID, ButtonID)
);

create table ANALYSIS_SessionTime (
	SessionID LONG not null primary key,
	GroupID VARCHAR(75) null,
	StartTime DATE null,
	EndTime DATE null
);

create table ANALYSIS_TagData (
	TagID LONG not null primary key,
	Tag VARCHAR(75) null
);