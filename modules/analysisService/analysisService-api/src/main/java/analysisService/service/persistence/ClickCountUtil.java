/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.ClickCount;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the click count service. This utility wraps {@link analysisService.service.persistence.impl.ClickCountPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClickCountPersistence
 * @see analysisService.service.persistence.impl.ClickCountPersistenceImpl
 * @generated
 */
@ProviderType
public class ClickCountUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ClickCount clickCount) {
		getPersistence().clearCache(clickCount);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ClickCount> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ClickCount> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ClickCount> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ClickCount> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ClickCount update(ClickCount clickCount) {
		return getPersistence().update(clickCount);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ClickCount update(ClickCount clickCount,
		ServiceContext serviceContext) {
		return getPersistence().update(clickCount, serviceContext);
	}

	/**
	* Caches the click count in the entity cache if it is enabled.
	*
	* @param clickCount the click count
	*/
	public static void cacheResult(ClickCount clickCount) {
		getPersistence().cacheResult(clickCount);
	}

	/**
	* Caches the click counts in the entity cache if it is enabled.
	*
	* @param clickCounts the click counts
	*/
	public static void cacheResult(List<ClickCount> clickCounts) {
		getPersistence().cacheResult(clickCounts);
	}

	/**
	* Creates a new click count with the primary key. Does not add the click count to the database.
	*
	* @param clickCountPK the primary key for the new click count
	* @return the new click count
	*/
	public static ClickCount create(
		analysisService.service.persistence.ClickCountPK clickCountPK) {
		return getPersistence().create(clickCountPK);
	}

	/**
	* Removes the click count with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count that was removed
	* @throws NoSuchClickCountException if a click count with the primary key could not be found
	*/
	public static ClickCount remove(
		analysisService.service.persistence.ClickCountPK clickCountPK)
		throws analysisService.exception.NoSuchClickCountException {
		return getPersistence().remove(clickCountPK);
	}

	public static ClickCount updateImpl(ClickCount clickCount) {
		return getPersistence().updateImpl(clickCount);
	}

	/**
	* Returns the click count with the primary key or throws a {@link NoSuchClickCountException} if it could not be found.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count
	* @throws NoSuchClickCountException if a click count with the primary key could not be found
	*/
	public static ClickCount findByPrimaryKey(
		analysisService.service.persistence.ClickCountPK clickCountPK)
		throws analysisService.exception.NoSuchClickCountException {
		return getPersistence().findByPrimaryKey(clickCountPK);
	}

	/**
	* Returns the click count with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count, or <code>null</code> if a click count with the primary key could not be found
	*/
	public static ClickCount fetchByPrimaryKey(
		analysisService.service.persistence.ClickCountPK clickCountPK) {
		return getPersistence().fetchByPrimaryKey(clickCountPK);
	}

	public static java.util.Map<java.io.Serializable, ClickCount> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the click counts.
	*
	* @return the click counts
	*/
	public static List<ClickCount> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @return the range of click counts
	*/
	public static List<ClickCount> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of click counts
	*/
	public static List<ClickCount> findAll(int start, int end,
		OrderByComparator<ClickCount> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of click counts
	*/
	public static List<ClickCount> findAll(int start, int end,
		OrderByComparator<ClickCount> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the click counts from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of click counts.
	*
	* @return the number of click counts
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ClickCountPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ClickCountPersistence, ClickCountPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ClickCountPersistence.class);
}