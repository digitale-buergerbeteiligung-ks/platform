/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ClickCountPK implements Comparable<ClickCountPK>, Serializable {
	public String GroupID;
	public String ButtonID;

	public ClickCountPK() {
	}

	public ClickCountPK(String GroupID, String ButtonID) {
		this.GroupID = GroupID;
		this.ButtonID = ButtonID;
	}

	public String getGroupID() {
		return GroupID;
	}

	public void setGroupID(String GroupID) {
		this.GroupID = GroupID;
	}

	public String getButtonID() {
		return ButtonID;
	}

	public void setButtonID(String ButtonID) {
		this.ButtonID = ButtonID;
	}

	@Override
	public int compareTo(ClickCountPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		value = GroupID.compareTo(pk.GroupID);

		if (value != 0) {
			return value;
		}

		value = ButtonID.compareTo(pk.ButtonID);

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClickCountPK)) {
			return false;
		}

		ClickCountPK pk = (ClickCountPK)obj;

		if ((GroupID.equals(pk.GroupID)) && (ButtonID.equals(pk.ButtonID))) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, GroupID);
		hashCode = HashUtil.hash(hashCode, ButtonID);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("GroupID");
		sb.append(StringPool.EQUAL);
		sb.append(GroupID);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("ButtonID");
		sb.append(StringPool.EQUAL);
		sb.append(ButtonID);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}