/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AnalysisLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AnalysisLocalService
 * @generated
 */
@ProviderType
public class AnalysisLocalServiceWrapper implements AnalysisLocalService,
	ServiceWrapper<AnalysisLocalService> {
	public AnalysisLocalServiceWrapper(
		AnalysisLocalService analysisLocalService) {
		_analysisLocalService = analysisLocalService;
	}

	/**
	* Adds the analysis to the database. Also notifies the appropriate model listeners.
	*
	* @param analysis the analysis
	* @return the analysis that was added
	*/
	@Override
	public analysisService.model.Analysis addAnalysis(
		analysisService.model.Analysis analysis) {
		return _analysisLocalService.addAnalysis(analysis);
	}

	/**
	* Creates a new analysis with the primary key. Does not add the analysis to the database.
	*
	* @param dataId the primary key for the new analysis
	* @return the new analysis
	*/
	@Override
	public analysisService.model.Analysis createAnalysis(long dataId) {
		return _analysisLocalService.createAnalysis(dataId);
	}

	/**
	* Deletes the analysis from the database. Also notifies the appropriate model listeners.
	*
	* @param analysis the analysis
	* @return the analysis that was removed
	*/
	@Override
	public analysisService.model.Analysis deleteAnalysis(
		analysisService.model.Analysis analysis) {
		return _analysisLocalService.deleteAnalysis(analysis);
	}

	/**
	* Deletes the analysis with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis that was removed
	* @throws PortalException if a analysis with the primary key could not be found
	*/
	@Override
	public analysisService.model.Analysis deleteAnalysis(long dataId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _analysisLocalService.deleteAnalysis(dataId);
	}

	@Override
	public analysisService.model.Analysis fetchAnalysis(long dataId) {
		return _analysisLocalService.fetchAnalysis(dataId);
	}

	/**
	* Returns the analysis with the primary key.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis
	* @throws PortalException if a analysis with the primary key could not be found
	*/
	@Override
	public analysisService.model.Analysis getAnalysis(long dataId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _analysisLocalService.getAnalysis(dataId);
	}

	/**
	* Updates the analysis in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param analysis the analysis
	* @return the analysis that was updated
	*/
	@Override
	public analysisService.model.Analysis updateAnalysis(
		analysisService.model.Analysis analysis) {
		return _analysisLocalService.updateAnalysis(analysis);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _analysisLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _analysisLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _analysisLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _analysisLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _analysisLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of analysises.
	*
	* @return the number of analysises
	*/
	@Override
	public int getAnalysisesCount() {
		return _analysisLocalService.getAnalysisesCount();
	}

	@Override
	public int predictImage(java.io.File file, byte[] model) {
		return _analysisLocalService.predictImage(file, model);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _analysisLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _analysisLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _analysisLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _analysisLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @return the range of analysises
	*/
	@Override
	public java.util.List<analysisService.model.Analysis> getAnalysises(
		int start, int end) {
		return _analysisLocalService.getAnalysises(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _analysisLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _analysisLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public AnalysisLocalService getWrappedService() {
		return _analysisLocalService;
	}

	@Override
	public void setWrappedService(AnalysisLocalService analysisLocalService) {
		_analysisLocalService = analysisLocalService;
	}

	private AnalysisLocalService _analysisLocalService;
}