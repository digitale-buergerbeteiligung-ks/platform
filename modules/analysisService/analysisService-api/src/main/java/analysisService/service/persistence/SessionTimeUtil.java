/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.SessionTime;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the session time service. This utility wraps {@link analysisService.service.persistence.impl.SessionTimePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SessionTimePersistence
 * @see analysisService.service.persistence.impl.SessionTimePersistenceImpl
 * @generated
 */
@ProviderType
public class SessionTimeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(SessionTime sessionTime) {
		getPersistence().clearCache(sessionTime);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SessionTime> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SessionTime> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SessionTime> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<SessionTime> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static SessionTime update(SessionTime sessionTime) {
		return getPersistence().update(sessionTime);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static SessionTime update(SessionTime sessionTime,
		ServiceContext serviceContext) {
		return getPersistence().update(sessionTime, serviceContext);
	}

	/**
	* Caches the session time in the entity cache if it is enabled.
	*
	* @param sessionTime the session time
	*/
	public static void cacheResult(SessionTime sessionTime) {
		getPersistence().cacheResult(sessionTime);
	}

	/**
	* Caches the session times in the entity cache if it is enabled.
	*
	* @param sessionTimes the session times
	*/
	public static void cacheResult(List<SessionTime> sessionTimes) {
		getPersistence().cacheResult(sessionTimes);
	}

	/**
	* Creates a new session time with the primary key. Does not add the session time to the database.
	*
	* @param SessionID the primary key for the new session time
	* @return the new session time
	*/
	public static SessionTime create(long SessionID) {
		return getPersistence().create(SessionID);
	}

	/**
	* Removes the session time with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SessionID the primary key of the session time
	* @return the session time that was removed
	* @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	*/
	public static SessionTime remove(long SessionID)
		throws analysisService.exception.NoSuchSessionTimeException {
		return getPersistence().remove(SessionID);
	}

	public static SessionTime updateImpl(SessionTime sessionTime) {
		return getPersistence().updateImpl(sessionTime);
	}

	/**
	* Returns the session time with the primary key or throws a {@link NoSuchSessionTimeException} if it could not be found.
	*
	* @param SessionID the primary key of the session time
	* @return the session time
	* @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	*/
	public static SessionTime findByPrimaryKey(long SessionID)
		throws analysisService.exception.NoSuchSessionTimeException {
		return getPersistence().findByPrimaryKey(SessionID);
	}

	/**
	* Returns the session time with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param SessionID the primary key of the session time
	* @return the session time, or <code>null</code> if a session time with the primary key could not be found
	*/
	public static SessionTime fetchByPrimaryKey(long SessionID) {
		return getPersistence().fetchByPrimaryKey(SessionID);
	}

	public static java.util.Map<java.io.Serializable, SessionTime> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the session times.
	*
	* @return the session times
	*/
	public static List<SessionTime> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @return the range of session times
	*/
	public static List<SessionTime> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of session times
	*/
	public static List<SessionTime> findAll(int start, int end,
		OrderByComparator<SessionTime> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of session times
	*/
	public static List<SessionTime> findAll(int start, int end,
		OrderByComparator<SessionTime> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the session times from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of session times.
	*
	* @return the number of session times
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static SessionTimePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SessionTimePersistence, SessionTimePersistence> _serviceTracker =
		ServiceTrackerFactory.open(SessionTimePersistence.class);
}