/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchTagDataException;

import analysisService.model.TagData;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the tag data service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.persistence.impl.TagDataPersistenceImpl
 * @see TagDataUtil
 * @generated
 */
@ProviderType
public interface TagDataPersistence extends BasePersistence<TagData> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TagDataUtil} to access the tag data persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the tag data where Tag = &#63; or throws a {@link NoSuchTagDataException} if it could not be found.
	*
	* @param Tag the tag
	* @return the matching tag data
	* @throws NoSuchTagDataException if a matching tag data could not be found
	*/
	public TagData findByTagName(java.lang.String Tag)
		throws NoSuchTagDataException;

	/**
	* Returns the tag data where Tag = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param Tag the tag
	* @return the matching tag data, or <code>null</code> if a matching tag data could not be found
	*/
	public TagData fetchByTagName(java.lang.String Tag);

	/**
	* Returns the tag data where Tag = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param Tag the tag
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching tag data, or <code>null</code> if a matching tag data could not be found
	*/
	public TagData fetchByTagName(java.lang.String Tag,
		boolean retrieveFromCache);

	/**
	* Removes the tag data where Tag = &#63; from the database.
	*
	* @param Tag the tag
	* @return the tag data that was removed
	*/
	public TagData removeByTagName(java.lang.String Tag)
		throws NoSuchTagDataException;

	/**
	* Returns the number of tag datas where Tag = &#63;.
	*
	* @param Tag the tag
	* @return the number of matching tag datas
	*/
	public int countByTagName(java.lang.String Tag);

	/**
	* Caches the tag data in the entity cache if it is enabled.
	*
	* @param tagData the tag data
	*/
	public void cacheResult(TagData tagData);

	/**
	* Caches the tag datas in the entity cache if it is enabled.
	*
	* @param tagDatas the tag datas
	*/
	public void cacheResult(java.util.List<TagData> tagDatas);

	/**
	* Creates a new tag data with the primary key. Does not add the tag data to the database.
	*
	* @param TagID the primary key for the new tag data
	* @return the new tag data
	*/
	public TagData create(long TagID);

	/**
	* Removes the tag data with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param TagID the primary key of the tag data
	* @return the tag data that was removed
	* @throws NoSuchTagDataException if a tag data with the primary key could not be found
	*/
	public TagData remove(long TagID) throws NoSuchTagDataException;

	public TagData updateImpl(TagData tagData);

	/**
	* Returns the tag data with the primary key or throws a {@link NoSuchTagDataException} if it could not be found.
	*
	* @param TagID the primary key of the tag data
	* @return the tag data
	* @throws NoSuchTagDataException if a tag data with the primary key could not be found
	*/
	public TagData findByPrimaryKey(long TagID) throws NoSuchTagDataException;

	/**
	* Returns the tag data with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param TagID the primary key of the tag data
	* @return the tag data, or <code>null</code> if a tag data with the primary key could not be found
	*/
	public TagData fetchByPrimaryKey(long TagID);

	@Override
	public java.util.Map<java.io.Serializable, TagData> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the tag datas.
	*
	* @return the tag datas
	*/
	public java.util.List<TagData> findAll();

	/**
	* Returns a range of all the tag datas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tag datas
	* @param end the upper bound of the range of tag datas (not inclusive)
	* @return the range of tag datas
	*/
	public java.util.List<TagData> findAll(int start, int end);

	/**
	* Returns an ordered range of all the tag datas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tag datas
	* @param end the upper bound of the range of tag datas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tag datas
	*/
	public java.util.List<TagData> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TagData> orderByComparator);

	/**
	* Returns an ordered range of all the tag datas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tag datas
	* @param end the upper bound of the range of tag datas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of tag datas
	*/
	public java.util.List<TagData> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TagData> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the tag datas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of tag datas.
	*
	* @return the number of tag datas
	*/
	public int countAll();
}