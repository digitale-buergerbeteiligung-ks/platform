package analysisService.service.enums;

public enum ButtonPositions {
	FIRST("First"),MIDDLE("Middle"),LAST("Last");

	private final String buttonPositionDescription;

	private ButtonPositions(String s){
		buttonPositionDescription = s;
	}

	public String getButtonPositionDescription(){
		return buttonPositionDescription;
	}
}
