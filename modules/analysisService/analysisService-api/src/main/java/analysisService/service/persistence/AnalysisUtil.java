/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.Analysis;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the analysis service. This utility wraps {@link analysisService.service.persistence.impl.AnalysisPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AnalysisPersistence
 * @see analysisService.service.persistence.impl.AnalysisPersistenceImpl
 * @generated
 */
@ProviderType
public class AnalysisUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Analysis analysis) {
		getPersistence().clearCache(analysis);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Analysis> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Analysis> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Analysis> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Analysis> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Analysis update(Analysis analysis) {
		return getPersistence().update(analysis);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Analysis update(Analysis analysis,
		ServiceContext serviceContext) {
		return getPersistence().update(analysis, serviceContext);
	}

	/**
	* Returns the analysis where input = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param input the input
	* @return the matching analysis
	* @throws NoSuchAnalysisException if a matching analysis could not be found
	*/
	public static Analysis findByinput(java.lang.String input)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().findByinput(input);
	}

	/**
	* Returns the analysis where input = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param input the input
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public static Analysis fetchByinput(java.lang.String input) {
		return getPersistence().fetchByinput(input);
	}

	/**
	* Returns the analysis where input = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param input the input
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public static Analysis fetchByinput(java.lang.String input,
		boolean retrieveFromCache) {
		return getPersistence().fetchByinput(input, retrieveFromCache);
	}

	/**
	* Removes the analysis where input = &#63; from the database.
	*
	* @param input the input
	* @return the analysis that was removed
	*/
	public static Analysis removeByinput(java.lang.String input)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().removeByinput(input);
	}

	/**
	* Returns the number of analysises where input = &#63;.
	*
	* @param input the input
	* @return the number of matching analysises
	*/
	public static int countByinput(java.lang.String input) {
		return getPersistence().countByinput(input);
	}

	/**
	* Returns the analysis where algorithmApplied = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param algorithmApplied the algorithm applied
	* @return the matching analysis
	* @throws NoSuchAnalysisException if a matching analysis could not be found
	*/
	public static Analysis findByalgorithmApplied(
		java.lang.String algorithmApplied)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().findByalgorithmApplied(algorithmApplied);
	}

	/**
	* Returns the analysis where algorithmApplied = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param algorithmApplied the algorithm applied
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public static Analysis fetchByalgorithmApplied(
		java.lang.String algorithmApplied) {
		return getPersistence().fetchByalgorithmApplied(algorithmApplied);
	}

	/**
	* Returns the analysis where algorithmApplied = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param algorithmApplied the algorithm applied
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public static Analysis fetchByalgorithmApplied(
		java.lang.String algorithmApplied, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByalgorithmApplied(algorithmApplied, retrieveFromCache);
	}

	/**
	* Removes the analysis where algorithmApplied = &#63; from the database.
	*
	* @param algorithmApplied the algorithm applied
	* @return the analysis that was removed
	*/
	public static Analysis removeByalgorithmApplied(
		java.lang.String algorithmApplied)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().removeByalgorithmApplied(algorithmApplied);
	}

	/**
	* Returns the number of analysises where algorithmApplied = &#63;.
	*
	* @param algorithmApplied the algorithm applied
	* @return the number of matching analysises
	*/
	public static int countByalgorithmApplied(java.lang.String algorithmApplied) {
		return getPersistence().countByalgorithmApplied(algorithmApplied);
	}

	/**
	* Returns the analysis where timeExecuted = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param timeExecuted the time executed
	* @return the matching analysis
	* @throws NoSuchAnalysisException if a matching analysis could not be found
	*/
	public static Analysis findBytimeExecuted(long timeExecuted)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().findBytimeExecuted(timeExecuted);
	}

	/**
	* Returns the analysis where timeExecuted = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param timeExecuted the time executed
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public static Analysis fetchBytimeExecuted(long timeExecuted) {
		return getPersistence().fetchBytimeExecuted(timeExecuted);
	}

	/**
	* Returns the analysis where timeExecuted = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param timeExecuted the time executed
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public static Analysis fetchBytimeExecuted(long timeExecuted,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchBytimeExecuted(timeExecuted, retrieveFromCache);
	}

	/**
	* Removes the analysis where timeExecuted = &#63; from the database.
	*
	* @param timeExecuted the time executed
	* @return the analysis that was removed
	*/
	public static Analysis removeBytimeExecuted(long timeExecuted)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().removeBytimeExecuted(timeExecuted);
	}

	/**
	* Returns the number of analysises where timeExecuted = &#63;.
	*
	* @param timeExecuted the time executed
	* @return the number of matching analysises
	*/
	public static int countBytimeExecuted(long timeExecuted) {
		return getPersistence().countBytimeExecuted(timeExecuted);
	}

	/**
	* Caches the analysis in the entity cache if it is enabled.
	*
	* @param analysis the analysis
	*/
	public static void cacheResult(Analysis analysis) {
		getPersistence().cacheResult(analysis);
	}

	/**
	* Caches the analysises in the entity cache if it is enabled.
	*
	* @param analysises the analysises
	*/
	public static void cacheResult(List<Analysis> analysises) {
		getPersistence().cacheResult(analysises);
	}

	/**
	* Creates a new analysis with the primary key. Does not add the analysis to the database.
	*
	* @param dataId the primary key for the new analysis
	* @return the new analysis
	*/
	public static Analysis create(long dataId) {
		return getPersistence().create(dataId);
	}

	/**
	* Removes the analysis with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis that was removed
	* @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	*/
	public static Analysis remove(long dataId)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().remove(dataId);
	}

	public static Analysis updateImpl(Analysis analysis) {
		return getPersistence().updateImpl(analysis);
	}

	/**
	* Returns the analysis with the primary key or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis
	* @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	*/
	public static Analysis findByPrimaryKey(long dataId)
		throws analysisService.exception.NoSuchAnalysisException {
		return getPersistence().findByPrimaryKey(dataId);
	}

	/**
	* Returns the analysis with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis, or <code>null</code> if a analysis with the primary key could not be found
	*/
	public static Analysis fetchByPrimaryKey(long dataId) {
		return getPersistence().fetchByPrimaryKey(dataId);
	}

	public static java.util.Map<java.io.Serializable, Analysis> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the analysises.
	*
	* @return the analysises
	*/
	public static List<Analysis> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @return the range of analysises
	*/
	public static List<Analysis> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of analysises
	*/
	public static List<Analysis> findAll(int start, int end,
		OrderByComparator<Analysis> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of analysises
	*/
	public static List<Analysis> findAll(int start, int end,
		OrderByComparator<Analysis> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the analysises from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of analysises.
	*
	* @return the number of analysises
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AnalysisPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AnalysisPersistence, AnalysisPersistence> _serviceTracker =
		ServiceTrackerFactory.open(AnalysisPersistence.class);
}