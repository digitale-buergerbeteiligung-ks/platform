/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchClickCountException;

import analysisService.model.ClickCount;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the click count service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.persistence.impl.ClickCountPersistenceImpl
 * @see ClickCountUtil
 * @generated
 */
@ProviderType
public interface ClickCountPersistence extends BasePersistence<ClickCount> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ClickCountUtil} to access the click count persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the click count in the entity cache if it is enabled.
	*
	* @param clickCount the click count
	*/
	public void cacheResult(ClickCount clickCount);

	/**
	* Caches the click counts in the entity cache if it is enabled.
	*
	* @param clickCounts the click counts
	*/
	public void cacheResult(java.util.List<ClickCount> clickCounts);

	/**
	* Creates a new click count with the primary key. Does not add the click count to the database.
	*
	* @param clickCountPK the primary key for the new click count
	* @return the new click count
	*/
	public ClickCount create(
		analysisService.service.persistence.ClickCountPK clickCountPK);

	/**
	* Removes the click count with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count that was removed
	* @throws NoSuchClickCountException if a click count with the primary key could not be found
	*/
	public ClickCount remove(
		analysisService.service.persistence.ClickCountPK clickCountPK)
		throws NoSuchClickCountException;

	public ClickCount updateImpl(ClickCount clickCount);

	/**
	* Returns the click count with the primary key or throws a {@link NoSuchClickCountException} if it could not be found.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count
	* @throws NoSuchClickCountException if a click count with the primary key could not be found
	*/
	public ClickCount findByPrimaryKey(
		analysisService.service.persistence.ClickCountPK clickCountPK)
		throws NoSuchClickCountException;

	/**
	* Returns the click count with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count, or <code>null</code> if a click count with the primary key could not be found
	*/
	public ClickCount fetchByPrimaryKey(
		analysisService.service.persistence.ClickCountPK clickCountPK);

	@Override
	public java.util.Map<java.io.Serializable, ClickCount> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the click counts.
	*
	* @return the click counts
	*/
	public java.util.List<ClickCount> findAll();

	/**
	* Returns a range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @return the range of click counts
	*/
	public java.util.List<ClickCount> findAll(int start, int end);

	/**
	* Returns an ordered range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of click counts
	*/
	public java.util.List<ClickCount> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClickCount> orderByComparator);

	/**
	* Returns an ordered range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of click counts
	*/
	public java.util.List<ClickCount> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ClickCount> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the click counts from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of click counts.
	*
	* @return the number of click counts
	*/
	public int countAll();
}