/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.TagData;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the tag data service. This utility wraps {@link analysisService.service.persistence.impl.TagDataPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TagDataPersistence
 * @see analysisService.service.persistence.impl.TagDataPersistenceImpl
 * @generated
 */
@ProviderType
public class TagDataUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(TagData tagData) {
		getPersistence().clearCache(tagData);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TagData> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TagData> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TagData> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<TagData> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static TagData update(TagData tagData) {
		return getPersistence().update(tagData);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static TagData update(TagData tagData, ServiceContext serviceContext) {
		return getPersistence().update(tagData, serviceContext);
	}

	/**
	* Returns the tag data where Tag = &#63; or throws a {@link NoSuchTagDataException} if it could not be found.
	*
	* @param Tag the tag
	* @return the matching tag data
	* @throws NoSuchTagDataException if a matching tag data could not be found
	*/
	public static TagData findByTagName(java.lang.String Tag)
		throws analysisService.exception.NoSuchTagDataException {
		return getPersistence().findByTagName(Tag);
	}

	/**
	* Returns the tag data where Tag = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param Tag the tag
	* @return the matching tag data, or <code>null</code> if a matching tag data could not be found
	*/
	public static TagData fetchByTagName(java.lang.String Tag) {
		return getPersistence().fetchByTagName(Tag);
	}

	/**
	* Returns the tag data where Tag = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param Tag the tag
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching tag data, or <code>null</code> if a matching tag data could not be found
	*/
	public static TagData fetchByTagName(java.lang.String Tag,
		boolean retrieveFromCache) {
		return getPersistence().fetchByTagName(Tag, retrieveFromCache);
	}

	/**
	* Removes the tag data where Tag = &#63; from the database.
	*
	* @param Tag the tag
	* @return the tag data that was removed
	*/
	public static TagData removeByTagName(java.lang.String Tag)
		throws analysisService.exception.NoSuchTagDataException {
		return getPersistence().removeByTagName(Tag);
	}

	/**
	* Returns the number of tag datas where Tag = &#63;.
	*
	* @param Tag the tag
	* @return the number of matching tag datas
	*/
	public static int countByTagName(java.lang.String Tag) {
		return getPersistence().countByTagName(Tag);
	}

	/**
	* Caches the tag data in the entity cache if it is enabled.
	*
	* @param tagData the tag data
	*/
	public static void cacheResult(TagData tagData) {
		getPersistence().cacheResult(tagData);
	}

	/**
	* Caches the tag datas in the entity cache if it is enabled.
	*
	* @param tagDatas the tag datas
	*/
	public static void cacheResult(List<TagData> tagDatas) {
		getPersistence().cacheResult(tagDatas);
	}

	/**
	* Creates a new tag data with the primary key. Does not add the tag data to the database.
	*
	* @param TagID the primary key for the new tag data
	* @return the new tag data
	*/
	public static TagData create(long TagID) {
		return getPersistence().create(TagID);
	}

	/**
	* Removes the tag data with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param TagID the primary key of the tag data
	* @return the tag data that was removed
	* @throws NoSuchTagDataException if a tag data with the primary key could not be found
	*/
	public static TagData remove(long TagID)
		throws analysisService.exception.NoSuchTagDataException {
		return getPersistence().remove(TagID);
	}

	public static TagData updateImpl(TagData tagData) {
		return getPersistence().updateImpl(tagData);
	}

	/**
	* Returns the tag data with the primary key or throws a {@link NoSuchTagDataException} if it could not be found.
	*
	* @param TagID the primary key of the tag data
	* @return the tag data
	* @throws NoSuchTagDataException if a tag data with the primary key could not be found
	*/
	public static TagData findByPrimaryKey(long TagID)
		throws analysisService.exception.NoSuchTagDataException {
		return getPersistence().findByPrimaryKey(TagID);
	}

	/**
	* Returns the tag data with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param TagID the primary key of the tag data
	* @return the tag data, or <code>null</code> if a tag data with the primary key could not be found
	*/
	public static TagData fetchByPrimaryKey(long TagID) {
		return getPersistence().fetchByPrimaryKey(TagID);
	}

	public static java.util.Map<java.io.Serializable, TagData> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the tag datas.
	*
	* @return the tag datas
	*/
	public static List<TagData> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the tag datas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tag datas
	* @param end the upper bound of the range of tag datas (not inclusive)
	* @return the range of tag datas
	*/
	public static List<TagData> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the tag datas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tag datas
	* @param end the upper bound of the range of tag datas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tag datas
	*/
	public static List<TagData> findAll(int start, int end,
		OrderByComparator<TagData> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the tag datas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tag datas
	* @param end the upper bound of the range of tag datas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of tag datas
	*/
	public static List<TagData> findAll(int start, int end,
		OrderByComparator<TagData> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the tag datas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of tag datas.
	*
	* @return the number of tag datas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static TagDataPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TagDataPersistence, TagDataPersistence> _serviceTracker =
		ServiceTrackerFactory.open(TagDataPersistence.class);
}