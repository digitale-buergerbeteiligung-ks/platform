/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ClickCount}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ClickCount
 * @generated
 */
@ProviderType
public class ClickCountWrapper implements ClickCount, ModelWrapper<ClickCount> {
	public ClickCountWrapper(ClickCount clickCount) {
		_clickCount = clickCount;
	}

	@Override
	public Class<?> getModelClass() {
		return ClickCount.class;
	}

	@Override
	public String getModelClassName() {
		return ClickCount.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("GroupID", getGroupID());
		attributes.put("ButtonID", getButtonID());
		attributes.put("Count", getCount());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String GroupID = (String)attributes.get("GroupID");

		if (GroupID != null) {
			setGroupID(GroupID);
		}

		String ButtonID = (String)attributes.get("ButtonID");

		if (ButtonID != null) {
			setButtonID(ButtonID);
		}

		Integer Count = (Integer)attributes.get("Count");

		if (Count != null) {
			setCount(Count);
		}
	}

	@Override
	public analysisService.model.ClickCount toEscapedModel() {
		return new ClickCountWrapper(_clickCount.toEscapedModel());
	}

	@Override
	public analysisService.model.ClickCount toUnescapedModel() {
		return new ClickCountWrapper(_clickCount.toUnescapedModel());
	}

	/**
	* Returns the primary key of this click count.
	*
	* @return the primary key of this click count
	*/
	@Override
	public analysisService.service.persistence.ClickCountPK getPrimaryKey() {
		return _clickCount.getPrimaryKey();
	}

	@Override
	public boolean isCachedModel() {
		return _clickCount.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _clickCount.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _clickCount.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _clickCount.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<analysisService.model.ClickCount> toCacheModel() {
		return _clickCount.toCacheModel();
	}

	@Override
	public int compareTo(analysisService.model.ClickCount clickCount) {
		return _clickCount.compareTo(clickCount);
	}

	/**
	* Returns the count of this click count.
	*
	* @return the count of this click count
	*/
	@Override
	public int getCount() {
		return _clickCount.getCount();
	}

	@Override
	public int hashCode() {
		return _clickCount.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _clickCount.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ClickCountWrapper((ClickCount)_clickCount.clone());
	}

	/**
	* Returns the button ID of this click count.
	*
	* @return the button ID of this click count
	*/
	@Override
	public java.lang.String getButtonID() {
		return _clickCount.getButtonID();
	}

	/**
	* Returns the group ID of this click count.
	*
	* @return the group ID of this click count
	*/
	@Override
	public java.lang.String getGroupID() {
		return _clickCount.getGroupID();
	}

	@Override
	public java.lang.String toString() {
		return _clickCount.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clickCount.toXmlString();
	}

	@Override
	public void persist() {
		_clickCount.persist();
	}

	/**
	* Sets the button ID of this click count.
	*
	* @param ButtonID the button ID of this click count
	*/
	@Override
	public void setButtonID(java.lang.String ButtonID) {
		_clickCount.setButtonID(ButtonID);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clickCount.setCachedModel(cachedModel);
	}

	/**
	* Sets the count of this click count.
	*
	* @param Count the count of this click count
	*/
	@Override
	public void setCount(int Count) {
		_clickCount.setCount(Count);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_clickCount.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_clickCount.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_clickCount.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this click count.
	*
	* @param GroupID the group ID of this click count
	*/
	@Override
	public void setGroupID(java.lang.String GroupID) {
		_clickCount.setGroupID(GroupID);
	}

	@Override
	public void setNew(boolean n) {
		_clickCount.setNew(n);
	}

	/**
	* Sets the primary key of this click count.
	*
	* @param primaryKey the primary key of this click count
	*/
	@Override
	public void setPrimaryKey(
		analysisService.service.persistence.ClickCountPK primaryKey) {
		_clickCount.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_clickCount.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClickCountWrapper)) {
			return false;
		}

		ClickCountWrapper clickCountWrapper = (ClickCountWrapper)obj;

		if (Objects.equals(_clickCount, clickCountWrapper._clickCount)) {
			return true;
		}

		return false;
	}

	@Override
	public ClickCount getWrappedModel() {
		return _clickCount;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _clickCount.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _clickCount.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_clickCount.resetOriginalValues();
	}

	private final ClickCount _clickCount;
}