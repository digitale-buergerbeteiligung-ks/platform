/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Analysis}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Analysis
 * @generated
 */
@ProviderType
public class AnalysisWrapper implements Analysis, ModelWrapper<Analysis> {
	public AnalysisWrapper(Analysis analysis) {
		_analysis = analysis;
	}

	@Override
	public Class<?> getModelClass() {
		return Analysis.class;
	}

	@Override
	public String getModelClassName() {
		return Analysis.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("dataId", getDataId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("input", getInput());
		attributes.put("result", getResult());
		attributes.put("algorithmApplied", getAlgorithmApplied());
		attributes.put("timeExecuted", getTimeExecuted());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long dataId = (Long)attributes.get("dataId");

		if (dataId != null) {
			setDataId(dataId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String input = (String)attributes.get("input");

		if (input != null) {
			setInput(input);
		}

		String result = (String)attributes.get("result");

		if (result != null) {
			setResult(result);
		}

		String algorithmApplied = (String)attributes.get("algorithmApplied");

		if (algorithmApplied != null) {
			setAlgorithmApplied(algorithmApplied);
		}

		Long timeExecuted = (Long)attributes.get("timeExecuted");

		if (timeExecuted != null) {
			setTimeExecuted(timeExecuted);
		}
	}

	@Override
	public analysisService.model.Analysis toEscapedModel() {
		return new AnalysisWrapper(_analysis.toEscapedModel());
	}

	@Override
	public analysisService.model.Analysis toUnescapedModel() {
		return new AnalysisWrapper(_analysis.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _analysis.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _analysis.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _analysis.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _analysis.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<analysisService.model.Analysis> toCacheModel() {
		return _analysis.toCacheModel();
	}

	@Override
	public int compareTo(analysisService.model.Analysis analysis) {
		return _analysis.compareTo(analysis);
	}

	@Override
	public int hashCode() {
		return _analysis.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _analysis.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new AnalysisWrapper((Analysis)_analysis.clone());
	}

	/**
	* Returns the algorithm applied of this analysis.
	*
	* @return the algorithm applied of this analysis
	*/
	@Override
	public java.lang.String getAlgorithmApplied() {
		return _analysis.getAlgorithmApplied();
	}

	/**
	* Returns the input of this analysis.
	*
	* @return the input of this analysis
	*/
	@Override
	public java.lang.String getInput() {
		return _analysis.getInput();
	}

	/**
	* Returns the result of this analysis.
	*
	* @return the result of this analysis
	*/
	@Override
	public java.lang.String getResult() {
		return _analysis.getResult();
	}

	/**
	* Returns the user name of this analysis.
	*
	* @return the user name of this analysis
	*/
	@Override
	public java.lang.String getUserName() {
		return _analysis.getUserName();
	}

	/**
	* Returns the user uuid of this analysis.
	*
	* @return the user uuid of this analysis
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _analysis.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _analysis.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _analysis.toXmlString();
	}

	/**
	* Returns the create date of this analysis.
	*
	* @return the create date of this analysis
	*/
	@Override
	public Date getCreateDate() {
		return _analysis.getCreateDate();
	}

	/**
	* Returns the modified date of this analysis.
	*
	* @return the modified date of this analysis
	*/
	@Override
	public Date getModifiedDate() {
		return _analysis.getModifiedDate();
	}

	/**
	* Returns the company ID of this analysis.
	*
	* @return the company ID of this analysis
	*/
	@Override
	public long getCompanyId() {
		return _analysis.getCompanyId();
	}

	/**
	* Returns the data ID of this analysis.
	*
	* @return the data ID of this analysis
	*/
	@Override
	public long getDataId() {
		return _analysis.getDataId();
	}

	/**
	* Returns the group ID of this analysis.
	*
	* @return the group ID of this analysis
	*/
	@Override
	public long getGroupId() {
		return _analysis.getGroupId();
	}

	/**
	* Returns the primary key of this analysis.
	*
	* @return the primary key of this analysis
	*/
	@Override
	public long getPrimaryKey() {
		return _analysis.getPrimaryKey();
	}

	/**
	* Returns the time executed of this analysis.
	*
	* @return the time executed of this analysis
	*/
	@Override
	public long getTimeExecuted() {
		return _analysis.getTimeExecuted();
	}

	/**
	* Returns the user ID of this analysis.
	*
	* @return the user ID of this analysis
	*/
	@Override
	public long getUserId() {
		return _analysis.getUserId();
	}

	@Override
	public void persist() {
		_analysis.persist();
	}

	/**
	* Sets the algorithm applied of this analysis.
	*
	* @param algorithmApplied the algorithm applied of this analysis
	*/
	@Override
	public void setAlgorithmApplied(java.lang.String algorithmApplied) {
		_analysis.setAlgorithmApplied(algorithmApplied);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_analysis.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this analysis.
	*
	* @param companyId the company ID of this analysis
	*/
	@Override
	public void setCompanyId(long companyId) {
		_analysis.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this analysis.
	*
	* @param createDate the create date of this analysis
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_analysis.setCreateDate(createDate);
	}

	/**
	* Sets the data ID of this analysis.
	*
	* @param dataId the data ID of this analysis
	*/
	@Override
	public void setDataId(long dataId) {
		_analysis.setDataId(dataId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_analysis.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_analysis.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_analysis.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this analysis.
	*
	* @param groupId the group ID of this analysis
	*/
	@Override
	public void setGroupId(long groupId) {
		_analysis.setGroupId(groupId);
	}

	/**
	* Sets the input of this analysis.
	*
	* @param input the input of this analysis
	*/
	@Override
	public void setInput(java.lang.String input) {
		_analysis.setInput(input);
	}

	/**
	* Sets the modified date of this analysis.
	*
	* @param modifiedDate the modified date of this analysis
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_analysis.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_analysis.setNew(n);
	}

	/**
	* Sets the primary key of this analysis.
	*
	* @param primaryKey the primary key of this analysis
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_analysis.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_analysis.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the result of this analysis.
	*
	* @param result the result of this analysis
	*/
	@Override
	public void setResult(java.lang.String result) {
		_analysis.setResult(result);
	}

	/**
	* Sets the time executed of this analysis.
	*
	* @param timeExecuted the time executed of this analysis
	*/
	@Override
	public void setTimeExecuted(long timeExecuted) {
		_analysis.setTimeExecuted(timeExecuted);
	}

	/**
	* Sets the user ID of this analysis.
	*
	* @param userId the user ID of this analysis
	*/
	@Override
	public void setUserId(long userId) {
		_analysis.setUserId(userId);
	}

	/**
	* Sets the user name of this analysis.
	*
	* @param userName the user name of this analysis
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_analysis.setUserName(userName);
	}

	/**
	* Sets the user uuid of this analysis.
	*
	* @param userUuid the user uuid of this analysis
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_analysis.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AnalysisWrapper)) {
			return false;
		}

		AnalysisWrapper analysisWrapper = (AnalysisWrapper)obj;

		if (Objects.equals(_analysis, analysisWrapper._analysis)) {
			return true;
		}

		return false;
	}

	@Override
	public Analysis getWrappedModel() {
		return _analysis;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _analysis.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _analysis.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_analysis.resetOriginalValues();
	}

	private final Analysis _analysis;
}