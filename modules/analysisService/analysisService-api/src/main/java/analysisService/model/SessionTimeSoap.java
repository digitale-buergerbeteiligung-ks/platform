/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link analysisService.service.http.SessionTimeServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.http.SessionTimeServiceSoap
 * @generated
 */
@ProviderType
public class SessionTimeSoap implements Serializable {
	public static SessionTimeSoap toSoapModel(SessionTime model) {
		SessionTimeSoap soapModel = new SessionTimeSoap();

		soapModel.setSessionID(model.getSessionID());
		soapModel.setGroupID(model.getGroupID());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setEndTime(model.getEndTime());

		return soapModel;
	}

	public static SessionTimeSoap[] toSoapModels(SessionTime[] models) {
		SessionTimeSoap[] soapModels = new SessionTimeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SessionTimeSoap[][] toSoapModels(SessionTime[][] models) {
		SessionTimeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SessionTimeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SessionTimeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SessionTimeSoap[] toSoapModels(List<SessionTime> models) {
		List<SessionTimeSoap> soapModels = new ArrayList<SessionTimeSoap>(models.size());

		for (SessionTime model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SessionTimeSoap[soapModels.size()]);
	}

	public SessionTimeSoap() {
	}

	public long getPrimaryKey() {
		return _SessionID;
	}

	public void setPrimaryKey(long pk) {
		setSessionID(pk);
	}

	public long getSessionID() {
		return _SessionID;
	}

	public void setSessionID(long SessionID) {
		_SessionID = SessionID;
	}

	public String getGroupID() {
		return _GroupID;
	}

	public void setGroupID(String GroupID) {
		_GroupID = GroupID;
	}

	public Date getStartTime() {
		return _StartTime;
	}

	public void setStartTime(Date StartTime) {
		_StartTime = StartTime;
	}

	public Date getEndTime() {
		return _EndTime;
	}

	public void setEndTime(Date EndTime) {
		_EndTime = EndTime;
	}

	private long _SessionID;
	private String _GroupID;
	private Date _StartTime;
	private Date _EndTime;
}