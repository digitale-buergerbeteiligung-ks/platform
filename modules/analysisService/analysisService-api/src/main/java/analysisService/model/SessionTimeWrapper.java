/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link SessionTime}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SessionTime
 * @generated
 */
@ProviderType
public class SessionTimeWrapper implements SessionTime,
	ModelWrapper<SessionTime> {
	public SessionTimeWrapper(SessionTime sessionTime) {
		_sessionTime = sessionTime;
	}

	@Override
	public Class<?> getModelClass() {
		return SessionTime.class;
	}

	@Override
	public String getModelClassName() {
		return SessionTime.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("SessionID", getSessionID());
		attributes.put("GroupID", getGroupID());
		attributes.put("StartTime", getStartTime());
		attributes.put("EndTime", getEndTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long SessionID = (Long)attributes.get("SessionID");

		if (SessionID != null) {
			setSessionID(SessionID);
		}

		String GroupID = (String)attributes.get("GroupID");

		if (GroupID != null) {
			setGroupID(GroupID);
		}

		Date StartTime = (Date)attributes.get("StartTime");

		if (StartTime != null) {
			setStartTime(StartTime);
		}

		Date EndTime = (Date)attributes.get("EndTime");

		if (EndTime != null) {
			setEndTime(EndTime);
		}
	}

	@Override
	public analysisService.model.SessionTime toEscapedModel() {
		return new SessionTimeWrapper(_sessionTime.toEscapedModel());
	}

	@Override
	public analysisService.model.SessionTime toUnescapedModel() {
		return new SessionTimeWrapper(_sessionTime.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _sessionTime.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _sessionTime.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _sessionTime.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _sessionTime.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<analysisService.model.SessionTime> toCacheModel() {
		return _sessionTime.toCacheModel();
	}

	@Override
	public int compareTo(analysisService.model.SessionTime sessionTime) {
		return _sessionTime.compareTo(sessionTime);
	}

	@Override
	public int hashCode() {
		return _sessionTime.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _sessionTime.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SessionTimeWrapper((SessionTime)_sessionTime.clone());
	}

	/**
	* Returns the group ID of this session time.
	*
	* @return the group ID of this session time
	*/
	@Override
	public java.lang.String getGroupID() {
		return _sessionTime.getGroupID();
	}

	@Override
	public java.lang.String toString() {
		return _sessionTime.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _sessionTime.toXmlString();
	}

	/**
	* Returns the end time of this session time.
	*
	* @return the end time of this session time
	*/
	@Override
	public Date getEndTime() {
		return _sessionTime.getEndTime();
	}

	/**
	* Returns the start time of this session time.
	*
	* @return the start time of this session time
	*/
	@Override
	public Date getStartTime() {
		return _sessionTime.getStartTime();
	}

	/**
	* Returns the primary key of this session time.
	*
	* @return the primary key of this session time
	*/
	@Override
	public long getPrimaryKey() {
		return _sessionTime.getPrimaryKey();
	}

	/**
	* Returns the session ID of this session time.
	*
	* @return the session ID of this session time
	*/
	@Override
	public long getSessionID() {
		return _sessionTime.getSessionID();
	}

	@Override
	public void persist() {
		_sessionTime.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_sessionTime.setCachedModel(cachedModel);
	}

	/**
	* Sets the end time of this session time.
	*
	* @param EndTime the end time of this session time
	*/
	@Override
	public void setEndTime(Date EndTime) {
		_sessionTime.setEndTime(EndTime);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_sessionTime.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_sessionTime.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_sessionTime.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this session time.
	*
	* @param GroupID the group ID of this session time
	*/
	@Override
	public void setGroupID(java.lang.String GroupID) {
		_sessionTime.setGroupID(GroupID);
	}

	@Override
	public void setNew(boolean n) {
		_sessionTime.setNew(n);
	}

	/**
	* Sets the primary key of this session time.
	*
	* @param primaryKey the primary key of this session time
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_sessionTime.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_sessionTime.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the session ID of this session time.
	*
	* @param SessionID the session ID of this session time
	*/
	@Override
	public void setSessionID(long SessionID) {
		_sessionTime.setSessionID(SessionID);
	}

	/**
	* Sets the start time of this session time.
	*
	* @param StartTime the start time of this session time
	*/
	@Override
	public void setStartTime(Date StartTime) {
		_sessionTime.setStartTime(StartTime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SessionTimeWrapper)) {
			return false;
		}

		SessionTimeWrapper sessionTimeWrapper = (SessionTimeWrapper)obj;

		if (Objects.equals(_sessionTime, sessionTimeWrapper._sessionTime)) {
			return true;
		}

		return false;
	}

	@Override
	public SessionTime getWrappedModel() {
		return _sessionTime;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _sessionTime.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _sessionTime.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_sessionTime.resetOriginalValues();
	}

	private final SessionTime _sessionTime;
}