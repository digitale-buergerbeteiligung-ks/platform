/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link analysisService.service.http.TagDataServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.http.TagDataServiceSoap
 * @generated
 */
@ProviderType
public class TagDataSoap implements Serializable {
	public static TagDataSoap toSoapModel(TagData model) {
		TagDataSoap soapModel = new TagDataSoap();

		soapModel.setTagID(model.getTagID());
		soapModel.setTag(model.getTag());

		return soapModel;
	}

	public static TagDataSoap[] toSoapModels(TagData[] models) {
		TagDataSoap[] soapModels = new TagDataSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TagDataSoap[][] toSoapModels(TagData[][] models) {
		TagDataSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TagDataSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TagDataSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TagDataSoap[] toSoapModels(List<TagData> models) {
		List<TagDataSoap> soapModels = new ArrayList<TagDataSoap>(models.size());

		for (TagData model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TagDataSoap[soapModels.size()]);
	}

	public TagDataSoap() {
	}

	public long getPrimaryKey() {
		return _TagID;
	}

	public void setPrimaryKey(long pk) {
		setTagID(pk);
	}

	public long getTagID() {
		return _TagID;
	}

	public void setTagID(long TagID) {
		_TagID = TagID;
	}

	public String getTag() {
		return _Tag;
	}

	public void setTag(String Tag) {
		_Tag = Tag;
	}

	private long _TagID;
	private String _Tag;
}