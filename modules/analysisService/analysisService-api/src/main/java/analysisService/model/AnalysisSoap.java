/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link analysisService.service.http.AnalysisServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.http.AnalysisServiceSoap
 * @generated
 */
@ProviderType
public class AnalysisSoap implements Serializable {
	public static AnalysisSoap toSoapModel(Analysis model) {
		AnalysisSoap soapModel = new AnalysisSoap();

		soapModel.setDataId(model.getDataId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setInput(model.getInput());
		soapModel.setResult(model.getResult());
		soapModel.setAlgorithmApplied(model.getAlgorithmApplied());
		soapModel.setTimeExecuted(model.getTimeExecuted());

		return soapModel;
	}

	public static AnalysisSoap[] toSoapModels(Analysis[] models) {
		AnalysisSoap[] soapModels = new AnalysisSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AnalysisSoap[][] toSoapModels(Analysis[][] models) {
		AnalysisSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AnalysisSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AnalysisSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AnalysisSoap[] toSoapModels(List<Analysis> models) {
		List<AnalysisSoap> soapModels = new ArrayList<AnalysisSoap>(models.size());

		for (Analysis model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AnalysisSoap[soapModels.size()]);
	}

	public AnalysisSoap() {
	}

	public long getPrimaryKey() {
		return _dataId;
	}

	public void setPrimaryKey(long pk) {
		setDataId(pk);
	}

	public long getDataId() {
		return _dataId;
	}

	public void setDataId(long dataId) {
		_dataId = dataId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getInput() {
		return _input;
	}

	public void setInput(String input) {
		_input = input;
	}

	public String getResult() {
		return _result;
	}

	public void setResult(String result) {
		_result = result;
	}

	public String getAlgorithmApplied() {
		return _algorithmApplied;
	}

	public void setAlgorithmApplied(String algorithmApplied) {
		_algorithmApplied = algorithmApplied;
	}

	public long getTimeExecuted() {
		return _timeExecuted;
	}

	public void setTimeExecuted(long timeExecuted) {
		_timeExecuted = timeExecuted;
	}

	private long _dataId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _input;
	private String _result;
	private String _algorithmApplied;
	private long _timeExecuted;
}