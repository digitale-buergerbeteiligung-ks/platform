/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import analysisService.service.persistence.ClickCountPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link analysisService.service.http.ClickCountServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.http.ClickCountServiceSoap
 * @generated
 */
@ProviderType
public class ClickCountSoap implements Serializable {
	public static ClickCountSoap toSoapModel(ClickCount model) {
		ClickCountSoap soapModel = new ClickCountSoap();

		soapModel.setGroupID(model.getGroupID());
		soapModel.setButtonID(model.getButtonID());
		soapModel.setCount(model.getCount());

		return soapModel;
	}

	public static ClickCountSoap[] toSoapModels(ClickCount[] models) {
		ClickCountSoap[] soapModels = new ClickCountSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ClickCountSoap[][] toSoapModels(ClickCount[][] models) {
		ClickCountSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ClickCountSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ClickCountSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ClickCountSoap[] toSoapModels(List<ClickCount> models) {
		List<ClickCountSoap> soapModels = new ArrayList<ClickCountSoap>(models.size());

		for (ClickCount model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ClickCountSoap[soapModels.size()]);
	}

	public ClickCountSoap() {
	}

	public ClickCountPK getPrimaryKey() {
		return new ClickCountPK(_GroupID, _ButtonID);
	}

	public void setPrimaryKey(ClickCountPK pk) {
		setGroupID(pk.GroupID);
		setButtonID(pk.ButtonID);
	}

	public String getGroupID() {
		return _GroupID;
	}

	public void setGroupID(String GroupID) {
		_GroupID = GroupID;
	}

	public String getButtonID() {
		return _ButtonID;
	}

	public void setButtonID(String ButtonID) {
		_ButtonID = ButtonID;
	}

	public int getCount() {
		return _Count;
	}

	public void setCount(int Count) {
		_Count = Count;
	}

	private String _GroupID;
	private String _ButtonID;
	private int _Count;
}