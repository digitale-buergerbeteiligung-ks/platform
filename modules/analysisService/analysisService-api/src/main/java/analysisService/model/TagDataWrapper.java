/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link TagData}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TagData
 * @generated
 */
@ProviderType
public class TagDataWrapper implements TagData, ModelWrapper<TagData> {
	public TagDataWrapper(TagData tagData) {
		_tagData = tagData;
	}

	@Override
	public Class<?> getModelClass() {
		return TagData.class;
	}

	@Override
	public String getModelClassName() {
		return TagData.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("TagID", getTagID());
		attributes.put("Tag", getTag());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long TagID = (Long)attributes.get("TagID");

		if (TagID != null) {
			setTagID(TagID);
		}

		String Tag = (String)attributes.get("Tag");

		if (Tag != null) {
			setTag(Tag);
		}
	}

	@Override
	public analysisService.model.TagData toEscapedModel() {
		return new TagDataWrapper(_tagData.toEscapedModel());
	}

	@Override
	public analysisService.model.TagData toUnescapedModel() {
		return new TagDataWrapper(_tagData.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _tagData.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _tagData.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _tagData.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _tagData.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<analysisService.model.TagData> toCacheModel() {
		return _tagData.toCacheModel();
	}

	@Override
	public int compareTo(analysisService.model.TagData tagData) {
		return _tagData.compareTo(tagData);
	}

	@Override
	public int hashCode() {
		return _tagData.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _tagData.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new TagDataWrapper((TagData)_tagData.clone());
	}

	/**
	* Returns the tag of this tag data.
	*
	* @return the tag of this tag data
	*/
	@Override
	public java.lang.String getTag() {
		return _tagData.getTag();
	}

	@Override
	public java.lang.String toString() {
		return _tagData.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _tagData.toXmlString();
	}

	/**
	* Returns the primary key of this tag data.
	*
	* @return the primary key of this tag data
	*/
	@Override
	public long getPrimaryKey() {
		return _tagData.getPrimaryKey();
	}

	/**
	* Returns the tag ID of this tag data.
	*
	* @return the tag ID of this tag data
	*/
	@Override
	public long getTagID() {
		return _tagData.getTagID();
	}

	@Override
	public void persist() {
		_tagData.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_tagData.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_tagData.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_tagData.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_tagData.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_tagData.setNew(n);
	}

	/**
	* Sets the primary key of this tag data.
	*
	* @param primaryKey the primary key of this tag data
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_tagData.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_tagData.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the tag of this tag data.
	*
	* @param Tag the tag of this tag data
	*/
	@Override
	public void setTag(java.lang.String Tag) {
		_tagData.setTag(Tag);
	}

	/**
	* Sets the tag ID of this tag data.
	*
	* @param TagID the tag ID of this tag data
	*/
	@Override
	public void setTagID(long TagID) {
		_tagData.setTagID(TagID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TagDataWrapper)) {
			return false;
		}

		TagDataWrapper tagDataWrapper = (TagDataWrapper)obj;

		if (Objects.equals(_tagData, tagDataWrapper._tagData)) {
			return true;
		}

		return false;
	}

	@Override
	public TagData getWrappedModel() {
		return _tagData;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _tagData.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _tagData.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_tagData.resetOriginalValues();
	}

	private final TagData _tagData;
}