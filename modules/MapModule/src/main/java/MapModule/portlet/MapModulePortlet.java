package MapModule.portlet;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import MapModule.constants.MapModulePortletKeys;
import ideaService.model.Ideas;
import ideaService.service.IdeasLocalServiceUtil;
import projectService.service.ProjectLocalServiceUtil;

/**
 * @author englmeier
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.EEN",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=Ideenkarte",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MapModulePortletKeys.MapModule, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class MapModulePortlet extends MVCPortlet {

	private final static String OLD_IDEAS = "oldIdeas";
	private final static String CURRENT_IDEAS = "currentIdeas";

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String all_ideas = null;
		String old_ideas = null;
		String current_ideas = null;
		List<Ideas> all_ideasList = new ArrayList<Ideas>();
		List<Ideas> current_ideasList = new ArrayList<Ideas>();
		List<Ideas> old_ideasList = new ArrayList<Ideas>();
		
		User user;
		if (ProjectLocalServiceUtil.getProjectByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey()) != null) {
			long projectId = ProjectLocalServiceUtil.getProjectByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey())
					.getPrimaryKey();
			try {
				user = PortalUtil.getUser(renderRequest);
				for (Role role : user.getRoles()) {
					if (role.getName().equals(RoleConstants.ADMINISTRATOR)
							|| role.getName().equals(RoleConstants.PORTAL_CONTENT_REVIEWER)) {
						
						// Admins and ContentRevs have same viewing rights.
						all_ideasList = removeUnimportantData(IdeasLocalServiceUtil.getIdeasByUserRoleProjectId(
								RoleConstants.ADMINISTRATOR, user.getUserId(), projectId));
						for(Ideas idea : all_ideasList) {
							if(this.isIdeaInValidPeriod(idea)) {
								current_ideasList.add(idea);
							} else {
								old_ideasList.add(idea);
							}
						}
						
						all_ideas = JSONFactoryUtil.looseSerializeDeep(all_ideasList);
						old_ideas = JSONFactoryUtil.looseSerializeDeep(old_ideasList);
						current_ideas = JSONFactoryUtil.looseSerializeDeep(current_ideasList);
						break;
					} else if (role.getName().equals(RoleConstants.USER)) {
						all_ideasList = removeUnimportantData(IdeasLocalServiceUtil
								.getIdeasByUserRoleProjectId(RoleConstants.USER, user.getUserId(), projectId));
						for(Ideas idea : all_ideasList) {
							if(this.isIdeaInValidPeriod(idea)) {
								current_ideasList.add(idea);
							} else {
								old_ideasList.add(idea);
							}
						}
						
						all_ideas = JSONFactoryUtil.looseSerializeDeep(all_ideasList);
						old_ideas = JSONFactoryUtil.looseSerializeDeep(old_ideasList);
						current_ideas = JSONFactoryUtil.looseSerializeDeep(current_ideasList);
						break;
					}

				}
			} catch (Exception e) {
				all_ideasList = removeUnimportantData(
						IdeasLocalServiceUtil.getIdeasByUserRoleProjectId(RoleConstants.GUEST, -1, projectId));
				// case guest user
				for(Ideas idea : all_ideasList) {
					if(this.isIdeaInValidPeriod(idea)) {
						current_ideasList.add(idea);
					} else {
						old_ideasList.add(idea);
					}
				}
				
				all_ideas = JSONFactoryUtil.looseSerializeDeep(all_ideasList);
				old_ideas = JSONFactoryUtil.looseSerializeDeep(old_ideasList);
				current_ideas = JSONFactoryUtil.looseSerializeDeep(current_ideasList);
			}
			if (all_ideas.equals(null) || all_ideas.equals("") || all_ideas.equals("[]")) {
				// at least return all accepted ideas in any case
				all_ideasList = removeUnimportantData(IdeasLocalServiceUtil.getAllAccpetedIdeas());
				for(Ideas idea : all_ideasList) {
					if(this.isIdeaInValidPeriod(idea)) {
						current_ideasList.add(idea);
					} else {
						old_ideasList.add(idea);
					}
				}
				
				all_ideas = JSONFactoryUtil.looseSerializeDeep(all_ideasList);
				old_ideas = JSONFactoryUtil.looseSerializeDeep(old_ideasList);
				current_ideas = JSONFactoryUtil.looseSerializeDeep(current_ideasList);
			}
			renderRequest.setAttribute(OLD_IDEAS, old_ideas);
			renderRequest.setAttribute(CURRENT_IDEAS, current_ideas);

		} else if (IdeasLocalServiceUtil.getIdeasByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey()) != null) {
			List<Ideas> tmp = new ArrayList<Ideas>();
			Ideas i = IdeasLocalServiceUtil.getIdeasByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey());
			tmp.add(i);
			tmp = removeUnimportantData(tmp);
			
			for(Ideas idea : tmp) {
				if(this.isIdeaInValidPeriod(idea)) {
					current_ideasList.add(idea);
				} else {
					old_ideasList.add(idea);
				}
			}
			all_ideas = JSONFactoryUtil.looseSerializeDeep(tmp);
			old_ideas = JSONFactoryUtil.looseSerializeDeep(old_ideasList);
			current_ideas = JSONFactoryUtil.looseSerializeDeep(current_ideasList);

			renderRequest.setAttribute(OLD_IDEAS, old_ideas);
			renderRequest.setAttribute(CURRENT_IDEAS, current_ideas);
		} else {
			all_ideasList = removeUnimportantData(IdeasLocalServiceUtil.getAllAccpetedIdeas());
			for(Ideas idea : all_ideasList) {
				if(this.isIdeaInValidPeriod(idea)) {
					current_ideasList.add(idea);
				} else {
					old_ideasList.add(idea);
				}
			}
			
			all_ideas = JSONFactoryUtil.looseSerializeDeep(all_ideasList);
			old_ideas = JSONFactoryUtil.looseSerializeDeep(old_ideasList);
			current_ideas = JSONFactoryUtil.looseSerializeDeep(current_ideasList);
			
			
			renderRequest.setAttribute(OLD_IDEAS, old_ideas);
			renderRequest.setAttribute(CURRENT_IDEAS, current_ideas);
		}

		renderRequest.setAttribute(OLD_IDEAS, old_ideas);
		renderRequest.setAttribute(CURRENT_IDEAS, current_ideas);
		super.render(renderRequest, renderResponse);
	}

	private List<Ideas> removeUnimportantData(List<Ideas> ideas) {
		// some special chars are not properly escaped by the serialization so
		// all unimportant information is set to null.
		for (Ideas i : ideas) {
			i.setVideoUrl(null);
			i.setUuid(null);
			i.setTags(null);
			i.setGoal(null);
			i.setImportance(null);
			i.setPitch(null);
			i.setUserUuid(null);
			i.setSolution(null);
			i.setReviewStatus(null);
			i.setShortdescription(null);
			i.setDescription(null);
			i.setPitch(null);
			i.setStatus(0);
			i.setTargetAudience(null);
		}
		return ideas;
	}

	private boolean isIdeaInValidPeriod(Ideas i) {
		try {
			// get current date			 
			 Date currentDate = new Date();			
			
			//shift current date to the prev month (also changes year if month is JANUARY)
			Calendar calCurrent = Calendar.getInstance();
			calCurrent.setTime(currentDate);
			calCurrent.add(Calendar.MONTH, -1);
			currentDate = calCurrent.getTime();		
			
			//get current year
			int currentYear = this.getYearOfDate(currentDate);

			//get current month
			int currentMonth = this.getMonthOfDate(currentDate);

			Date createDateOfIdea = i.getCreateDate();
			
			//shift createDate of idea to previous month
			Calendar calIdea = Calendar.getInstance();
			calIdea.setTime(createDateOfIdea);
			calIdea.add(Calendar.MONTH, -1);
			createDateOfIdea = calIdea.getTime();	
			
			// get year of the idea
			int yearOfIdea = this.getYearOfDate(createDateOfIdea);

			// get month of idea in number and not in string
			int monthOfIdea = this.getMonthOfDate(createDateOfIdea);

			int[] ideaPeriod = this.getPeriod(monthOfIdea);

			int[] currentPeriod = this.getPeriod(currentMonth);

			// if the idea belongs to the current year and in the current
			// period, then it's available for voting
			if (yearOfIdea == currentYear && this.samePeriods(ideaPeriod, currentPeriod)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	private int getYearOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int yearOfIdea = cal.get(Calendar.YEAR);
		return yearOfIdea;
	}

	private int getMonthOfDate(Date date) {
		LocalDate localIdeaDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int monthOfIdea = localIdeaDate.getMonthValue();
		return monthOfIdea;
	}

	/**
	 * Return the period of the given month. The months of the year are split by
	 * two, defining one period.
	 * 
	 * @param month
	 *            The month to be tested
	 * @return The period that the month belongs to
	 */
	private int[] getPeriod(int month) {
		Map<String, int[]> monthPeriods = new HashMap<String, int[]>();
		monthPeriods.put("JAN_FEB", new int[] { 1, 2 });
		monthPeriods.put("MAR_APR", new int[] { 3, 4 });
		monthPeriods.put("MAY_JUN", new int[] { 5, 6 });
		monthPeriods.put("JUL_AUG", new int[] { 7, 8 });
		monthPeriods.put("SEPT_OCT", new int[] { 9, 10 });
		monthPeriods.put("NOV_DEC", new int[] { 11, 12 });

		int[] period = new int[] {};
		switch (month) {
		case 1:
			period = monthPeriods.get("JAN_FEB");
			break;
		case 2:
			period = monthPeriods.get("JAN_FEB");
			break;
		case 3:
			period = monthPeriods.get("MAR_APR");
			break;
		case 4:
			period = monthPeriods.get("MAR_APR");
			break;
		case 5:
			period = monthPeriods.get("MAY_JUN");
			break;
		case 6:
			period = monthPeriods.get("MAY_JUN");
			break;
		case 7:
			period = monthPeriods.get("JUL_AUG");
			break;
		case 8:
			period = monthPeriods.get("JUL_AUG");
			break;
		case 9:
			period = monthPeriods.get("SEPT_OCT");
			break;
		case 10:
			period = monthPeriods.get("SEPT_OCT");
			break;
		case 11:
			period = monthPeriods.get("NOV_DEC");
			break;
		case 12:
			period = monthPeriods.get("NOV_DEC");
			break;
		default:
			break;
		}
		return period;
	}

	/**
	 * Compare if the months of the two periods are the same.
	 * 
	 * @return true if periods are the same, false otherwise
	 */
	private boolean samePeriods(int[] period1, int[] period2) {
		if (period1[0] == period2[0] && period1[1] == period2[1]) {
			return true;
		}
		return false;
	}

}
