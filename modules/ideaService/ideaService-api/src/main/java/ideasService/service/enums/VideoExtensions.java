package ideasService.service.enums;

public enum VideoExtensions {
OGV("ogv"),MP4("mp4"),WEBM("webm"),ORIGINAL("original");

private final String reviewStatusDescription;

private VideoExtensions(String s){
	reviewStatusDescription = s;
}

public String getVideoExtensionDescription(){
	return reviewStatusDescription;
}


}
