/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchPicturesException;

import ideaService.model.Pictures;

/**
 * The persistence interface for the pictures service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.PicturesPersistenceImpl
 * @see PicturesUtil
 * @generated
 */
@ProviderType
public interface PicturesPersistence extends BasePersistence<Pictures> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PicturesUtil} to access the pictures persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the pictureses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the matching pictureses
	*/
	public java.util.List<Pictures> findByIdeasRef(long IdeasRef);

	/**
	* Returns a range of all the pictureses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of matching pictureses
	*/
	public java.util.List<Pictures> findByIdeasRef(long IdeasRef, int start,
		int end);

	/**
	* Returns an ordered range of all the pictureses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching pictureses
	*/
	public java.util.List<Pictures> findByIdeasRef(long IdeasRef, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Returns an ordered range of all the pictureses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching pictureses
	*/
	public java.util.List<Pictures> findByIdeasRef(long IdeasRef, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public Pictures findByIdeasRef_First(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException;

	/**
	* Returns the first pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public Pictures fetchByIdeasRef_First(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Returns the last pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public Pictures findByIdeasRef_Last(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException;

	/**
	* Returns the last pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public Pictures fetchByIdeasRef_Last(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Returns the pictureses before and after the current pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param PictureId the primary key of the current pictures
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next pictures
	* @throws NoSuchPicturesException if a pictures with the primary key could not be found
	*/
	public Pictures[] findByIdeasRef_PrevAndNext(long PictureId, long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException;

	/**
	* Removes all the pictureses where IdeasRef = &#63; from the database.
	*
	* @param IdeasRef the ideas ref
	*/
	public void removeByIdeasRef(long IdeasRef);

	/**
	* Returns the number of pictureses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the number of matching pictureses
	*/
	public int countByIdeasRef(long IdeasRef);

	/**
	* Returns the pictures where IdeasRef = &#63; and Position = &#63; or throws a {@link NoSuchPicturesException} if it could not be found.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public Pictures findByPositionIdeasRef(long IdeasRef, int Position)
		throws NoSuchPicturesException;

	/**
	* Returns the pictures where IdeasRef = &#63; and Position = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public Pictures fetchByPositionIdeasRef(long IdeasRef, int Position);

	/**
	* Returns the pictures where IdeasRef = &#63; and Position = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public Pictures fetchByPositionIdeasRef(long IdeasRef, int Position,
		boolean retrieveFromCache);

	/**
	* Removes the pictures where IdeasRef = &#63; and Position = &#63; from the database.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the pictures that was removed
	*/
	public Pictures removeByPositionIdeasRef(long IdeasRef, int Position)
		throws NoSuchPicturesException;

	/**
	* Returns the number of pictureses where IdeasRef = &#63; and Position = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the number of matching pictureses
	*/
	public int countByPositionIdeasRef(long IdeasRef, int Position);

	/**
	* Returns all the pictureses where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @return the matching pictureses
	*/
	public java.util.List<Pictures> findByPicturesId(long PictureId);

	/**
	* Returns a range of all the pictureses where PictureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param PictureId the picture ID
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of matching pictureses
	*/
	public java.util.List<Pictures> findByPicturesId(long PictureId, int start,
		int end);

	/**
	* Returns an ordered range of all the pictureses where PictureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param PictureId the picture ID
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching pictureses
	*/
	public java.util.List<Pictures> findByPicturesId(long PictureId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Returns an ordered range of all the pictureses where PictureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param PictureId the picture ID
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching pictureses
	*/
	public java.util.List<Pictures> findByPicturesId(long PictureId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public Pictures findByPicturesId_First(long PictureId,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException;

	/**
	* Returns the first pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public Pictures fetchByPicturesId_First(long PictureId,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Returns the last pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public Pictures findByPicturesId_Last(long PictureId,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException;

	/**
	* Returns the last pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public Pictures fetchByPicturesId_Last(long PictureId,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Removes all the pictureses where PictureId = &#63; from the database.
	*
	* @param PictureId the picture ID
	*/
	public void removeByPicturesId(long PictureId);

	/**
	* Returns the number of pictureses where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @return the number of matching pictureses
	*/
	public int countByPicturesId(long PictureId);

	/**
	* Caches the pictures in the entity cache if it is enabled.
	*
	* @param pictures the pictures
	*/
	public void cacheResult(Pictures pictures);

	/**
	* Caches the pictureses in the entity cache if it is enabled.
	*
	* @param pictureses the pictureses
	*/
	public void cacheResult(java.util.List<Pictures> pictureses);

	/**
	* Creates a new pictures with the primary key. Does not add the pictures to the database.
	*
	* @param PictureId the primary key for the new pictures
	* @return the new pictures
	*/
	public Pictures create(long PictureId);

	/**
	* Removes the pictures with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures that was removed
	* @throws NoSuchPicturesException if a pictures with the primary key could not be found
	*/
	public Pictures remove(long PictureId) throws NoSuchPicturesException;

	public Pictures updateImpl(Pictures pictures);

	/**
	* Returns the pictures with the primary key or throws a {@link NoSuchPicturesException} if it could not be found.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures
	* @throws NoSuchPicturesException if a pictures with the primary key could not be found
	*/
	public Pictures findByPrimaryKey(long PictureId)
		throws NoSuchPicturesException;

	/**
	* Returns the pictures with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures, or <code>null</code> if a pictures with the primary key could not be found
	*/
	public Pictures fetchByPrimaryKey(long PictureId);

	@Override
	public java.util.Map<java.io.Serializable, Pictures> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the pictureses.
	*
	* @return the pictureses
	*/
	public java.util.List<Pictures> findAll();

	/**
	* Returns a range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of pictureses
	*/
	public java.util.List<Pictures> findAll(int start, int end);

	/**
	* Returns an ordered range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of pictureses
	*/
	public java.util.List<Pictures> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator);

	/**
	* Returns an ordered range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of pictureses
	*/
	public java.util.List<Pictures> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Pictures> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the pictureses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of pictureses.
	*
	* @return the number of pictureses
	*/
	public int countAll();
}