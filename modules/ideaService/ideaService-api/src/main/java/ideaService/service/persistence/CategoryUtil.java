/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import ideaService.model.Category;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the category service. This utility wraps {@link ideaService.service.persistence.impl.CategoryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CategoryPersistence
 * @see ideaService.service.persistence.impl.CategoryPersistenceImpl
 * @generated
 */
@ProviderType
public class CategoryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Category category) {
		getPersistence().clearCache(category);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Category> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Category> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Category> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Category update(Category category) {
		return getPersistence().update(category);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Category update(Category category,
		ServiceContext serviceContext) {
		return getPersistence().update(category, serviceContext);
	}

	/**
	* Returns all the categories where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching categories
	*/
	public static List<Category> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the categories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of matching categories
	*/
	public static List<Category> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the categories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categories
	*/
	public static List<Category> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Category> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the categories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching categories
	*/
	public static List<Category> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Category> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByUuid_First(java.lang.String uuid,
		OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the categories before and after the current category in the ordered set where uuid = &#63;.
	*
	* @param categoryId the primary key of the current category
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public static Category[] findByUuid_PrevAndNext(long categoryId,
		java.lang.String uuid, OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByUuid_PrevAndNext(categoryId, uuid, orderByComparator);
	}

	/**
	* Removes all the categories where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of categories where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching categories
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the category where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByUUID_G(java.lang.String uuid, long groupId)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the category where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the category where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the category where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the category that was removed
	*/
	public static Category removeByUUID_G(java.lang.String uuid, long groupId)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of categories where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching categories
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the categories where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching categories
	*/
	public static List<Category> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the categories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of matching categories
	*/
	public static List<Category> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the categories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categories
	*/
	public static List<Category> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the categories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching categories
	*/
	public static List<Category> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Category> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the categories before and after the current category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param categoryId the primary key of the current category
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public static Category[] findByUuid_C_PrevAndNext(long categoryId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(categoryId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the categories where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of categories where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching categories
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the category where projectRef = &#63; or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param projectRef the project ref
	* @return the matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByProjectRef(long projectRef)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().findByProjectRef(projectRef);
	}

	/**
	* Returns the category where projectRef = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectRef the project ref
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByProjectRef(long projectRef) {
		return getPersistence().fetchByProjectRef(projectRef);
	}

	/**
	* Returns the category where projectRef = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectRef the project ref
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByProjectRef(long projectRef,
		boolean retrieveFromCache) {
		return getPersistence().fetchByProjectRef(projectRef, retrieveFromCache);
	}

	/**
	* Removes the category where projectRef = &#63; from the database.
	*
	* @param projectRef the project ref
	* @return the category that was removed
	*/
	public static Category removeByProjectRef(long projectRef)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().removeByProjectRef(projectRef);
	}

	/**
	* Returns the number of categories where projectRef = &#63;.
	*
	* @param projectRef the project ref
	* @return the number of matching categories
	*/
	public static int countByProjectRef(long projectRef) {
		return getPersistence().countByProjectRef(projectRef);
	}

	/**
	* Returns the category where categoryTitle = &#63; or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param categoryTitle the category title
	* @return the matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByCategory(java.lang.String categoryTitle)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().findByCategory(categoryTitle);
	}

	/**
	* Returns the category where categoryTitle = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param categoryTitle the category title
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByCategory(java.lang.String categoryTitle) {
		return getPersistence().fetchByCategory(categoryTitle);
	}

	/**
	* Returns the category where categoryTitle = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param categoryTitle the category title
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByCategory(java.lang.String categoryTitle,
		boolean retrieveFromCache) {
		return getPersistence().fetchByCategory(categoryTitle, retrieveFromCache);
	}

	/**
	* Removes the category where categoryTitle = &#63; from the database.
	*
	* @param categoryTitle the category title
	* @return the category that was removed
	*/
	public static Category removeByCategory(java.lang.String categoryTitle)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().removeByCategory(categoryTitle);
	}

	/**
	* Returns the number of categories where categoryTitle = &#63;.
	*
	* @param categoryTitle the category title
	* @return the number of matching categories
	*/
	public static int countByCategory(java.lang.String categoryTitle) {
		return getPersistence().countByCategory(categoryTitle);
	}

	/**
	* Returns all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @return the matching categories
	*/
	public static List<Category> findByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle) {
		return getPersistence()
				   .findByProjectRefAndCategroy(projectRef, categoryTitle);
	}

	/**
	* Returns a range of all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of matching categories
	*/
	public static List<Category> findByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle, int start, int end) {
		return getPersistence()
				   .findByProjectRefAndCategroy(projectRef, categoryTitle,
			start, end);
	}

	/**
	* Returns an ordered range of all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categories
	*/
	public static List<Category> findByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle, int start, int end,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .findByProjectRefAndCategroy(projectRef, categoryTitle,
			start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching categories
	*/
	public static List<Category> findByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle, int start, int end,
		OrderByComparator<Category> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectRefAndCategroy(projectRef, categoryTitle,
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByProjectRefAndCategroy_First(long projectRef,
		java.lang.String categoryTitle,
		OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByProjectRefAndCategroy_First(projectRef,
			categoryTitle, orderByComparator);
	}

	/**
	* Returns the first category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByProjectRefAndCategroy_First(long projectRef,
		java.lang.String categoryTitle,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .fetchByProjectRefAndCategroy_First(projectRef,
			categoryTitle, orderByComparator);
	}

	/**
	* Returns the last category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public static Category findByProjectRefAndCategroy_Last(long projectRef,
		java.lang.String categoryTitle,
		OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByProjectRefAndCategroy_Last(projectRef, categoryTitle,
			orderByComparator);
	}

	/**
	* Returns the last category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category, or <code>null</code> if a matching category could not be found
	*/
	public static Category fetchByProjectRefAndCategroy_Last(long projectRef,
		java.lang.String categoryTitle,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence()
				   .fetchByProjectRefAndCategroy_Last(projectRef,
			categoryTitle, orderByComparator);
	}

	/**
	* Returns the categories before and after the current category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param categoryId the primary key of the current category
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public static Category[] findByProjectRefAndCategroy_PrevAndNext(
		long categoryId, long projectRef, java.lang.String categoryTitle,
		OrderByComparator<Category> orderByComparator)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence()
				   .findByProjectRefAndCategroy_PrevAndNext(categoryId,
			projectRef, categoryTitle, orderByComparator);
	}

	/**
	* Removes all the categories where projectRef = &#63; and categoryTitle = &#63; from the database.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	*/
	public static void removeByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle) {
		getPersistence().removeByProjectRefAndCategroy(projectRef, categoryTitle);
	}

	/**
	* Returns the number of categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @return the number of matching categories
	*/
	public static int countByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle) {
		return getPersistence()
				   .countByProjectRefAndCategroy(projectRef, categoryTitle);
	}

	/**
	* Caches the category in the entity cache if it is enabled.
	*
	* @param category the category
	*/
	public static void cacheResult(Category category) {
		getPersistence().cacheResult(category);
	}

	/**
	* Caches the categories in the entity cache if it is enabled.
	*
	* @param categories the categories
	*/
	public static void cacheResult(List<Category> categories) {
		getPersistence().cacheResult(categories);
	}

	/**
	* Creates a new category with the primary key. Does not add the category to the database.
	*
	* @param categoryId the primary key for the new category
	* @return the new category
	*/
	public static Category create(long categoryId) {
		return getPersistence().create(categoryId);
	}

	/**
	* Removes the category with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param categoryId the primary key of the category
	* @return the category that was removed
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public static Category remove(long categoryId)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().remove(categoryId);
	}

	public static Category updateImpl(Category category) {
		return getPersistence().updateImpl(category);
	}

	/**
	* Returns the category with the primary key or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param categoryId the primary key of the category
	* @return the category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public static Category findByPrimaryKey(long categoryId)
		throws ideaService.exception.NoSuchCategoryException {
		return getPersistence().findByPrimaryKey(categoryId);
	}

	/**
	* Returns the category with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param categoryId the primary key of the category
	* @return the category, or <code>null</code> if a category with the primary key could not be found
	*/
	public static Category fetchByPrimaryKey(long categoryId) {
		return getPersistence().fetchByPrimaryKey(categoryId);
	}

	public static java.util.Map<java.io.Serializable, Category> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the categories.
	*
	* @return the categories
	*/
	public static List<Category> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of categories
	*/
	public static List<Category> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of categories
	*/
	public static List<Category> findAll(int start, int end,
		OrderByComparator<Category> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of categories
	*/
	public static List<Category> findAll(int start, int end,
		OrderByComparator<Category> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the categories from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of categories.
	*
	* @return the number of categories
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static CategoryPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CategoryPersistence, CategoryPersistence> _serviceTracker =
		ServiceTrackerFactory.open(CategoryPersistence.class);
}