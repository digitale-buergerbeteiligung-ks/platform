/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Videos. This utility wraps
 * {@link ideaService.service.impl.VideosLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see VideosLocalService
 * @see ideaService.service.base.VideosLocalServiceBaseImpl
 * @see ideaService.service.impl.VideosLocalServiceImpl
 * @generated
 */
@ProviderType
public class VideosLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link ideaService.service.impl.VideosLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the videos to the database. Also notifies the appropriate model listeners.
	*
	* @param videos the videos
	* @return the videos that was added
	*/
	public static ideaService.model.Videos addVideos(
		ideaService.model.Videos videos) {
		return getService().addVideos(videos);
	}

	/**
	* Creates a new videos with the primary key. Does not add the videos to the database.
	*
	* @param VideoId the primary key for the new videos
	* @return the new videos
	*/
	public static ideaService.model.Videos createVideos(long VideoId) {
		return getService().createVideos(VideoId);
	}

	/**
	* Deletes the videos from the database. Also notifies the appropriate model listeners.
	*
	* @param videos the videos
	* @return the videos that was removed
	*/
	public static ideaService.model.Videos deleteVideos(
		ideaService.model.Videos videos) {
		return getService().deleteVideos(videos);
	}

	/**
	* Deletes the videos with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param VideoId the primary key of the videos
	* @return the videos that was removed
	* @throws PortalException if a videos with the primary key could not be found
	*/
	public static ideaService.model.Videos deleteVideos(long VideoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteVideos(VideoId);
	}

	public static ideaService.model.Videos fetchVideos(long VideoId) {
		return getService().fetchVideos(VideoId);
	}

	public static ideaService.model.Videos getVideoByIdeaRefAndExtension(
		long ideasRef, ideasService.service.enums.VideoExtensions ex) {
		return getService().getVideoByIdeaRefAndExtension(ideasRef, ex);
	}

	public static ideaService.model.Videos getVideoUrlByIdeaRefAndExtension(
		long ideasRef, java.lang.String ex) {
		return getService().getVideoUrlByIdeaRefAndExtension(ideasRef, ex);
	}

	/**
	* Returns the videos with the primary key.
	*
	* @param VideoId the primary key of the videos
	* @return the videos
	* @throws PortalException if a videos with the primary key could not be found
	*/
	public static ideaService.model.Videos getVideos(long VideoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getVideos(VideoId);
	}

	/**
	* Updates the videos in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param videos the videos
	* @return the videos that was updated
	*/
	public static ideaService.model.Videos updateVideos(
		ideaService.model.Videos videos) {
		return getService().updateVideos(videos);
	}

	/**
	* Returns the number of videoses.
	*
	* @return the number of videoses
	*/
	public static int getVideosesCount() {
		return getService().getVideosesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String getVideoUrlByIdeaRefAndExtension(
		long ideasRef, ideasService.service.enums.VideoExtensions ex) {
		return getService().getVideoUrlByIdeaRefAndExtension(ideasRef, ex);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<ideaService.model.Videos> getAllVideosForIdeasRef(
		long ideasRef) throws ideaService.exception.NoSuchVideosException {
		return getService().getAllVideosForIdeasRef(ideasRef);
	}

	/**
	* Returns a range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @return the range of videoses
	*/
	public static java.util.List<ideaService.model.Videos> getVideoses(
		int start, int end) {
		return getService().getVideoses(start, end);
	}

	public static long createNewVideoEntry(long ideasRef, long fileRef,
		java.lang.String videoUrl, java.lang.String extension) {
		return getService()
				   .createNewVideoEntry(ideasRef, fileRef, videoUrl, extension);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static void deleteAllVideosForIdeaRef(long ideasRef) {
		getService().deleteAllVideosForIdeaRef(ideasRef);
	}

	public static VideosLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VideosLocalService, VideosLocalService> _serviceTracker =
		ServiceTrackerFactory.open(VideosLocalService.class);
}