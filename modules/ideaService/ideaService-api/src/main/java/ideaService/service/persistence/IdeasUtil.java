/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import ideaService.model.Ideas;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the ideas service. This utility wraps {@link ideaService.service.persistence.impl.IdeasPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasPersistence
 * @see ideaService.service.persistence.impl.IdeasPersistenceImpl
 * @generated
 */
@ProviderType
public class IdeasUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Ideas ideas) {
		getPersistence().clearCache(ideas);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Ideas> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Ideas> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Ideas> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Ideas update(Ideas ideas) {
		return getPersistence().update(ideas);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Ideas update(Ideas ideas, ServiceContext serviceContext) {
		return getPersistence().update(ideas, serviceContext);
	}

	/**
	* Returns all the ideases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching ideases
	*/
	public static List<Ideas> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the ideases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public static List<Ideas> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the ideases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Ideas> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ideases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByUuid_First(java.lang.String uuid,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the ideases before and after the current ideas in the ordered set where uuid = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas[] findByUuid_PrevAndNext(long ideasId,
		java.lang.String uuid, OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByUuid_PrevAndNext(ideasId, uuid, orderByComparator);
	}

	/**
	* Removes all the ideases where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of ideases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching ideases
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the ideas where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByUUID_G(java.lang.String uuid, long groupId)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the ideas where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the ideas where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the ideas where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the ideas that was removed
	*/
	public static Ideas removeByUUID_G(java.lang.String uuid, long groupId)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of ideases where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching ideases
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching ideases
	*/
	public static List<Ideas> findByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public static List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Ideas> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the ideases before and after the current ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas[] findByUuid_C_PrevAndNext(long ideasId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(ideasId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the ideases where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of ideases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching ideases
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the ideas where layoutRef = &#63; or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param layoutRef the layout ref
	* @return the matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByLayoutRef(long layoutRef)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByLayoutRef(layoutRef);
	}

	/**
	* Returns the ideas where layoutRef = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param layoutRef the layout ref
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByLayoutRef(long layoutRef) {
		return getPersistence().fetchByLayoutRef(layoutRef);
	}

	/**
	* Returns the ideas where layoutRef = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param layoutRef the layout ref
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByLayoutRef(long layoutRef,
		boolean retrieveFromCache) {
		return getPersistence().fetchByLayoutRef(layoutRef, retrieveFromCache);
	}

	/**
	* Removes the ideas where layoutRef = &#63; from the database.
	*
	* @param layoutRef the layout ref
	* @return the ideas that was removed
	*/
	public static Ideas removeByLayoutRef(long layoutRef)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().removeByLayoutRef(layoutRef);
	}

	/**
	* Returns the number of ideases where layoutRef = &#63;.
	*
	* @param layoutRef the layout ref
	* @return the number of matching ideases
	*/
	public static int countByLayoutRef(long layoutRef) {
		return getPersistence().countByLayoutRef(layoutRef);
	}

	/**
	* Returns all the ideases where status = &#63;.
	*
	* @param status the status
	* @return the matching ideases
	*/
	public static List<Ideas> findByStatus(int status) {
		return getPersistence().findByStatus(status);
	}

	/**
	* Returns a range of all the ideases where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public static List<Ideas> findByStatus(int status, int start, int end) {
		return getPersistence().findByStatus(status, start, end);
	}

	/**
	* Returns an ordered range of all the ideases where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByStatus(int status, int start, int end,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .findByStatus(status, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ideases where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByStatus(int status, int start, int end,
		OrderByComparator<Ideas> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByStatus(status, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByStatus_First(int status,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByStatus_First(status, orderByComparator);
	}

	/**
	* Returns the first ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByStatus_First(int status,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence().fetchByStatus_First(status, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByStatus_Last(int status,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByStatus_Last(status, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByStatus_Last(int status,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence().fetchByStatus_Last(status, orderByComparator);
	}

	/**
	* Returns the ideases before and after the current ideas in the ordered set where status = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas[] findByStatus_PrevAndNext(long ideasId, int status,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByStatus_PrevAndNext(ideasId, status, orderByComparator);
	}

	/**
	* Removes all the ideases where status = &#63; from the database.
	*
	* @param status the status
	*/
	public static void removeByStatus(int status) {
		getPersistence().removeByStatus(status);
	}

	/**
	* Returns the number of ideases where status = &#63;.
	*
	* @param status the status
	* @return the number of matching ideases
	*/
	public static int countByStatus(int status) {
		return getPersistence().countByStatus(status);
	}

	/**
	* Returns all the ideases where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the matching ideases
	*/
	public static List<Ideas> findByG_S(long groupId, int status) {
		return getPersistence().findByG_S(groupId, status);
	}

	/**
	* Returns a range of all the ideases where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public static List<Ideas> findByG_S(long groupId, int status, int start,
		int end) {
		return getPersistence().findByG_S(groupId, status, start, end);
	}

	/**
	* Returns an ordered range of all the ideases where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByG_S(long groupId, int status, int start,
		int end, OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .findByG_S(groupId, status, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ideases where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public static List<Ideas> findByG_S(long groupId, int status, int start,
		int end, OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByG_S(groupId, status, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByG_S_First(long groupId, int status,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByG_S_First(groupId, status, orderByComparator);
	}

	/**
	* Returns the first ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByG_S_First(long groupId, int status,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .fetchByG_S_First(groupId, status, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public static Ideas findByG_S_Last(long groupId, int status,
		OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByG_S_Last(groupId, status, orderByComparator);
	}

	/**
	* Returns the last ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public static Ideas fetchByG_S_Last(long groupId, int status,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .fetchByG_S_Last(groupId, status, orderByComparator);
	}

	/**
	* Returns the ideases before and after the current ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas[] findByG_S_PrevAndNext(long ideasId, long groupId,
		int status, OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .findByG_S_PrevAndNext(ideasId, groupId, status,
			orderByComparator);
	}

	/**
	* Returns all the ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the matching ideases that the user has permission to view
	*/
	public static List<Ideas> filterFindByG_S(long groupId, int status) {
		return getPersistence().filterFindByG_S(groupId, status);
	}

	/**
	* Returns a range of all the ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases that the user has permission to view
	*/
	public static List<Ideas> filterFindByG_S(long groupId, int status,
		int start, int end) {
		return getPersistence().filterFindByG_S(groupId, status, start, end);
	}

	/**
	* Returns an ordered range of all the ideases that the user has permissions to view where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases that the user has permission to view
	*/
	public static List<Ideas> filterFindByG_S(long groupId, int status,
		int start, int end, OrderByComparator<Ideas> orderByComparator) {
		return getPersistence()
				   .filterFindByG_S(groupId, status, start, end,
			orderByComparator);
	}

	/**
	* Returns the ideases before and after the current ideas in the ordered set of ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas[] filterFindByG_S_PrevAndNext(long ideasId,
		long groupId, int status, OrderByComparator<Ideas> orderByComparator)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence()
				   .filterFindByG_S_PrevAndNext(ideasId, groupId, status,
			orderByComparator);
	}

	/**
	* Removes all the ideases where groupId = &#63; and status = &#63; from the database.
	*
	* @param groupId the group ID
	* @param status the status
	*/
	public static void removeByG_S(long groupId, int status) {
		getPersistence().removeByG_S(groupId, status);
	}

	/**
	* Returns the number of ideases where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the number of matching ideases
	*/
	public static int countByG_S(long groupId, int status) {
		return getPersistence().countByG_S(groupId, status);
	}

	/**
	* Returns the number of ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the number of matching ideases that the user has permission to view
	*/
	public static int filterCountByG_S(long groupId, int status) {
		return getPersistence().filterCountByG_S(groupId, status);
	}

	/**
	* Caches the ideas in the entity cache if it is enabled.
	*
	* @param ideas the ideas
	*/
	public static void cacheResult(Ideas ideas) {
		getPersistence().cacheResult(ideas);
	}

	/**
	* Caches the ideases in the entity cache if it is enabled.
	*
	* @param ideases the ideases
	*/
	public static void cacheResult(List<Ideas> ideases) {
		getPersistence().cacheResult(ideases);
	}

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	public static Ideas create(long ideasId) {
		return getPersistence().create(ideasId);
	}

	/**
	* Removes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas remove(long ideasId)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().remove(ideasId);
	}

	public static Ideas updateImpl(Ideas ideas) {
		return getPersistence().updateImpl(ideas);
	}

	/**
	* Returns the ideas with the primary key or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public static Ideas findByPrimaryKey(long ideasId)
		throws ideaService.exception.NoSuchIdeasException {
		return getPersistence().findByPrimaryKey(ideasId);
	}

	/**
	* Returns the ideas with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas, or <code>null</code> if a ideas with the primary key could not be found
	*/
	public static Ideas fetchByPrimaryKey(long ideasId) {
		return getPersistence().fetchByPrimaryKey(ideasId);
	}

	public static java.util.Map<java.io.Serializable, Ideas> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the ideases.
	*
	* @return the ideases
	*/
	public static List<Ideas> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	public static List<Ideas> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ideases
	*/
	public static List<Ideas> findAll(int start, int end,
		OrderByComparator<Ideas> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of ideases
	*/
	public static List<Ideas> findAll(int start, int end,
		OrderByComparator<Ideas> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the ideases from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static IdeasPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<IdeasPersistence, IdeasPersistence> _serviceTracker =
		ServiceTrackerFactory.open(IdeasPersistence.class);
}