/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VideosLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see VideosLocalService
 * @generated
 */
@ProviderType
public class VideosLocalServiceWrapper implements VideosLocalService,
	ServiceWrapper<VideosLocalService> {
	public VideosLocalServiceWrapper(VideosLocalService videosLocalService) {
		_videosLocalService = videosLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _videosLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _videosLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _videosLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _videosLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _videosLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the videos to the database. Also notifies the appropriate model listeners.
	*
	* @param videos the videos
	* @return the videos that was added
	*/
	@Override
	public ideaService.model.Videos addVideos(ideaService.model.Videos videos) {
		return _videosLocalService.addVideos(videos);
	}

	/**
	* Creates a new videos with the primary key. Does not add the videos to the database.
	*
	* @param VideoId the primary key for the new videos
	* @return the new videos
	*/
	@Override
	public ideaService.model.Videos createVideos(long VideoId) {
		return _videosLocalService.createVideos(VideoId);
	}

	/**
	* Deletes the videos from the database. Also notifies the appropriate model listeners.
	*
	* @param videos the videos
	* @return the videos that was removed
	*/
	@Override
	public ideaService.model.Videos deleteVideos(
		ideaService.model.Videos videos) {
		return _videosLocalService.deleteVideos(videos);
	}

	/**
	* Deletes the videos with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param VideoId the primary key of the videos
	* @return the videos that was removed
	* @throws PortalException if a videos with the primary key could not be found
	*/
	@Override
	public ideaService.model.Videos deleteVideos(long VideoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _videosLocalService.deleteVideos(VideoId);
	}

	@Override
	public ideaService.model.Videos fetchVideos(long VideoId) {
		return _videosLocalService.fetchVideos(VideoId);
	}

	@Override
	public ideaService.model.Videos getVideoByIdeaRefAndExtension(
		long ideasRef, ideasService.service.enums.VideoExtensions ex) {
		return _videosLocalService.getVideoByIdeaRefAndExtension(ideasRef, ex);
	}

	@Override
	public ideaService.model.Videos getVideoUrlByIdeaRefAndExtension(
		long ideasRef, java.lang.String ex) {
		return _videosLocalService.getVideoUrlByIdeaRefAndExtension(ideasRef, ex);
	}

	/**
	* Returns the videos with the primary key.
	*
	* @param VideoId the primary key of the videos
	* @return the videos
	* @throws PortalException if a videos with the primary key could not be found
	*/
	@Override
	public ideaService.model.Videos getVideos(long VideoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _videosLocalService.getVideos(VideoId);
	}

	/**
	* Updates the videos in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param videos the videos
	* @return the videos that was updated
	*/
	@Override
	public ideaService.model.Videos updateVideos(
		ideaService.model.Videos videos) {
		return _videosLocalService.updateVideos(videos);
	}

	/**
	* Returns the number of videoses.
	*
	* @return the number of videoses
	*/
	@Override
	public int getVideosesCount() {
		return _videosLocalService.getVideosesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _videosLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String getVideoUrlByIdeaRefAndExtension(long ideasRef,
		ideasService.service.enums.VideoExtensions ex) {
		return _videosLocalService.getVideoUrlByIdeaRefAndExtension(ideasRef, ex);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _videosLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _videosLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _videosLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<ideaService.model.Videos> getAllVideosForIdeasRef(
		long ideasRef) throws ideaService.exception.NoSuchVideosException {
		return _videosLocalService.getAllVideosForIdeasRef(ideasRef);
	}

	/**
	* Returns a range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @return the range of videoses
	*/
	@Override
	public java.util.List<ideaService.model.Videos> getVideoses(int start,
		int end) {
		return _videosLocalService.getVideoses(start, end);
	}

	@Override
	public long createNewVideoEntry(long ideasRef, long fileRef,
		java.lang.String videoUrl, java.lang.String extension) {
		return _videosLocalService.createNewVideoEntry(ideasRef, fileRef,
			videoUrl, extension);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _videosLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _videosLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public void deleteAllVideosForIdeaRef(long ideasRef) {
		_videosLocalService.deleteAllVideosForIdeaRef(ideasRef);
	}

	@Override
	public VideosLocalService getWrappedService() {
		return _videosLocalService;
	}

	@Override
	public void setWrappedService(VideosLocalService videosLocalService) {
		_videosLocalService = videosLocalService;
	}

	private VideosLocalService _videosLocalService;
}