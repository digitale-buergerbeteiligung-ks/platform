/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchIdeasException;

import ideaService.model.Ideas;

/**
 * The persistence interface for the ideas service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.IdeasPersistenceImpl
 * @see IdeasUtil
 * @generated
 */
@ProviderType
public interface IdeasPersistence extends BasePersistence<Ideas> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeasUtil} to access the ideas persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the ideases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching ideases
	*/
	public java.util.List<Ideas> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the ideases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public java.util.List<Ideas> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the ideases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns an ordered range of all the ideases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the first ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the last ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the last ideas in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the ideases before and after the current ideas in the ordered set where uuid = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas[] findByUuid_PrevAndNext(long ideasId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Removes all the ideases where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of ideases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching ideases
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the ideas where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchIdeasException;

	/**
	* Returns the ideas where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the ideas where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the ideas where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the ideas that was removed
	*/
	public Ideas removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchIdeasException;

	/**
	* Returns the number of ideases where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching ideases
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching ideases
	*/
	public java.util.List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public java.util.List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns an ordered range of all the ideases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the first ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the last ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the last ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the ideases before and after the current ideas in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas[] findByUuid_C_PrevAndNext(long ideasId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Removes all the ideases where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of ideases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching ideases
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the ideas where layoutRef = &#63; or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param layoutRef the layout ref
	* @return the matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByLayoutRef(long layoutRef) throws NoSuchIdeasException;

	/**
	* Returns the ideas where layoutRef = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param layoutRef the layout ref
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByLayoutRef(long layoutRef);

	/**
	* Returns the ideas where layoutRef = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param layoutRef the layout ref
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByLayoutRef(long layoutRef, boolean retrieveFromCache);

	/**
	* Removes the ideas where layoutRef = &#63; from the database.
	*
	* @param layoutRef the layout ref
	* @return the ideas that was removed
	*/
	public Ideas removeByLayoutRef(long layoutRef) throws NoSuchIdeasException;

	/**
	* Returns the number of ideases where layoutRef = &#63;.
	*
	* @param layoutRef the layout ref
	* @return the number of matching ideases
	*/
	public int countByLayoutRef(long layoutRef);

	/**
	* Returns all the ideases where status = &#63;.
	*
	* @param status the status
	* @return the matching ideases
	*/
	public java.util.List<Ideas> findByStatus(int status);

	/**
	* Returns a range of all the ideases where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public java.util.List<Ideas> findByStatus(int status, int start, int end);

	/**
	* Returns an ordered range of all the ideases where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByStatus(int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns an ordered range of all the ideases where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByStatus(int status, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByStatus_First(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the first ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByStatus_First(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the last ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByStatus_Last(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the last ideas in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByStatus_Last(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the ideases before and after the current ideas in the ordered set where status = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas[] findByStatus_PrevAndNext(long ideasId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Removes all the ideases where status = &#63; from the database.
	*
	* @param status the status
	*/
	public void removeByStatus(int status);

	/**
	* Returns the number of ideases where status = &#63;.
	*
	* @param status the status
	* @return the number of matching ideases
	*/
	public int countByStatus(int status);

	/**
	* Returns all the ideases where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the matching ideases
	*/
	public java.util.List<Ideas> findByG_S(long groupId, int status);

	/**
	* Returns a range of all the ideases where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases
	*/
	public java.util.List<Ideas> findByG_S(long groupId, int status, int start,
		int end);

	/**
	* Returns an ordered range of all the ideases where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByG_S(long groupId, int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns an ordered range of all the ideases where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching ideases
	*/
	public java.util.List<Ideas> findByG_S(long groupId, int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByG_S_First(long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the first ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByG_S_First(long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the last ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas
	* @throws NoSuchIdeasException if a matching ideas could not be found
	*/
	public Ideas findByG_S_Last(long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns the last ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	public Ideas fetchByG_S_Last(long groupId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the ideases before and after the current ideas in the ordered set where groupId = &#63; and status = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas[] findByG_S_PrevAndNext(long ideasId, long groupId,
		int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Returns all the ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the matching ideases that the user has permission to view
	*/
	public java.util.List<Ideas> filterFindByG_S(long groupId, int status);

	/**
	* Returns a range of all the ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of matching ideases that the user has permission to view
	*/
	public java.util.List<Ideas> filterFindByG_S(long groupId, int status,
		int start, int end);

	/**
	* Returns an ordered range of all the ideases that the user has permissions to view where groupId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param status the status
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching ideases that the user has permission to view
	*/
	public java.util.List<Ideas> filterFindByG_S(long groupId, int status,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns the ideases before and after the current ideas in the ordered set of ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* @param ideasId the primary key of the current ideas
	* @param groupId the group ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas[] filterFindByG_S_PrevAndNext(long ideasId, long groupId,
		int status,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator)
		throws NoSuchIdeasException;

	/**
	* Removes all the ideases where groupId = &#63; and status = &#63; from the database.
	*
	* @param groupId the group ID
	* @param status the status
	*/
	public void removeByG_S(long groupId, int status);

	/**
	* Returns the number of ideases where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the number of matching ideases
	*/
	public int countByG_S(long groupId, int status);

	/**
	* Returns the number of ideases that the user has permission to view where groupId = &#63; and status = &#63;.
	*
	* @param groupId the group ID
	* @param status the status
	* @return the number of matching ideases that the user has permission to view
	*/
	public int filterCountByG_S(long groupId, int status);

	/**
	* Caches the ideas in the entity cache if it is enabled.
	*
	* @param ideas the ideas
	*/
	public void cacheResult(Ideas ideas);

	/**
	* Caches the ideases in the entity cache if it is enabled.
	*
	* @param ideases the ideases
	*/
	public void cacheResult(java.util.List<Ideas> ideases);

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	public Ideas create(long ideasId);

	/**
	* Removes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas remove(long ideasId) throws NoSuchIdeasException;

	public Ideas updateImpl(Ideas ideas);

	/**
	* Returns the ideas with the primary key or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas findByPrimaryKey(long ideasId) throws NoSuchIdeasException;

	/**
	* Returns the ideas with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas, or <code>null</code> if a ideas with the primary key could not be found
	*/
	public Ideas fetchByPrimaryKey(long ideasId);

	@Override
	public java.util.Map<java.io.Serializable, Ideas> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the ideases.
	*
	* @return the ideases
	*/
	public java.util.List<Ideas> findAll();

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	public java.util.List<Ideas> findAll(int start, int end);

	/**
	* Returns an ordered range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ideases
	*/
	public java.util.List<Ideas> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns an ordered range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of ideases
	*/
	public java.util.List<Ideas> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the ideases from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}