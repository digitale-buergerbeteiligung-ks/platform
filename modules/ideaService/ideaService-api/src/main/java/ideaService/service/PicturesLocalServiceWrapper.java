/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PicturesLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PicturesLocalService
 * @generated
 */
@ProviderType
public class PicturesLocalServiceWrapper implements PicturesLocalService,
	ServiceWrapper<PicturesLocalService> {
	public PicturesLocalServiceWrapper(
		PicturesLocalService picturesLocalService) {
		_picturesLocalService = picturesLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _picturesLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _picturesLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _picturesLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _picturesLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _picturesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the pictures to the database. Also notifies the appropriate model listeners.
	*
	* @param pictures the pictures
	* @return the pictures that was added
	*/
	@Override
	public ideaService.model.Pictures addPictures(
		ideaService.model.Pictures pictures) {
		return _picturesLocalService.addPictures(pictures);
	}

	/**
	* Creates a new pictures with the primary key. Does not add the pictures to the database.
	*
	* @param PictureId the primary key for the new pictures
	* @return the new pictures
	*/
	@Override
	public ideaService.model.Pictures createPictures(long PictureId) {
		return _picturesLocalService.createPictures(PictureId);
	}

	/**
	* Deletes the pictures from the database. Also notifies the appropriate model listeners.
	*
	* @param pictures the pictures
	* @return the pictures that was removed
	*/
	@Override
	public ideaService.model.Pictures deletePictures(
		ideaService.model.Pictures pictures) {
		return _picturesLocalService.deletePictures(pictures);
	}

	/**
	* Deletes the pictures with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures that was removed
	* @throws PortalException if a pictures with the primary key could not be found
	*/
	@Override
	public ideaService.model.Pictures deletePictures(long PictureId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _picturesLocalService.deletePictures(PictureId);
	}

	@Override
	public ideaService.model.Pictures fetchPictures(long PictureId) {
		return _picturesLocalService.fetchPictures(PictureId);
	}

	/**
	* Returns the pictures with the primary key.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures
	* @throws PortalException if a pictures with the primary key could not be found
	*/
	@Override
	public ideaService.model.Pictures getPictures(long PictureId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _picturesLocalService.getPictures(PictureId);
	}

	/**
	* Updates the pictures in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param pictures the pictures
	* @return the pictures that was updated
	*/
	@Override
	public ideaService.model.Pictures updatePictures(
		ideaService.model.Pictures pictures) {
		return _picturesLocalService.updatePictures(pictures);
	}

	/**
	* Returns the number of pictureses.
	*
	* @return the number of pictureses
	*/
	@Override
	public int getPicturesesCount() {
		return _picturesLocalService.getPicturesesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _picturesLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _picturesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _picturesLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _picturesLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<ideaService.model.Pictures> getAllPicturesForIdeasRef(
		long ideasRef) throws ideaService.exception.NoSuchPicturesException {
		return _picturesLocalService.getAllPicturesForIdeasRef(ideasRef);
	}

	/**
	* Returns a range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of pictureses
	*/
	@Override
	public java.util.List<ideaService.model.Pictures> getPictureses(int start,
		int end) {
		return _picturesLocalService.getPictureses(start, end);
	}

	@Override
	public long createNewPictureEntry(long ideasRef, long fileRef,
		java.lang.String picUrl, int position) {
		return _picturesLocalService.createNewPictureEntry(ideasRef, fileRef,
			picUrl, position);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _picturesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _picturesLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public void deleteAllPicturesForIdeaRef(long ideasRef) {
		_picturesLocalService.deleteAllPicturesForIdeaRef(ideasRef);
	}

	@Override
	public PicturesLocalService getWrappedService() {
		return _picturesLocalService;
	}

	@Override
	public void setWrappedService(PicturesLocalService picturesLocalService) {
		_picturesLocalService = picturesLocalService;
	}

	private PicturesLocalService _picturesLocalService;
}