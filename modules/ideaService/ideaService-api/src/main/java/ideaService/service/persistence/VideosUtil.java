/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import ideaService.model.Videos;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the videos service. This utility wraps {@link ideaService.service.persistence.impl.VideosPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VideosPersistence
 * @see ideaService.service.persistence.impl.VideosPersistenceImpl
 * @generated
 */
@ProviderType
public class VideosUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Videos videos) {
		getPersistence().clearCache(videos);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Videos> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Videos> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Videos> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Videos> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Videos update(Videos videos) {
		return getPersistence().update(videos);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Videos update(Videos videos, ServiceContext serviceContext) {
		return getPersistence().update(videos, serviceContext);
	}

	/**
	* Returns all the videoses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the matching videoses
	*/
	public static List<Videos> findByIdeasRef(long IdeasRef) {
		return getPersistence().findByIdeasRef(IdeasRef);
	}

	/**
	* Returns a range of all the videoses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @return the range of matching videoses
	*/
	public static List<Videos> findByIdeasRef(long IdeasRef, int start, int end) {
		return getPersistence().findByIdeasRef(IdeasRef, start, end);
	}

	/**
	* Returns an ordered range of all the videoses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching videoses
	*/
	public static List<Videos> findByIdeasRef(long IdeasRef, int start,
		int end, OrderByComparator<Videos> orderByComparator) {
		return getPersistence()
				   .findByIdeasRef(IdeasRef, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the videoses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching videoses
	*/
	public static List<Videos> findByIdeasRef(long IdeasRef, int start,
		int end, OrderByComparator<Videos> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByIdeasRef(IdeasRef, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching videos
	* @throws NoSuchVideosException if a matching videos could not be found
	*/
	public static Videos findByIdeasRef_First(long IdeasRef,
		OrderByComparator<Videos> orderByComparator)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence().findByIdeasRef_First(IdeasRef, orderByComparator);
	}

	/**
	* Returns the first videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public static Videos fetchByIdeasRef_First(long IdeasRef,
		OrderByComparator<Videos> orderByComparator) {
		return getPersistence()
				   .fetchByIdeasRef_First(IdeasRef, orderByComparator);
	}

	/**
	* Returns the last videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching videos
	* @throws NoSuchVideosException if a matching videos could not be found
	*/
	public static Videos findByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Videos> orderByComparator)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence().findByIdeasRef_Last(IdeasRef, orderByComparator);
	}

	/**
	* Returns the last videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public static Videos fetchByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Videos> orderByComparator) {
		return getPersistence().fetchByIdeasRef_Last(IdeasRef, orderByComparator);
	}

	/**
	* Returns the videoses before and after the current videos in the ordered set where IdeasRef = &#63;.
	*
	* @param VideoId the primary key of the current videos
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next videos
	* @throws NoSuchVideosException if a videos with the primary key could not be found
	*/
	public static Videos[] findByIdeasRef_PrevAndNext(long VideoId,
		long IdeasRef, OrderByComparator<Videos> orderByComparator)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence()
				   .findByIdeasRef_PrevAndNext(VideoId, IdeasRef,
			orderByComparator);
	}

	/**
	* Removes all the videoses where IdeasRef = &#63; from the database.
	*
	* @param IdeasRef the ideas ref
	*/
	public static void removeByIdeasRef(long IdeasRef) {
		getPersistence().removeByIdeasRef(IdeasRef);
	}

	/**
	* Returns the number of videoses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the number of matching videoses
	*/
	public static int countByIdeasRef(long IdeasRef) {
		return getPersistence().countByIdeasRef(IdeasRef);
	}

	/**
	* Returns the videos where Extension = &#63; or throws a {@link NoSuchVideosException} if it could not be found.
	*
	* @param Extension the extension
	* @return the matching videos
	* @throws NoSuchVideosException if a matching videos could not be found
	*/
	public static Videos findByExtension(java.lang.String Extension)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence().findByExtension(Extension);
	}

	/**
	* Returns the videos where Extension = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param Extension the extension
	* @return the matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public static Videos fetchByExtension(java.lang.String Extension) {
		return getPersistence().fetchByExtension(Extension);
	}

	/**
	* Returns the videos where Extension = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param Extension the extension
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public static Videos fetchByExtension(java.lang.String Extension,
		boolean retrieveFromCache) {
		return getPersistence().fetchByExtension(Extension, retrieveFromCache);
	}

	/**
	* Removes the videos where Extension = &#63; from the database.
	*
	* @param Extension the extension
	* @return the videos that was removed
	*/
	public static Videos removeByExtension(java.lang.String Extension)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence().removeByExtension(Extension);
	}

	/**
	* Returns the number of videoses where Extension = &#63;.
	*
	* @param Extension the extension
	* @return the number of matching videoses
	*/
	public static int countByExtension(java.lang.String Extension) {
		return getPersistence().countByExtension(Extension);
	}

	/**
	* Caches the videos in the entity cache if it is enabled.
	*
	* @param videos the videos
	*/
	public static void cacheResult(Videos videos) {
		getPersistence().cacheResult(videos);
	}

	/**
	* Caches the videoses in the entity cache if it is enabled.
	*
	* @param videoses the videoses
	*/
	public static void cacheResult(List<Videos> videoses) {
		getPersistence().cacheResult(videoses);
	}

	/**
	* Creates a new videos with the primary key. Does not add the videos to the database.
	*
	* @param VideoId the primary key for the new videos
	* @return the new videos
	*/
	public static Videos create(long VideoId) {
		return getPersistence().create(VideoId);
	}

	/**
	* Removes the videos with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param VideoId the primary key of the videos
	* @return the videos that was removed
	* @throws NoSuchVideosException if a videos with the primary key could not be found
	*/
	public static Videos remove(long VideoId)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence().remove(VideoId);
	}

	public static Videos updateImpl(Videos videos) {
		return getPersistence().updateImpl(videos);
	}

	/**
	* Returns the videos with the primary key or throws a {@link NoSuchVideosException} if it could not be found.
	*
	* @param VideoId the primary key of the videos
	* @return the videos
	* @throws NoSuchVideosException if a videos with the primary key could not be found
	*/
	public static Videos findByPrimaryKey(long VideoId)
		throws ideaService.exception.NoSuchVideosException {
		return getPersistence().findByPrimaryKey(VideoId);
	}

	/**
	* Returns the videos with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param VideoId the primary key of the videos
	* @return the videos, or <code>null</code> if a videos with the primary key could not be found
	*/
	public static Videos fetchByPrimaryKey(long VideoId) {
		return getPersistence().fetchByPrimaryKey(VideoId);
	}

	public static java.util.Map<java.io.Serializable, Videos> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the videoses.
	*
	* @return the videoses
	*/
	public static List<Videos> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @return the range of videoses
	*/
	public static List<Videos> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of videoses
	*/
	public static List<Videos> findAll(int start, int end,
		OrderByComparator<Videos> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of videoses
	*/
	public static List<Videos> findAll(int start, int end,
		OrderByComparator<Videos> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the videoses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of videoses.
	*
	* @return the number of videoses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static VideosPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VideosPersistence, VideosPersistence> _serviceTracker =
		ServiceTrackerFactory.open(VideosPersistence.class);
}