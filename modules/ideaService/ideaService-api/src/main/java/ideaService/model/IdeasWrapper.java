/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Ideas}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Ideas
 * @generated
 */
@ProviderType
public class IdeasWrapper implements Ideas, ModelWrapper<Ideas> {
	public IdeasWrapper(Ideas ideas) {
		_ideas = ideas;
	}

	@Override
	public Class<?> getModelClass() {
		return Ideas.class;
	}

	@Override
	public String getModelClassName() {
		return Ideas.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("ideasId", getIdeasId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("shortdescription", getShortdescription());
		attributes.put("reviewStatus", getReviewStatus());
		attributes.put("goal", getGoal());
		attributes.put("tags", getTags());
		attributes.put("importance", getImportance());
		attributes.put("targetAudience", getTargetAudience());
		attributes.put("solution", getSolution());
		attributes.put("category", getCategory());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("videoUrl", getVideoUrl());
		attributes.put("published", getPublished());
		attributes.put("isVisibleOnMap", getIsVisibleOnMap());
		attributes.put("rating", getRating());
		attributes.put("projectRef", getProjectRef());
		attributes.put("videoFileRef", getVideoFileRef());
		attributes.put("pageUrl", getPageUrl());
		attributes.put("layoutRef", getLayoutRef());
		attributes.put("pitch", getPitch());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long ideasId = (Long)attributes.get("ideasId");

		if (ideasId != null) {
			setIdeasId(ideasId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String shortdescription = (String)attributes.get("shortdescription");

		if (shortdescription != null) {
			setShortdescription(shortdescription);
		}

		String reviewStatus = (String)attributes.get("reviewStatus");

		if (reviewStatus != null) {
			setReviewStatus(reviewStatus);
		}

		String goal = (String)attributes.get("goal");

		if (goal != null) {
			setGoal(goal);
		}

		String tags = (String)attributes.get("tags");

		if (tags != null) {
			setTags(tags);
		}

		String importance = (String)attributes.get("importance");

		if (importance != null) {
			setImportance(importance);
		}

		String targetAudience = (String)attributes.get("targetAudience");

		if (targetAudience != null) {
			setTargetAudience(targetAudience);
		}

		String solution = (String)attributes.get("solution");

		if (solution != null) {
			setSolution(solution);
		}

		Long category = (Long)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		Double latitude = (Double)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		Double longitude = (Double)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		String videoUrl = (String)attributes.get("videoUrl");

		if (videoUrl != null) {
			setVideoUrl(videoUrl);
		}

		Boolean published = (Boolean)attributes.get("published");

		if (published != null) {
			setPublished(published);
		}

		Boolean isVisibleOnMap = (Boolean)attributes.get("isVisibleOnMap");

		if (isVisibleOnMap != null) {
			setIsVisibleOnMap(isVisibleOnMap);
		}

		String rating = (String)attributes.get("rating");

		if (rating != null) {
			setRating(rating);
		}

		Long projectRef = (Long)attributes.get("projectRef");

		if (projectRef != null) {
			setProjectRef(projectRef);
		}

		Long videoFileRef = (Long)attributes.get("videoFileRef");

		if (videoFileRef != null) {
			setVideoFileRef(videoFileRef);
		}

		String pageUrl = (String)attributes.get("pageUrl");

		if (pageUrl != null) {
			setPageUrl(pageUrl);
		}

		Long layoutRef = (Long)attributes.get("layoutRef");

		if (layoutRef != null) {
			setLayoutRef(layoutRef);
		}

		String pitch = (String)attributes.get("pitch");

		if (pitch != null) {
			setPitch(pitch);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}
	}

	/**
	* Returns the is visible on map of this ideas.
	*
	* @return the is visible on map of this ideas
	*/
	@Override
	public boolean getIsVisibleOnMap() {
		return _ideas.getIsVisibleOnMap();
	}

	/**
	* Returns the published of this ideas.
	*
	* @return the published of this ideas
	*/
	@Override
	public boolean getPublished() {
		return _ideas.getPublished();
	}

	/**
	* Returns <code>true</code> if this ideas is approved.
	*
	* @return <code>true</code> if this ideas is approved; <code>false</code> otherwise
	*/
	@Override
	public boolean isApproved() {
		return _ideas.isApproved();
	}

	@Override
	public boolean isCachedModel() {
		return _ideas.isCachedModel();
	}

	/**
	* Returns <code>true</code> if this ideas is denied.
	*
	* @return <code>true</code> if this ideas is denied; <code>false</code> otherwise
	*/
	@Override
	public boolean isDenied() {
		return _ideas.isDenied();
	}

	/**
	* Returns <code>true</code> if this ideas is a draft.
	*
	* @return <code>true</code> if this ideas is a draft; <code>false</code> otherwise
	*/
	@Override
	public boolean isDraft() {
		return _ideas.isDraft();
	}

	@Override
	public boolean isEscapedModel() {
		return _ideas.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this ideas is expired.
	*
	* @return <code>true</code> if this ideas is expired; <code>false</code> otherwise
	*/
	@Override
	public boolean isExpired() {
		return _ideas.isExpired();
	}

	/**
	* Returns <code>true</code> if this ideas is inactive.
	*
	* @return <code>true</code> if this ideas is inactive; <code>false</code> otherwise
	*/
	@Override
	public boolean isInactive() {
		return _ideas.isInactive();
	}

	/**
	* Returns <code>true</code> if this ideas is incomplete.
	*
	* @return <code>true</code> if this ideas is incomplete; <code>false</code> otherwise
	*/
	@Override
	public boolean isIncomplete() {
		return _ideas.isIncomplete();
	}

	/**
	* Returns <code>true</code> if this ideas is is visible on map.
	*
	* @return <code>true</code> if this ideas is is visible on map; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsVisibleOnMap() {
		return _ideas.isIsVisibleOnMap();
	}

	@Override
	public boolean isNew() {
		return _ideas.isNew();
	}

	/**
	* Returns <code>true</code> if this ideas is pending.
	*
	* @return <code>true</code> if this ideas is pending; <code>false</code> otherwise
	*/
	@Override
	public boolean isPending() {
		return _ideas.isPending();
	}

	/**
	* Returns <code>true</code> if this ideas is published.
	*
	* @return <code>true</code> if this ideas is published; <code>false</code> otherwise
	*/
	@Override
	public boolean isPublished() {
		return _ideas.isPublished();
	}

	/**
	* Returns <code>true</code> if this ideas is scheduled.
	*
	* @return <code>true</code> if this ideas is scheduled; <code>false</code> otherwise
	*/
	@Override
	public boolean isScheduled() {
		return _ideas.isScheduled();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _ideas.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Ideas> toCacheModel() {
		return _ideas.toCacheModel();
	}

	/**
	* Returns the latitude of this ideas.
	*
	* @return the latitude of this ideas
	*/
	@Override
	public double getLatitude() {
		return _ideas.getLatitude();
	}

	/**
	* Returns the longitude of this ideas.
	*
	* @return the longitude of this ideas
	*/
	@Override
	public double getLongitude() {
		return _ideas.getLongitude();
	}

	@Override
	public ideaService.model.Ideas toEscapedModel() {
		return new IdeasWrapper(_ideas.toEscapedModel());
	}

	@Override
	public ideaService.model.Ideas toUnescapedModel() {
		return new IdeasWrapper(_ideas.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Ideas ideas) {
		return _ideas.compareTo(ideas);
	}

	/**
	* Returns the status of this ideas.
	*
	* @return the status of this ideas
	*/
	@Override
	public int getStatus() {
		return _ideas.getStatus();
	}

	@Override
	public int hashCode() {
		return _ideas.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ideas.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new IdeasWrapper((Ideas)_ideas.clone());
	}

	/**
	* Returns the description of this ideas.
	*
	* @return the description of this ideas
	*/
	@Override
	public java.lang.String getDescription() {
		return _ideas.getDescription();
	}

	/**
	* Returns the goal of this ideas.
	*
	* @return the goal of this ideas
	*/
	@Override
	public java.lang.String getGoal() {
		return _ideas.getGoal();
	}

	/**
	* Returns the importance of this ideas.
	*
	* @return the importance of this ideas
	*/
	@Override
	public java.lang.String getImportance() {
		return _ideas.getImportance();
	}

	/**
	* Returns the page url of this ideas.
	*
	* @return the page url of this ideas
	*/
	@Override
	public java.lang.String getPageUrl() {
		return _ideas.getPageUrl();
	}

	/**
	* Returns the pitch of this ideas.
	*
	* @return the pitch of this ideas
	*/
	@Override
	public java.lang.String getPitch() {
		return _ideas.getPitch();
	}

	/**
	* Returns the rating of this ideas.
	*
	* @return the rating of this ideas
	*/
	@Override
	public java.lang.String getRating() {
		return _ideas.getRating();
	}

	/**
	* Returns the review status of this ideas.
	*
	* @return the review status of this ideas
	*/
	@Override
	public java.lang.String getReviewStatus() {
		return _ideas.getReviewStatus();
	}

	/**
	* Returns the shortdescription of this ideas.
	*
	* @return the shortdescription of this ideas
	*/
	@Override
	public java.lang.String getShortdescription() {
		return _ideas.getShortdescription();
	}

	/**
	* Returns the solution of this ideas.
	*
	* @return the solution of this ideas
	*/
	@Override
	public java.lang.String getSolution() {
		return _ideas.getSolution();
	}

	/**
	* Returns the status by user name of this ideas.
	*
	* @return the status by user name of this ideas
	*/
	@Override
	public java.lang.String getStatusByUserName() {
		return _ideas.getStatusByUserName();
	}

	/**
	* Returns the status by user uuid of this ideas.
	*
	* @return the status by user uuid of this ideas
	*/
	@Override
	public java.lang.String getStatusByUserUuid() {
		return _ideas.getStatusByUserUuid();
	}

	/**
	* Returns the tags of this ideas.
	*
	* @return the tags of this ideas
	*/
	@Override
	public java.lang.String getTags() {
		return _ideas.getTags();
	}

	/**
	* Returns the target audience of this ideas.
	*
	* @return the target audience of this ideas
	*/
	@Override
	public java.lang.String getTargetAudience() {
		return _ideas.getTargetAudience();
	}

	/**
	* Returns the title of this ideas.
	*
	* @return the title of this ideas
	*/
	@Override
	public java.lang.String getTitle() {
		return _ideas.getTitle();
	}

	/**
	* Returns the user name of this ideas.
	*
	* @return the user name of this ideas
	*/
	@Override
	public java.lang.String getUserName() {
		return _ideas.getUserName();
	}

	/**
	* Returns the user uuid of this ideas.
	*
	* @return the user uuid of this ideas
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _ideas.getUserUuid();
	}

	/**
	* Returns the uuid of this ideas.
	*
	* @return the uuid of this ideas
	*/
	@Override
	public java.lang.String getUuid() {
		return _ideas.getUuid();
	}

	/**
	* Returns the video url of this ideas.
	*
	* @return the video url of this ideas
	*/
	@Override
	public java.lang.String getVideoUrl() {
		return _ideas.getVideoUrl();
	}

	@Override
	public java.lang.String toString() {
		return _ideas.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ideas.toXmlString();
	}

	/**
	* Returns the create date of this ideas.
	*
	* @return the create date of this ideas
	*/
	@Override
	public Date getCreateDate() {
		return _ideas.getCreateDate();
	}

	/**
	* Returns the modified date of this ideas.
	*
	* @return the modified date of this ideas
	*/
	@Override
	public Date getModifiedDate() {
		return _ideas.getModifiedDate();
	}

	/**
	* Returns the status date of this ideas.
	*
	* @return the status date of this ideas
	*/
	@Override
	public Date getStatusDate() {
		return _ideas.getStatusDate();
	}

	/**
	* Returns the category of this ideas.
	*
	* @return the category of this ideas
	*/
	@Override
	public long getCategory() {
		return _ideas.getCategory();
	}

	/**
	* Returns the company ID of this ideas.
	*
	* @return the company ID of this ideas
	*/
	@Override
	public long getCompanyId() {
		return _ideas.getCompanyId();
	}

	/**
	* Returns the group ID of this ideas.
	*
	* @return the group ID of this ideas
	*/
	@Override
	public long getGroupId() {
		return _ideas.getGroupId();
	}

	/**
	* Returns the ideas ID of this ideas.
	*
	* @return the ideas ID of this ideas
	*/
	@Override
	public long getIdeasId() {
		return _ideas.getIdeasId();
	}

	/**
	* Returns the layout ref of this ideas.
	*
	* @return the layout ref of this ideas
	*/
	@Override
	public long getLayoutRef() {
		return _ideas.getLayoutRef();
	}

	/**
	* Returns the primary key of this ideas.
	*
	* @return the primary key of this ideas
	*/
	@Override
	public long getPrimaryKey() {
		return _ideas.getPrimaryKey();
	}

	/**
	* Returns the project ref of this ideas.
	*
	* @return the project ref of this ideas
	*/
	@Override
	public long getProjectRef() {
		return _ideas.getProjectRef();
	}

	/**
	* Returns the status by user ID of this ideas.
	*
	* @return the status by user ID of this ideas
	*/
	@Override
	public long getStatusByUserId() {
		return _ideas.getStatusByUserId();
	}

	/**
	* Returns the user ID of this ideas.
	*
	* @return the user ID of this ideas
	*/
	@Override
	public long getUserId() {
		return _ideas.getUserId();
	}

	/**
	* Returns the video file ref of this ideas.
	*
	* @return the video file ref of this ideas
	*/
	@Override
	public long getVideoFileRef() {
		return _ideas.getVideoFileRef();
	}

	@Override
	public void persist() {
		_ideas.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ideas.setCachedModel(cachedModel);
	}

	/**
	* Sets the category of this ideas.
	*
	* @param category the category of this ideas
	*/
	@Override
	public void setCategory(long category) {
		_ideas.setCategory(category);
	}

	/**
	* Sets the company ID of this ideas.
	*
	* @param companyId the company ID of this ideas
	*/
	@Override
	public void setCompanyId(long companyId) {
		_ideas.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this ideas.
	*
	* @param createDate the create date of this ideas
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_ideas.setCreateDate(createDate);
	}

	/**
	* Sets the description of this ideas.
	*
	* @param description the description of this ideas
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_ideas.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_ideas.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_ideas.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_ideas.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the goal of this ideas.
	*
	* @param goal the goal of this ideas
	*/
	@Override
	public void setGoal(java.lang.String goal) {
		_ideas.setGoal(goal);
	}

	/**
	* Sets the group ID of this ideas.
	*
	* @param groupId the group ID of this ideas
	*/
	@Override
	public void setGroupId(long groupId) {
		_ideas.setGroupId(groupId);
	}

	/**
	* Sets the ideas ID of this ideas.
	*
	* @param ideasId the ideas ID of this ideas
	*/
	@Override
	public void setIdeasId(long ideasId) {
		_ideas.setIdeasId(ideasId);
	}

	/**
	* Sets the importance of this ideas.
	*
	* @param importance the importance of this ideas
	*/
	@Override
	public void setImportance(java.lang.String importance) {
		_ideas.setImportance(importance);
	}

	/**
	* Sets whether this ideas is is visible on map.
	*
	* @param isVisibleOnMap the is visible on map of this ideas
	*/
	@Override
	public void setIsVisibleOnMap(boolean isVisibleOnMap) {
		_ideas.setIsVisibleOnMap(isVisibleOnMap);
	}

	/**
	* Sets the latitude of this ideas.
	*
	* @param latitude the latitude of this ideas
	*/
	@Override
	public void setLatitude(double latitude) {
		_ideas.setLatitude(latitude);
	}

	/**
	* Sets the layout ref of this ideas.
	*
	* @param layoutRef the layout ref of this ideas
	*/
	@Override
	public void setLayoutRef(long layoutRef) {
		_ideas.setLayoutRef(layoutRef);
	}

	/**
	* Sets the longitude of this ideas.
	*
	* @param longitude the longitude of this ideas
	*/
	@Override
	public void setLongitude(double longitude) {
		_ideas.setLongitude(longitude);
	}

	/**
	* Sets the modified date of this ideas.
	*
	* @param modifiedDate the modified date of this ideas
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_ideas.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_ideas.setNew(n);
	}

	/**
	* Sets the page url of this ideas.
	*
	* @param pageUrl the page url of this ideas
	*/
	@Override
	public void setPageUrl(java.lang.String pageUrl) {
		_ideas.setPageUrl(pageUrl);
	}

	/**
	* Sets the pitch of this ideas.
	*
	* @param pitch the pitch of this ideas
	*/
	@Override
	public void setPitch(java.lang.String pitch) {
		_ideas.setPitch(pitch);
	}

	/**
	* Sets the primary key of this ideas.
	*
	* @param primaryKey the primary key of this ideas
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ideas.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_ideas.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the project ref of this ideas.
	*
	* @param projectRef the project ref of this ideas
	*/
	@Override
	public void setProjectRef(long projectRef) {
		_ideas.setProjectRef(projectRef);
	}

	/**
	* Sets whether this ideas is published.
	*
	* @param published the published of this ideas
	*/
	@Override
	public void setPublished(boolean published) {
		_ideas.setPublished(published);
	}

	/**
	* Sets the rating of this ideas.
	*
	* @param rating the rating of this ideas
	*/
	@Override
	public void setRating(java.lang.String rating) {
		_ideas.setRating(rating);
	}

	/**
	* Sets the review status of this ideas.
	*
	* @param reviewStatus the review status of this ideas
	*/
	@Override
	public void setReviewStatus(java.lang.String reviewStatus) {
		_ideas.setReviewStatus(reviewStatus);
	}

	/**
	* Sets the shortdescription of this ideas.
	*
	* @param shortdescription the shortdescription of this ideas
	*/
	@Override
	public void setShortdescription(java.lang.String shortdescription) {
		_ideas.setShortdescription(shortdescription);
	}

	/**
	* Sets the solution of this ideas.
	*
	* @param solution the solution of this ideas
	*/
	@Override
	public void setSolution(java.lang.String solution) {
		_ideas.setSolution(solution);
	}

	/**
	* Sets the status of this ideas.
	*
	* @param status the status of this ideas
	*/
	@Override
	public void setStatus(int status) {
		_ideas.setStatus(status);
	}

	/**
	* Sets the status by user ID of this ideas.
	*
	* @param statusByUserId the status by user ID of this ideas
	*/
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_ideas.setStatusByUserId(statusByUserId);
	}

	/**
	* Sets the status by user name of this ideas.
	*
	* @param statusByUserName the status by user name of this ideas
	*/
	@Override
	public void setStatusByUserName(java.lang.String statusByUserName) {
		_ideas.setStatusByUserName(statusByUserName);
	}

	/**
	* Sets the status by user uuid of this ideas.
	*
	* @param statusByUserUuid the status by user uuid of this ideas
	*/
	@Override
	public void setStatusByUserUuid(java.lang.String statusByUserUuid) {
		_ideas.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	* Sets the status date of this ideas.
	*
	* @param statusDate the status date of this ideas
	*/
	@Override
	public void setStatusDate(Date statusDate) {
		_ideas.setStatusDate(statusDate);
	}

	/**
	* Sets the tags of this ideas.
	*
	* @param tags the tags of this ideas
	*/
	@Override
	public void setTags(java.lang.String tags) {
		_ideas.setTags(tags);
	}

	/**
	* Sets the target audience of this ideas.
	*
	* @param targetAudience the target audience of this ideas
	*/
	@Override
	public void setTargetAudience(java.lang.String targetAudience) {
		_ideas.setTargetAudience(targetAudience);
	}

	/**
	* Sets the title of this ideas.
	*
	* @param title the title of this ideas
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_ideas.setTitle(title);
	}

	/**
	* Sets the user ID of this ideas.
	*
	* @param userId the user ID of this ideas
	*/
	@Override
	public void setUserId(long userId) {
		_ideas.setUserId(userId);
	}

	/**
	* Sets the user name of this ideas.
	*
	* @param userName the user name of this ideas
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_ideas.setUserName(userName);
	}

	/**
	* Sets the user uuid of this ideas.
	*
	* @param userUuid the user uuid of this ideas
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_ideas.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this ideas.
	*
	* @param uuid the uuid of this ideas
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_ideas.setUuid(uuid);
	}

	/**
	* Sets the video file ref of this ideas.
	*
	* @param videoFileRef the video file ref of this ideas
	*/
	@Override
	public void setVideoFileRef(long videoFileRef) {
		_ideas.setVideoFileRef(videoFileRef);
	}

	/**
	* Sets the video url of this ideas.
	*
	* @param videoUrl the video url of this ideas
	*/
	@Override
	public void setVideoUrl(java.lang.String videoUrl) {
		_ideas.setVideoUrl(videoUrl);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeasWrapper)) {
			return false;
		}

		IdeasWrapper ideasWrapper = (IdeasWrapper)obj;

		if (Objects.equals(_ideas, ideasWrapper._ideas)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _ideas.getStagedModelType();
	}

	@Override
	public Ideas getWrappedModel() {
		return _ideas;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _ideas.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _ideas.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_ideas.resetOriginalValues();
	}

	private final Ideas _ideas;
}