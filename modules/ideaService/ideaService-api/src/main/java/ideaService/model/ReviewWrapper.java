/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Review}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Review
 * @generated
 */
@ProviderType
public class ReviewWrapper implements Review, ModelWrapper<Review> {
	public ReviewWrapper(Review review) {
		_review = review;
	}

	@Override
	public Class<?> getModelClass() {
		return Review.class;
	}

	@Override
	public String getModelClassName() {
		return Review.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("reviewId", getReviewId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("accepted", getAccepted());
		attributes.put("feedback", getFeedback());
		attributes.put("ideasIdRef", getIdeasIdRef());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long reviewId = (Long)attributes.get("reviewId");

		if (reviewId != null) {
			setReviewId(reviewId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Boolean accepted = (Boolean)attributes.get("accepted");

		if (accepted != null) {
			setAccepted(accepted);
		}

		String feedback = (String)attributes.get("feedback");

		if (feedback != null) {
			setFeedback(feedback);
		}

		Long ideasIdRef = (Long)attributes.get("ideasIdRef");

		if (ideasIdRef != null) {
			setIdeasIdRef(ideasIdRef);
		}
	}

	/**
	* Returns the accepted of this review.
	*
	* @return the accepted of this review
	*/
	@Override
	public boolean getAccepted() {
		return _review.getAccepted();
	}

	/**
	* Returns <code>true</code> if this review is accepted.
	*
	* @return <code>true</code> if this review is accepted; <code>false</code> otherwise
	*/
	@Override
	public boolean isAccepted() {
		return _review.isAccepted();
	}

	@Override
	public boolean isCachedModel() {
		return _review.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _review.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _review.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _review.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Review> toCacheModel() {
		return _review.toCacheModel();
	}

	@Override
	public ideaService.model.Review toEscapedModel() {
		return new ReviewWrapper(_review.toEscapedModel());
	}

	@Override
	public ideaService.model.Review toUnescapedModel() {
		return new ReviewWrapper(_review.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Review review) {
		return _review.compareTo(review);
	}

	@Override
	public int hashCode() {
		return _review.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _review.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ReviewWrapper((Review)_review.clone());
	}

	/**
	* Returns the feedback of this review.
	*
	* @return the feedback of this review
	*/
	@Override
	public java.lang.String getFeedback() {
		return _review.getFeedback();
	}

	/**
	* Returns the user name of this review.
	*
	* @return the user name of this review
	*/
	@Override
	public java.lang.String getUserName() {
		return _review.getUserName();
	}

	/**
	* Returns the user uuid of this review.
	*
	* @return the user uuid of this review
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _review.getUserUuid();
	}

	/**
	* Returns the uuid of this review.
	*
	* @return the uuid of this review
	*/
	@Override
	public java.lang.String getUuid() {
		return _review.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _review.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _review.toXmlString();
	}

	/**
	* Returns the create date of this review.
	*
	* @return the create date of this review
	*/
	@Override
	public Date getCreateDate() {
		return _review.getCreateDate();
	}

	/**
	* Returns the modified date of this review.
	*
	* @return the modified date of this review
	*/
	@Override
	public Date getModifiedDate() {
		return _review.getModifiedDate();
	}

	/**
	* Returns the company ID of this review.
	*
	* @return the company ID of this review
	*/
	@Override
	public long getCompanyId() {
		return _review.getCompanyId();
	}

	/**
	* Returns the group ID of this review.
	*
	* @return the group ID of this review
	*/
	@Override
	public long getGroupId() {
		return _review.getGroupId();
	}

	/**
	* Returns the ideas ID ref of this review.
	*
	* @return the ideas ID ref of this review
	*/
	@Override
	public long getIdeasIdRef() {
		return _review.getIdeasIdRef();
	}

	/**
	* Returns the primary key of this review.
	*
	* @return the primary key of this review
	*/
	@Override
	public long getPrimaryKey() {
		return _review.getPrimaryKey();
	}

	/**
	* Returns the review ID of this review.
	*
	* @return the review ID of this review
	*/
	@Override
	public long getReviewId() {
		return _review.getReviewId();
	}

	/**
	* Returns the user ID of this review.
	*
	* @return the user ID of this review
	*/
	@Override
	public long getUserId() {
		return _review.getUserId();
	}

	@Override
	public void persist() {
		_review.persist();
	}

	/**
	* Sets whether this review is accepted.
	*
	* @param accepted the accepted of this review
	*/
	@Override
	public void setAccepted(boolean accepted) {
		_review.setAccepted(accepted);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_review.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this review.
	*
	* @param companyId the company ID of this review
	*/
	@Override
	public void setCompanyId(long companyId) {
		_review.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this review.
	*
	* @param createDate the create date of this review
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_review.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_review.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_review.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_review.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the feedback of this review.
	*
	* @param feedback the feedback of this review
	*/
	@Override
	public void setFeedback(java.lang.String feedback) {
		_review.setFeedback(feedback);
	}

	/**
	* Sets the group ID of this review.
	*
	* @param groupId the group ID of this review
	*/
	@Override
	public void setGroupId(long groupId) {
		_review.setGroupId(groupId);
	}

	/**
	* Sets the ideas ID ref of this review.
	*
	* @param ideasIdRef the ideas ID ref of this review
	*/
	@Override
	public void setIdeasIdRef(long ideasIdRef) {
		_review.setIdeasIdRef(ideasIdRef);
	}

	/**
	* Sets the modified date of this review.
	*
	* @param modifiedDate the modified date of this review
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_review.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_review.setNew(n);
	}

	/**
	* Sets the primary key of this review.
	*
	* @param primaryKey the primary key of this review
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_review.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_review.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the review ID of this review.
	*
	* @param reviewId the review ID of this review
	*/
	@Override
	public void setReviewId(long reviewId) {
		_review.setReviewId(reviewId);
	}

	/**
	* Sets the user ID of this review.
	*
	* @param userId the user ID of this review
	*/
	@Override
	public void setUserId(long userId) {
		_review.setUserId(userId);
	}

	/**
	* Sets the user name of this review.
	*
	* @param userName the user name of this review
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_review.setUserName(userName);
	}

	/**
	* Sets the user uuid of this review.
	*
	* @param userUuid the user uuid of this review
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_review.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this review.
	*
	* @param uuid the uuid of this review
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_review.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReviewWrapper)) {
			return false;
		}

		ReviewWrapper reviewWrapper = (ReviewWrapper)obj;

		if (Objects.equals(_review, reviewWrapper._review)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _review.getStagedModelType();
	}

	@Override
	public Review getWrappedModel() {
		return _review;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _review.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _review.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_review.resetOriginalValues();
	}

	private final Review _review;
}