/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link ideaService.service.http.PicturesServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.http.PicturesServiceSoap
 * @generated
 */
@ProviderType
public class PicturesSoap implements Serializable {
	public static PicturesSoap toSoapModel(Pictures model) {
		PicturesSoap soapModel = new PicturesSoap();

		soapModel.setPictureId(model.getPictureId());
		soapModel.setIdeasRef(model.getIdeasRef());
		soapModel.setFileRef(model.getFileRef());
		soapModel.setPictureUrl(model.getPictureUrl());
		soapModel.setPosition(model.getPosition());

		return soapModel;
	}

	public static PicturesSoap[] toSoapModels(Pictures[] models) {
		PicturesSoap[] soapModels = new PicturesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PicturesSoap[][] toSoapModels(Pictures[][] models) {
		PicturesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PicturesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PicturesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PicturesSoap[] toSoapModels(List<Pictures> models) {
		List<PicturesSoap> soapModels = new ArrayList<PicturesSoap>(models.size());

		for (Pictures model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PicturesSoap[soapModels.size()]);
	}

	public PicturesSoap() {
	}

	public long getPrimaryKey() {
		return _PictureId;
	}

	public void setPrimaryKey(long pk) {
		setPictureId(pk);
	}

	public long getPictureId() {
		return _PictureId;
	}

	public void setPictureId(long PictureId) {
		_PictureId = PictureId;
	}

	public long getIdeasRef() {
		return _IdeasRef;
	}

	public void setIdeasRef(long IdeasRef) {
		_IdeasRef = IdeasRef;
	}

	public long getFileRef() {
		return _FileRef;
	}

	public void setFileRef(long FileRef) {
		_FileRef = FileRef;
	}

	public String getPictureUrl() {
		return _PictureUrl;
	}

	public void setPictureUrl(String PictureUrl) {
		_PictureUrl = PictureUrl;
	}

	public int getPosition() {
		return _Position;
	}

	public void setPosition(int Position) {
		_Position = Position;
	}

	private long _PictureId;
	private long _IdeasRef;
	private long _FileRef;
	private String _PictureUrl;
	private int _Position;
}