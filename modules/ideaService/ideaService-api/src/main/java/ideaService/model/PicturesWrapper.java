/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Pictures}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Pictures
 * @generated
 */
@ProviderType
public class PicturesWrapper implements Pictures, ModelWrapper<Pictures> {
	public PicturesWrapper(Pictures pictures) {
		_pictures = pictures;
	}

	@Override
	public Class<?> getModelClass() {
		return Pictures.class;
	}

	@Override
	public String getModelClassName() {
		return Pictures.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("PictureId", getPictureId());
		attributes.put("IdeasRef", getIdeasRef());
		attributes.put("FileRef", getFileRef());
		attributes.put("PictureUrl", getPictureUrl());
		attributes.put("Position", getPosition());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long PictureId = (Long)attributes.get("PictureId");

		if (PictureId != null) {
			setPictureId(PictureId);
		}

		Long IdeasRef = (Long)attributes.get("IdeasRef");

		if (IdeasRef != null) {
			setIdeasRef(IdeasRef);
		}

		Long FileRef = (Long)attributes.get("FileRef");

		if (FileRef != null) {
			setFileRef(FileRef);
		}

		String PictureUrl = (String)attributes.get("PictureUrl");

		if (PictureUrl != null) {
			setPictureUrl(PictureUrl);
		}

		Integer Position = (Integer)attributes.get("Position");

		if (Position != null) {
			setPosition(Position);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _pictures.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _pictures.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _pictures.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _pictures.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Pictures> toCacheModel() {
		return _pictures.toCacheModel();
	}

	@Override
	public ideaService.model.Pictures toEscapedModel() {
		return new PicturesWrapper(_pictures.toEscapedModel());
	}

	@Override
	public ideaService.model.Pictures toUnescapedModel() {
		return new PicturesWrapper(_pictures.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Pictures pictures) {
		return _pictures.compareTo(pictures);
	}

	/**
	* Returns the position of this pictures.
	*
	* @return the position of this pictures
	*/
	@Override
	public int getPosition() {
		return _pictures.getPosition();
	}

	@Override
	public int hashCode() {
		return _pictures.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _pictures.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new PicturesWrapper((Pictures)_pictures.clone());
	}

	/**
	* Returns the picture url of this pictures.
	*
	* @return the picture url of this pictures
	*/
	@Override
	public java.lang.String getPictureUrl() {
		return _pictures.getPictureUrl();
	}

	@Override
	public java.lang.String toString() {
		return _pictures.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _pictures.toXmlString();
	}

	/**
	* Returns the file ref of this pictures.
	*
	* @return the file ref of this pictures
	*/
	@Override
	public long getFileRef() {
		return _pictures.getFileRef();
	}

	/**
	* Returns the ideas ref of this pictures.
	*
	* @return the ideas ref of this pictures
	*/
	@Override
	public long getIdeasRef() {
		return _pictures.getIdeasRef();
	}

	/**
	* Returns the picture ID of this pictures.
	*
	* @return the picture ID of this pictures
	*/
	@Override
	public long getPictureId() {
		return _pictures.getPictureId();
	}

	/**
	* Returns the primary key of this pictures.
	*
	* @return the primary key of this pictures
	*/
	@Override
	public long getPrimaryKey() {
		return _pictures.getPrimaryKey();
	}

	@Override
	public void persist() {
		_pictures.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_pictures.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_pictures.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_pictures.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_pictures.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the file ref of this pictures.
	*
	* @param FileRef the file ref of this pictures
	*/
	@Override
	public void setFileRef(long FileRef) {
		_pictures.setFileRef(FileRef);
	}

	/**
	* Sets the ideas ref of this pictures.
	*
	* @param IdeasRef the ideas ref of this pictures
	*/
	@Override
	public void setIdeasRef(long IdeasRef) {
		_pictures.setIdeasRef(IdeasRef);
	}

	@Override
	public void setNew(boolean n) {
		_pictures.setNew(n);
	}

	/**
	* Sets the picture ID of this pictures.
	*
	* @param PictureId the picture ID of this pictures
	*/
	@Override
	public void setPictureId(long PictureId) {
		_pictures.setPictureId(PictureId);
	}

	/**
	* Sets the picture url of this pictures.
	*
	* @param PictureUrl the picture url of this pictures
	*/
	@Override
	public void setPictureUrl(java.lang.String PictureUrl) {
		_pictures.setPictureUrl(PictureUrl);
	}

	/**
	* Sets the position of this pictures.
	*
	* @param Position the position of this pictures
	*/
	@Override
	public void setPosition(int Position) {
		_pictures.setPosition(Position);
	}

	/**
	* Sets the primary key of this pictures.
	*
	* @param primaryKey the primary key of this pictures
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_pictures.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_pictures.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PicturesWrapper)) {
			return false;
		}

		PicturesWrapper picturesWrapper = (PicturesWrapper)obj;

		if (Objects.equals(_pictures, picturesWrapper._pictures)) {
			return true;
		}

		return false;
	}

	@Override
	public Pictures getWrappedModel() {
		return _pictures;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _pictures.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _pictures.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_pictures.resetOriginalValues();
	}

	private final Pictures _pictures;
}