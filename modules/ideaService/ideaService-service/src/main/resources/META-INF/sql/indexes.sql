create index IX_E47974FE on IDEA_Category (categoryTitle[$COLUMN_LENGTH:75$]);
create index IX_DE188690 on IDEA_Category (projectRef, categoryTitle[$COLUMN_LENGTH:75$]);
create index IX_6BE7B2A0 on IDEA_Category (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_72FC6122 on IDEA_Category (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_827B8916 on IDEA_Ideas (groupId, status);
create index IX_F7C6C27F on IDEA_Ideas (layoutRef);
create index IX_9D6770A0 on IDEA_Ideas (status);
create index IX_6AC4532E on IDEA_Ideas (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_1603CD30 on IDEA_Ideas (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_CE1008CB on IDEA_Pictures (IdeasRef, Position);
create index IX_35132806 on IDEA_Pictures (PictureId);
create index IX_24ED7260 on IDEA_Pictures (Position);

create index IX_5EE35FA on IDEA_Review (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_A79FF2FC on IDEA_Review (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_F8807AC9 on IDEA_Videos (Extension[$COLUMN_LENGTH:75$]);
create index IX_CC16BDF1 on IDEA_Videos (IdeasRef);