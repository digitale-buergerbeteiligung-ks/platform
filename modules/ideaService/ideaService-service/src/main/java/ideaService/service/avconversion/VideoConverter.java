package ideaService.service.avconversion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import org.osgi.service.component.annotations.Component;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.portal.kernel.backgroundtask.BackgroundTask;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskConstants;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskResult;
import com.liferay.portal.kernel.backgroundtask.BaseBackgroundTaskExecutor;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplay;
import com.liferay.portal.kernel.backgroundtask.display.BackgroundTaskDisplayFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.MimeTypesUtil;

import ideaService.model.Ideas;
import ideaService.service.VideosLocalServiceUtil;
import ideaService.service.persistence.IdeasUtil;

@Component(immediate = true, property = {"background.task.executor.class.name=ideaService.service.avconversion.VideoConverter"}, service = BackgroundTaskExecutor.class)
public class VideoConverter extends BaseBackgroundTaskExecutor {

    private static Log logger = LogFactoryUtil.getLog(VideoConverter.class);

    @Override
    public BackgroundTaskResult execute(BackgroundTask backgroundTask) throws Exception {
        logger.info("---------------------Executing background task started ---------------------");
        Map<String, Serializable> taskContextMap = backgroundTask.getTaskContextMap();

        long ideasId =  (long) taskContextMap.get("ideasId");
        long videoId = (long) taskContextMap.get("videoId");
        String convertTo = (String) taskContextMap.get("convertTo");
        String portalUrl = (String) taskContextMap.get("portalUrl");
        String contextPath = (String) taskContextMap.get("contextPath");
        long gId = (long) taskContextMap.get("gId");

        //Get FileEntry from Liferay Docs-Library
        DLFileEntry dlEntry = DLFileEntryLocalServiceUtil.getFileEntry(videoId);
        //Get InputStream from File
        InputStream videoInputStream = DLFileEntryLocalServiceUtil.getFileAsStream(videoId, dlEntry.getVersion());
        //Create File From Inputstream. Even when converting to ogg and webm in parallel creating one source file is enough. Hence, the same name.
        //However, filename still needs to be distinct in case of converting two files at the same time.
        File tmpSourceFile = new File("tmpSourceFileOF" + dlEntry.getFileName() );
        tmpSourceFile.deleteOnExit();
        FileOutputStream out = new FileOutputStream(tmpSourceFile);
        copyStream(videoInputStream,out);
        out.close();
        logger.info("-------------------------------- Created temp file at"  + tmpSourceFile.getAbsolutePath() + " -----------------------------------");

        String outputFileName = null;
        Process p = null;
        if(convertTo.equals("webm")){
        	logger.info("-------------------------- Started converting to WEBM  ----------------------------------------");
        	outputFileName = dlEntry.getFileName().substring(0, dlEntry.getFileName().length()-4) + ".webm";
        	p = Runtime.getRuntime().exec("ffmpeg -i " + tmpSourceFile.getAbsolutePath() +" -c:v libvpx -crf 10 -b:v 1M -c:a libvorbis " + outputFileName);
        }
        else if(convertTo.equals("ogv")){
        	logger.info("-------------------------- Started converting to OGG ----------------------------------------");
        	outputFileName = dlEntry.getFileName().substring(0, dlEntry.getFileName().length()-4) + ".ogv";
        	p = Runtime.getRuntime().exec("ffmpeg -i " + tmpSourceFile.getAbsolutePath() +" -codec:v libtheora -qscale:v 7 -codec:a libvorbis -qscale:a 5 " + outputFileName);
        }
        else if(convertTo.equals("mp4")){
        	logger.info("-------------------------- Started converting to MP4 H264 ----------------------------------------");
        	outputFileName = dlEntry.getFileName().substring(0, dlEntry.getFileName().length()-4) + ".mp4";
        	p = Runtime.getRuntime().exec("ffmpeg -i " + tmpSourceFile.getAbsolutePath() +" -vcodec h264 -acodec aac -strict -2 " + outputFileName);
        }

        String line;
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while ((line = in.readLine()) != null) {
            //logger.info(line);
          }
        while(p.isAlive()){
        	// make sure the process is finished
        }
        in.close();

        File tmpOutputFile = new File (outputFileName);
        logger.info("-------------------------------- Conversion finished. Created temp file at "  + tmpOutputFile.getAbsolutePath() +  " -----------------------------------");



        logger.info("------------------------------- Saving converted file to Liferay Docs library -----------------------------------");

        Ideas i = IdeasUtil.findByPrimaryKey(ideasId);

        String title = i.getUserName() + "IDEAS_VIDEO"  + UUID.randomUUID () + "." + convertTo;
        FileEntry createdEntry =  DLAppLocalServiceUtil.addFileEntry(i.getUserId(), gId,
        		dlEntry.getFolderId(), tmpOutputFile.getName(),
        		MimeTypesUtil.getContentType(tmpOutputFile), title,
        		"converted video file", "none", tmpOutputFile, new ServiceContext());

        String fileUrl = getFullyQualifiedUrlForFileEntry(portalUrl, contextPath, gId, createdEntry);

        logger.info("CREATED ENTRY: " + fileUrl);
        VideosLocalServiceUtil.createNewVideoEntry(i.getPrimaryKey(), createdEntry.getPrimaryKey(), fileUrl, convertTo);

        logger.info("-------------------------------- Deleting temp files -----------------------------------");
        tmpSourceFile.delete();
        tmpOutputFile.delete();

        logger.info("-------------------------------- Updating File Permission -----------------------------------");
        updateResourcePermission(createdEntry);

        logger.info("--------------------- Execution of background task finished ---------------------");
        return BackgroundTaskResult.SUCCESS;
    }

    private synchronized void updateResourcePermission(FileEntry createdEntry) throws PortalException {

    	Role siteMemberRole = RoleLocalServiceUtil.getRole(createdEntry.getCompanyId(), RoleConstants.GUEST);

    	ResourceAction resourceAction = ResourceActionLocalServiceUtil.getResourceAction(DLFileEntry.class.getName(), ActionKeys.VIEW);

    	ResourcePermission  resourcePermission = ResourcePermissionLocalServiceUtil
	    		.createResourcePermission(CounterLocalServiceUtil.increment());
	    		resourcePermission.setCompanyId(createdEntry.getCompanyId());
	    		resourcePermission.setName(DLFileEntry.class.getName());
	    		resourcePermission.setScope(ResourceConstants.SCOPE_INDIVIDUAL);
	    		resourcePermission.setPrimKey(String.valueOf(createdEntry.getPrimaryKey()));
	    		resourcePermission.setRoleId(siteMemberRole.getRoleId());
	    		resourcePermission.setActionIds(resourceAction.getBitwiseValue());

	    		ResourcePermissionLocalServiceUtil.addResourcePermission(resourcePermission);
	}

	@Override
    public BackgroundTaskDisplay getBackgroundTaskDisplay(BackgroundTask backgroundTask) {
        return BackgroundTaskDisplayFactoryUtil.getBackgroundTaskDisplay(backgroundTask);
    }

    @Override
    public int getIsolationLevel(){
        return BackgroundTaskConstants.ISOLATION_LEVEL_TASK_NAME;
    }

    @Override
    public BackgroundTaskExecutor clone() {
        return this;
    }


    @Override
    public boolean isSerial(){
        return false;
    }

    private static void copyStream(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    private String getFullyQualifiedUrlForFileEntry(String portalUrl,String contextPath , long gId, FileEntry entry){
        return portalUrl + contextPath + "/documents/" + gId + "/" + entry.getFolderId() +  "/" +entry.getTitle();
    }

}
