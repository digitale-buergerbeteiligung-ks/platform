/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import ideaService.service.IdeasServiceUtil;

/**
 * Provides the HTTP utility for the
 * {@link IdeasServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasServiceSoap
 * @see HttpPrincipal
 * @see IdeasServiceUtil
 * @generated
 */
@ProviderType
public class IdeasServiceHttp {
	public static java.lang.String getIdeaById(HttpPrincipal httpPrincipal,
		long id) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeaById", _getIdeaByIdParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, id);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByCategory(
		HttpPrincipal httpPrincipal, long categoryId) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByCategory", _getIdeasByCategoryParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String searchIdeasByField(
		HttpPrincipal httpPrincipal, java.lang.String fieldName,
		java.lang.String query, long[] groupIds) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"searchIdeasByField", _searchIdeasByFieldParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fieldName, query, groupIds);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String seachIdeasByFieldArray(
		HttpPrincipal httpPrincipal, java.lang.String[] fieldNames,
		java.lang.String query, long[] groupIds) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"seachIdeasByFieldArray",
					_seachIdeasByFieldArrayParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fieldNames, query, groupIds);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String searchIdeasByFieldArrayQueryArray(
		HttpPrincipal httpPrincipal, java.lang.String[] fieldNames,
		java.lang.String[] queries, long[] groupIds) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"searchIdeasByFieldArrayQueryArray",
					_searchIdeasByFieldArrayQueryArrayParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fieldNames, queries, groupIds);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByIsPublished(
		HttpPrincipal httpPrincipal, boolean published) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByIsPublished",
					_getIdeasByIsPublishedParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey, published);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByIsVisibleOnMap(
		HttpPrincipal httpPrincipal, boolean visible) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByIsVisibleOnMap",
					_getIdeasByIsVisibleOnMapParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(methodKey, visible);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String deleteIdea(HttpPrincipal httpPrincipal,
		java.lang.String Id) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"deleteIdea", _deleteIdeaParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(methodKey, Id);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(IdeasServiceHttp.class);
	private static final Class<?>[] _getIdeaByIdParameterTypes0 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getIdeasByCategoryParameterTypes1 = new Class[] {
			long.class
		};
	private static final Class<?>[] _searchIdeasByFieldParameterTypes2 = new Class[] {
			java.lang.String.class, java.lang.String.class, long[].class
		};
	private static final Class<?>[] _seachIdeasByFieldArrayParameterTypes3 = new Class[] {
			java.lang.String[].class, java.lang.String.class, long[].class
		};
	private static final Class<?>[] _searchIdeasByFieldArrayQueryArrayParameterTypes4 =
		new Class[] {
			java.lang.String[].class, java.lang.String[].class, long[].class
		};
	private static final Class<?>[] _getIdeasByIsPublishedParameterTypes5 = new Class[] {
			boolean.class
		};
	private static final Class<?>[] _getIdeasByIsVisibleOnMapParameterTypes6 = new Class[] {
			boolean.class
		};
	private static final Class<?>[] _deleteIdeaParameterTypes7 = new Class[] {
			java.lang.String.class
		};
}