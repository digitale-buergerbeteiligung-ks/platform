/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.base;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalServiceRegistry;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import ideaService.model.Pictures;

import ideaService.service.PicturesLocalService;

import ideaService.service.persistence.CategoryPersistence;
import ideaService.service.persistence.CommentPersistence;
import ideaService.service.persistence.IdeasPersistence;
import ideaService.service.persistence.PicturesPersistence;
import ideaService.service.persistence.ReviewPersistence;
import ideaService.service.persistence.VideosPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the pictures local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link ideaService.service.impl.PicturesLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.impl.PicturesLocalServiceImpl
 * @see ideaService.service.PicturesLocalServiceUtil
 * @generated
 */
@ProviderType
public abstract class PicturesLocalServiceBaseImpl extends BaseLocalServiceImpl
	implements PicturesLocalService, IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ideaService.service.PicturesLocalServiceUtil} to access the pictures local service.
	 */

	/**
	 * Adds the pictures to the database. Also notifies the appropriate model listeners.
	 *
	 * @param pictures the pictures
	 * @return the pictures that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Pictures addPictures(Pictures pictures) {
		pictures.setNew(true);

		return picturesPersistence.update(pictures);
	}

	/**
	 * Creates a new pictures with the primary key. Does not add the pictures to the database.
	 *
	 * @param PictureId the primary key for the new pictures
	 * @return the new pictures
	 */
	@Override
	public Pictures createPictures(long PictureId) {
		return picturesPersistence.create(PictureId);
	}

	/**
	 * Deletes the pictures with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param PictureId the primary key of the pictures
	 * @return the pictures that was removed
	 * @throws PortalException if a pictures with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Pictures deletePictures(long PictureId) throws PortalException {
		return picturesPersistence.remove(PictureId);
	}

	/**
	 * Deletes the pictures from the database. Also notifies the appropriate model listeners.
	 *
	 * @param pictures the pictures
	 * @return the pictures that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Pictures deletePictures(Pictures pictures) {
		return picturesPersistence.remove(pictures);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(Pictures.class,
			clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return picturesPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end) {
		return picturesPersistence.findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator) {
		return picturesPersistence.findWithDynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return picturesPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection) {
		return picturesPersistence.countWithDynamicQuery(dynamicQuery,
			projection);
	}

	@Override
	public Pictures fetchPictures(long PictureId) {
		return picturesPersistence.fetchByPrimaryKey(PictureId);
	}

	/**
	 * Returns the pictures with the primary key.
	 *
	 * @param PictureId the primary key of the pictures
	 * @return the pictures
	 * @throws PortalException if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures getPictures(long PictureId) throws PortalException {
		return picturesPersistence.findByPrimaryKey(PictureId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery = new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(picturesLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Pictures.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("PictureId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		IndexableActionableDynamicQuery indexableActionableDynamicQuery = new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(picturesLocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(Pictures.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName("PictureId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {
		actionableDynamicQuery.setBaseLocalService(picturesLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Pictures.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("PictureId");
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {
		return picturesLocalService.deletePictures((Pictures)persistedModel);
	}

	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {
		return picturesPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns a range of all the pictureses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @return the range of pictureses
	 */
	@Override
	public List<Pictures> getPictureses(int start, int end) {
		return picturesPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of pictureses.
	 *
	 * @return the number of pictureses
	 */
	@Override
	public int getPicturesesCount() {
		return picturesPersistence.countAll();
	}

	/**
	 * Updates the pictures in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param pictures the pictures
	 * @return the pictures that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Pictures updatePictures(Pictures pictures) {
		return picturesPersistence.update(pictures);
	}

	/**
	 * Returns the category local service.
	 *
	 * @return the category local service
	 */
	public ideaService.service.CategoryLocalService getCategoryLocalService() {
		return categoryLocalService;
	}

	/**
	 * Sets the category local service.
	 *
	 * @param categoryLocalService the category local service
	 */
	public void setCategoryLocalService(
		ideaService.service.CategoryLocalService categoryLocalService) {
		this.categoryLocalService = categoryLocalService;
	}

	/**
	 * Returns the category persistence.
	 *
	 * @return the category persistence
	 */
	public CategoryPersistence getCategoryPersistence() {
		return categoryPersistence;
	}

	/**
	 * Sets the category persistence.
	 *
	 * @param categoryPersistence the category persistence
	 */
	public void setCategoryPersistence(CategoryPersistence categoryPersistence) {
		this.categoryPersistence = categoryPersistence;
	}

	/**
	 * Returns the comment local service.
	 *
	 * @return the comment local service
	 */
	public ideaService.service.CommentLocalService getCommentLocalService() {
		return commentLocalService;
	}

	/**
	 * Sets the comment local service.
	 *
	 * @param commentLocalService the comment local service
	 */
	public void setCommentLocalService(
		ideaService.service.CommentLocalService commentLocalService) {
		this.commentLocalService = commentLocalService;
	}

	/**
	 * Returns the comment persistence.
	 *
	 * @return the comment persistence
	 */
	public CommentPersistence getCommentPersistence() {
		return commentPersistence;
	}

	/**
	 * Sets the comment persistence.
	 *
	 * @param commentPersistence the comment persistence
	 */
	public void setCommentPersistence(CommentPersistence commentPersistence) {
		this.commentPersistence = commentPersistence;
	}

	/**
	 * Returns the ideas local service.
	 *
	 * @return the ideas local service
	 */
	public ideaService.service.IdeasLocalService getIdeasLocalService() {
		return ideasLocalService;
	}

	/**
	 * Sets the ideas local service.
	 *
	 * @param ideasLocalService the ideas local service
	 */
	public void setIdeasLocalService(
		ideaService.service.IdeasLocalService ideasLocalService) {
		this.ideasLocalService = ideasLocalService;
	}

	/**
	 * Returns the ideas persistence.
	 *
	 * @return the ideas persistence
	 */
	public IdeasPersistence getIdeasPersistence() {
		return ideasPersistence;
	}

	/**
	 * Sets the ideas persistence.
	 *
	 * @param ideasPersistence the ideas persistence
	 */
	public void setIdeasPersistence(IdeasPersistence ideasPersistence) {
		this.ideasPersistence = ideasPersistence;
	}

	/**
	 * Returns the pictures local service.
	 *
	 * @return the pictures local service
	 */
	public PicturesLocalService getPicturesLocalService() {
		return picturesLocalService;
	}

	/**
	 * Sets the pictures local service.
	 *
	 * @param picturesLocalService the pictures local service
	 */
	public void setPicturesLocalService(
		PicturesLocalService picturesLocalService) {
		this.picturesLocalService = picturesLocalService;
	}

	/**
	 * Returns the pictures persistence.
	 *
	 * @return the pictures persistence
	 */
	public PicturesPersistence getPicturesPersistence() {
		return picturesPersistence;
	}

	/**
	 * Sets the pictures persistence.
	 *
	 * @param picturesPersistence the pictures persistence
	 */
	public void setPicturesPersistence(PicturesPersistence picturesPersistence) {
		this.picturesPersistence = picturesPersistence;
	}

	/**
	 * Returns the review local service.
	 *
	 * @return the review local service
	 */
	public ideaService.service.ReviewLocalService getReviewLocalService() {
		return reviewLocalService;
	}

	/**
	 * Sets the review local service.
	 *
	 * @param reviewLocalService the review local service
	 */
	public void setReviewLocalService(
		ideaService.service.ReviewLocalService reviewLocalService) {
		this.reviewLocalService = reviewLocalService;
	}

	/**
	 * Returns the review persistence.
	 *
	 * @return the review persistence
	 */
	public ReviewPersistence getReviewPersistence() {
		return reviewPersistence;
	}

	/**
	 * Sets the review persistence.
	 *
	 * @param reviewPersistence the review persistence
	 */
	public void setReviewPersistence(ReviewPersistence reviewPersistence) {
		this.reviewPersistence = reviewPersistence;
	}

	/**
	 * Returns the videos local service.
	 *
	 * @return the videos local service
	 */
	public ideaService.service.VideosLocalService getVideosLocalService() {
		return videosLocalService;
	}

	/**
	 * Sets the videos local service.
	 *
	 * @param videosLocalService the videos local service
	 */
	public void setVideosLocalService(
		ideaService.service.VideosLocalService videosLocalService) {
		this.videosLocalService = videosLocalService;
	}

	/**
	 * Returns the videos persistence.
	 *
	 * @return the videos persistence
	 */
	public VideosPersistence getVideosPersistence() {
		return videosPersistence;
	}

	/**
	 * Sets the videos persistence.
	 *
	 * @param videosPersistence the videos persistence
	 */
	public void setVideosPersistence(VideosPersistence videosPersistence) {
		this.videosPersistence = videosPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		persistedModelLocalServiceRegistry.register("ideaService.model.Pictures",
			picturesLocalService);
	}

	public void destroy() {
		persistedModelLocalServiceRegistry.unregister(
			"ideaService.model.Pictures");
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return PicturesLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return Pictures.class;
	}

	protected String getModelClassName() {
		return Pictures.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = picturesPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = ideaService.service.CategoryLocalService.class)
	protected ideaService.service.CategoryLocalService categoryLocalService;
	@BeanReference(type = CategoryPersistence.class)
	protected CategoryPersistence categoryPersistence;
	@BeanReference(type = ideaService.service.CommentLocalService.class)
	protected ideaService.service.CommentLocalService commentLocalService;
	@BeanReference(type = CommentPersistence.class)
	protected CommentPersistence commentPersistence;
	@BeanReference(type = ideaService.service.IdeasLocalService.class)
	protected ideaService.service.IdeasLocalService ideasLocalService;
	@BeanReference(type = IdeasPersistence.class)
	protected IdeasPersistence ideasPersistence;
	@BeanReference(type = PicturesLocalService.class)
	protected PicturesLocalService picturesLocalService;
	@BeanReference(type = PicturesPersistence.class)
	protected PicturesPersistence picturesPersistence;
	@BeanReference(type = ideaService.service.ReviewLocalService.class)
	protected ideaService.service.ReviewLocalService reviewLocalService;
	@BeanReference(type = ReviewPersistence.class)
	protected ReviewPersistence reviewPersistence;
	@BeanReference(type = ideaService.service.VideosLocalService.class)
	protected ideaService.service.VideosLocalService videosLocalService;
	@BeanReference(type = VideosPersistence.class)
	protected VideosPersistence videosPersistence;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	@ServiceReference(type = PersistedModelLocalServiceRegistry.class)
	protected PersistedModelLocalServiceRegistry persistedModelLocalServiceRegistry;
}