/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import ideaService.model.Review;
import ideaService.service.ReviewLocalServiceUtil;
import ideaService.service.base.ReviewLocalServiceBaseImpl;

/**
 * The implementation of the review local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.ReviewLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ReviewLocalServiceBaseImpl
 * @see ideaService.service.ReviewLocalServiceUtil
 */
public class ReviewLocalServiceImpl extends ReviewLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.ReviewLocalServiceUtil} to access the review local service.
	 */

	public Review createReviewWithAutomatedDbId(boolean accepted, String feedback, long ideasIdRef, long userId){
		int nextDbId = (int)CounterLocalServiceUtil.increment(Review.class.getName());
		Review review = ReviewLocalServiceUtil.createReview(nextDbId);
		review.setAccepted(accepted);
		review.setFeedback(feedback);
		review.setIdeasIdRef(ideasIdRef);

		try {
			User user = UserLocalServiceUtil.getUser(userId);
			review.setUserName(user.getScreenName());
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return review;
	}

	public void persistReviewAndPerformTypeChecks(Review review){
		//TODO validate
		review.persist();
	}
}