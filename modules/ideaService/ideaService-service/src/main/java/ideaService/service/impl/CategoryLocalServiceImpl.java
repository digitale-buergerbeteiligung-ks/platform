/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import ideaService.model.Category;
import ideaService.service.CategoryLocalServiceUtil;
import ideaService.service.base.CategoryLocalServiceBaseImpl;
import ideaService.service.persistence.CategoryUtil;

/**
 * The implementation of the category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.CategoryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CategoryLocalServiceBaseImpl
 * @see ideaService.service.CategoryLocalServiceUtil
 */
public class CategoryLocalServiceImpl extends CategoryLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.CategoryLocalServiceUtil} to access the category local service.
	 */

	public List<Category> getAllCategories(){
		return CategoryUtil.findAll();
	}

	public List<Category> getAllCategoriesByProjectId(long projectId){
		List<Category> allCategories = CategoryUtil.findAll();
		List<Category> result = new ArrayList<Category>();

		for(Category c : allCategories){
			if(c.getProjectRef() == projectId){
				result.add(c);
			}
		}

		return result;
	}

	public Category createNewCategoryWithAutomatedDBId(long userId, long groupId, String description, String categoryTitle, long projectRef){
		int nextDbId = (int)CounterLocalServiceUtil.increment(Category.class.getName());
		Category nextCategory = CategoryLocalServiceUtil.createCategory(nextDbId);
		nextCategory.setUserId(userId);
		nextCategory.setGroupId(groupId);
		nextCategory.setDescription(description);
		nextCategory.setCategoryTitle(categoryTitle);
		nextCategory.setProjectRef(projectRef);

		try {
			User user = UserLocalServiceUtil.getUser(userId);
			nextCategory.setUserName(user.getScreenName());
		} catch (PortalException e) {
			e.printStackTrace();
		}

		return nextCategory;
	}
	//TODO validation
	public long persistIdeasAndPerformTypeChecks(Category cat){
		cat.persist();
		return cat.getPrimaryKey();
	}

}