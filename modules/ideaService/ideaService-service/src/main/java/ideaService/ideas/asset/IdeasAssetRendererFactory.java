package ideaService.ideas.asset;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.portal.kernel.util.WebKeys;

import ideaService.model.Ideas;
import ideaService.service.IdeasLocalService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.ServletContext;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


@Component(immediate = true,
  property = {"javax.portlet.name=" + "IDEAS"},
  service = AssetRendererFactory.class
  )
public class IdeasAssetRendererFactory extends
  BaseAssetRendererFactory<Ideas> {

	private IdeasLocalService _ideasLocalService;
	private static final boolean _LINKABLE = true;
	public static final String CLASS_NAME = Ideas.class.getName();
	public static final String TYPE = "idea";
	private final String ideas_key = "IDEAS";

  public IdeasAssetRendererFactory() {
    setClassName(CLASS_NAME);
    setLinkable(_LINKABLE);
    setPortletId(ideas_key);
    setSearchable(true);
    setSelectable(true);
  }

  @Override
  public AssetRenderer<Ideas> getAssetRenderer(long classPK, int type)
  throws PortalException {

    Ideas idea = _ideasLocalService.getIdeas(classPK);

    IdeasAssetRenderer ideaAssetRenderer =
    new IdeasAssetRenderer(idea);

    ideaAssetRenderer.setAssetRendererType(type);
    ideaAssetRenderer.setServletContext(this._servletContext);

    return ideaAssetRenderer;
  }

  @Override
  public String getClassName() {
    return CLASS_NAME;
  }

  @Override
  public String getType() {
    return TYPE;
  }
  @Override
  public boolean hasPermission(PermissionChecker permissionChecker,
  long classPK, String actionId) throws Exception {

    return true;
  }

  @Override
  public PortletURL getURLAdd(LiferayPortletRequest liferayPortletRequest,
      LiferayPortletResponse liferayPortletResponse, long classTypeId) {
	  //TODO Replace with acutal add url or find way to block it.
    PortletURL portletURL = null;

    try {
      ThemeDisplay themeDisplay = (ThemeDisplay)
      liferayPortletRequest.getAttribute(WebKeys.THEME_DISPLAY);

      portletURL = liferayPortletResponse.createLiferayPortletURL(getControlPanelPlid(themeDisplay),
    		  ideas_key, PortletRequest.RENDER_PHASE);
     // portletURL.setParameter("mvcRenderCommandName", "/guestbookwebportlet/edit_guestbook");
      portletURL.setParameter("showback", Boolean.FALSE.toString());
    } catch (PortalException e) {
    }

    return portletURL;
  }

  @Override
  public boolean isLinkable() {
    return _LINKABLE;
  }

  @Override
  public String getIconCssClass() {
      return "bookmarks";
  }

@Reference(target = "(osgi.web.symbolicname=com.liferay.docs.guestbook.portlet)", unbind = "-")
public void setServletContext(ServletContext servletContext) {
        _servletContext = servletContext;
    }
    private ServletContext _servletContext;

@Reference(unbind = "-")
    protected void setIdeasLocalService(IdeasLocalService ideasLocalService) {
        _ideasLocalService = ideasLocalService;
}

}
