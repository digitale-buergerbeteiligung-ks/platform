/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Pictures;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Pictures in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Pictures
 * @generated
 */
@ProviderType
public class PicturesCacheModel implements CacheModel<Pictures>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PicturesCacheModel)) {
			return false;
		}

		PicturesCacheModel picturesCacheModel = (PicturesCacheModel)obj;

		if (PictureId == picturesCacheModel.PictureId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, PictureId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{PictureId=");
		sb.append(PictureId);
		sb.append(", IdeasRef=");
		sb.append(IdeasRef);
		sb.append(", FileRef=");
		sb.append(FileRef);
		sb.append(", PictureUrl=");
		sb.append(PictureUrl);
		sb.append(", Position=");
		sb.append(Position);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Pictures toEntityModel() {
		PicturesImpl picturesImpl = new PicturesImpl();

		picturesImpl.setPictureId(PictureId);
		picturesImpl.setIdeasRef(IdeasRef);
		picturesImpl.setFileRef(FileRef);

		if (PictureUrl == null) {
			picturesImpl.setPictureUrl(StringPool.BLANK);
		}
		else {
			picturesImpl.setPictureUrl(PictureUrl);
		}

		picturesImpl.setPosition(Position);

		picturesImpl.resetOriginalValues();

		return picturesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		PictureId = objectInput.readLong();

		IdeasRef = objectInput.readLong();

		FileRef = objectInput.readLong();
		PictureUrl = objectInput.readUTF();

		Position = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(PictureId);

		objectOutput.writeLong(IdeasRef);

		objectOutput.writeLong(FileRef);

		if (PictureUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(PictureUrl);
		}

		objectOutput.writeInt(Position);
	}

	public long PictureId;
	public long IdeasRef;
	public long FileRef;
	public String PictureUrl;
	public int Position;
}