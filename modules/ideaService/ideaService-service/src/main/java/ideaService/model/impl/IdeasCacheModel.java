/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Ideas;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Ideas in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Ideas
 * @generated
 */
@ProviderType
public class IdeasCacheModel implements CacheModel<Ideas>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeasCacheModel)) {
			return false;
		}

		IdeasCacheModel ideasCacheModel = (IdeasCacheModel)obj;

		if (ideasId == ideasCacheModel.ideasId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ideasId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(67);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", ideasId=");
		sb.append(ideasId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", shortdescription=");
		sb.append(shortdescription);
		sb.append(", reviewStatus=");
		sb.append(reviewStatus);
		sb.append(", goal=");
		sb.append(goal);
		sb.append(", tags=");
		sb.append(tags);
		sb.append(", importance=");
		sb.append(importance);
		sb.append(", targetAudience=");
		sb.append(targetAudience);
		sb.append(", solution=");
		sb.append(solution);
		sb.append(", category=");
		sb.append(category);
		sb.append(", latitude=");
		sb.append(latitude);
		sb.append(", longitude=");
		sb.append(longitude);
		sb.append(", videoUrl=");
		sb.append(videoUrl);
		sb.append(", published=");
		sb.append(published);
		sb.append(", isVisibleOnMap=");
		sb.append(isVisibleOnMap);
		sb.append(", rating=");
		sb.append(rating);
		sb.append(", projectRef=");
		sb.append(projectRef);
		sb.append(", videoFileRef=");
		sb.append(videoFileRef);
		sb.append(", pageUrl=");
		sb.append(pageUrl);
		sb.append(", layoutRef=");
		sb.append(layoutRef);
		sb.append(", pitch=");
		sb.append(pitch);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Ideas toEntityModel() {
		IdeasImpl ideasImpl = new IdeasImpl();

		if (uuid == null) {
			ideasImpl.setUuid(StringPool.BLANK);
		}
		else {
			ideasImpl.setUuid(uuid);
		}

		ideasImpl.setIdeasId(ideasId);
		ideasImpl.setCompanyId(companyId);
		ideasImpl.setUserId(userId);

		if (userName == null) {
			ideasImpl.setUserName(StringPool.BLANK);
		}
		else {
			ideasImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			ideasImpl.setCreateDate(null);
		}
		else {
			ideasImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			ideasImpl.setModifiedDate(null);
		}
		else {
			ideasImpl.setModifiedDate(new Date(modifiedDate));
		}

		ideasImpl.setGroupId(groupId);

		if (title == null) {
			ideasImpl.setTitle(StringPool.BLANK);
		}
		else {
			ideasImpl.setTitle(title);
		}

		if (description == null) {
			ideasImpl.setDescription(StringPool.BLANK);
		}
		else {
			ideasImpl.setDescription(description);
		}

		if (shortdescription == null) {
			ideasImpl.setShortdescription(StringPool.BLANK);
		}
		else {
			ideasImpl.setShortdescription(shortdescription);
		}

		if (reviewStatus == null) {
			ideasImpl.setReviewStatus(StringPool.BLANK);
		}
		else {
			ideasImpl.setReviewStatus(reviewStatus);
		}

		if (goal == null) {
			ideasImpl.setGoal(StringPool.BLANK);
		}
		else {
			ideasImpl.setGoal(goal);
		}

		if (tags == null) {
			ideasImpl.setTags(StringPool.BLANK);
		}
		else {
			ideasImpl.setTags(tags);
		}

		if (importance == null) {
			ideasImpl.setImportance(StringPool.BLANK);
		}
		else {
			ideasImpl.setImportance(importance);
		}

		if (targetAudience == null) {
			ideasImpl.setTargetAudience(StringPool.BLANK);
		}
		else {
			ideasImpl.setTargetAudience(targetAudience);
		}

		if (solution == null) {
			ideasImpl.setSolution(StringPool.BLANK);
		}
		else {
			ideasImpl.setSolution(solution);
		}

		ideasImpl.setCategory(category);
		ideasImpl.setLatitude(latitude);
		ideasImpl.setLongitude(longitude);

		if (videoUrl == null) {
			ideasImpl.setVideoUrl(StringPool.BLANK);
		}
		else {
			ideasImpl.setVideoUrl(videoUrl);
		}

		ideasImpl.setPublished(published);
		ideasImpl.setIsVisibleOnMap(isVisibleOnMap);

		if (rating == null) {
			ideasImpl.setRating(StringPool.BLANK);
		}
		else {
			ideasImpl.setRating(rating);
		}

		ideasImpl.setProjectRef(projectRef);
		ideasImpl.setVideoFileRef(videoFileRef);

		if (pageUrl == null) {
			ideasImpl.setPageUrl(StringPool.BLANK);
		}
		else {
			ideasImpl.setPageUrl(pageUrl);
		}

		ideasImpl.setLayoutRef(layoutRef);

		if (pitch == null) {
			ideasImpl.setPitch(StringPool.BLANK);
		}
		else {
			ideasImpl.setPitch(pitch);
		}

		ideasImpl.setStatus(status);
		ideasImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			ideasImpl.setStatusByUserName(StringPool.BLANK);
		}
		else {
			ideasImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			ideasImpl.setStatusDate(null);
		}
		else {
			ideasImpl.setStatusDate(new Date(statusDate));
		}

		ideasImpl.resetOriginalValues();

		return ideasImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		ideasId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		groupId = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		shortdescription = objectInput.readUTF();
		reviewStatus = objectInput.readUTF();
		goal = objectInput.readUTF();
		tags = objectInput.readUTF();
		importance = objectInput.readUTF();
		targetAudience = objectInput.readUTF();
		solution = objectInput.readUTF();

		category = objectInput.readLong();

		latitude = objectInput.readDouble();

		longitude = objectInput.readDouble();
		videoUrl = objectInput.readUTF();

		published = objectInput.readBoolean();

		isVisibleOnMap = objectInput.readBoolean();
		rating = objectInput.readUTF();

		projectRef = objectInput.readLong();

		videoFileRef = objectInput.readLong();
		pageUrl = objectInput.readUTF();

		layoutRef = objectInput.readLong();
		pitch = objectInput.readUTF();

		status = objectInput.readInt();

		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(ideasId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(groupId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (shortdescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shortdescription);
		}

		if (reviewStatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reviewStatus);
		}

		if (goal == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(goal);
		}

		if (tags == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tags);
		}

		if (importance == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(importance);
		}

		if (targetAudience == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(targetAudience);
		}

		if (solution == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(solution);
		}

		objectOutput.writeLong(category);

		objectOutput.writeDouble(latitude);

		objectOutput.writeDouble(longitude);

		if (videoUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(videoUrl);
		}

		objectOutput.writeBoolean(published);

		objectOutput.writeBoolean(isVisibleOnMap);

		if (rating == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rating);
		}

		objectOutput.writeLong(projectRef);

		objectOutput.writeLong(videoFileRef);

		if (pageUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pageUrl);
		}

		objectOutput.writeLong(layoutRef);

		if (pitch == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(pitch);
		}

		objectOutput.writeInt(status);

		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
	}

	public String uuid;
	public long ideasId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public String title;
	public String description;
	public String shortdescription;
	public String reviewStatus;
	public String goal;
	public String tags;
	public String importance;
	public String targetAudience;
	public String solution;
	public long category;
	public double latitude;
	public double longitude;
	public String videoUrl;
	public boolean published;
	public boolean isVisibleOnMap;
	public String rating;
	public long projectRef;
	public long videoFileRef;
	public String pageUrl;
	public long layoutRef;
	public String pitch;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
}