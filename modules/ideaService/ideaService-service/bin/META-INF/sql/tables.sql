create table IDEA_Category (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	categoryTitle VARCHAR(75) null,
	description VARCHAR(75) null,
	projectRef LONG
);

create table IDEA_Comment (
	commentId LONG not null primary key,
	createDate DATE null,
	modifiedDate DATE null,
	companyId LONG,
	groupId LONG,
	IdeaRef LONG,
	commentRef LONG,
	commentText VARCHAR(75) null,
	userId LONG,
	userName VARCHAR(75) null
);

create table IDEA_Ideas (
	uuid_ VARCHAR(75) null,
	ideasId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	title VARCHAR(75) null,
	description STRING null,
	shortdescription VARCHAR(100) null,
	reviewStatus VARCHAR(75) null,
	goal STRING null,
	tags STRING null,
	importance STRING null,
	targetAudience STRING null,
	solution STRING null,
	category LONG,
	latitude DOUBLE,
	longitude DOUBLE,
	videoUrl STRING null,
	published BOOLEAN,
	isVisibleOnMap BOOLEAN,
	rating TEXT null,
	projectRef LONG,
	videoFileRef LONG,
	pageUrl STRING null,
	layoutRef LONG,
	pitch STRING null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null
);

create table IDEA_Pictures (
	PictureId LONG not null primary key,
	IdeasRef LONG,
	FileRef LONG,
	PictureUrl STRING null,
	Position INTEGER
);

create table IDEA_Review (
	uuid_ VARCHAR(75) null,
	reviewId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	accepted BOOLEAN,
	feedback STRING null,
	ideasIdRef LONG
);

create table IDEA_Videos (
	VideoId LONG not null primary key,
	IdeasRef LONG,
	FileRef LONG,
	VideoUrl STRING null,
	Extension VARCHAR(75) null
);