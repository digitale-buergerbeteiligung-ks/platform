<h1>Civitas Digitalis Platform: Installation & Configruation </h1>

<h3> A platform for crowd-based ideation, exploration and development of services in a smart city context </h3>

<h2> Installing MySQL for Liferay </h2>
<ol>
<h4> In order to run the Civitas Digitalis Platform locally, you will need to setup a local MySQL server:<h4>
<li> Go to https://dev.mysql.com/downloads/mysql/ and download the latest version </li>
<li>.Net 4.5 or above should be installed. Usually if it is missing, the MySQL installer would fetch and install it automatically, but in some isolated incidents, it would fail to do so. 
    <br> If it fails, download .Net 4.5 or above from Microsoft's website and install it manually.  </li>
<li> Visual C++ redistributable should be installed. Same goes for this too. In case the installer is unable to install it automatically, download it and install manually.  </li>
<li>Once the aforementioned packages are installed, retry the MySQL installer. </li>
<li>During the setup of MySQL, you will be asked to setup the User Name and Password. These should be noted down, as they will be required in the next step. </li>
<li> Now use <b> CREATE DATABASE lportal character set utf8 collate utf8_general_ci; </b> to setup the database (for example in the MySQL workbench). </li>
</ol>

<h2> Setup Liferay with MySQL and Eclispe </h2>
<li> <h4> Downloads </h4> </li>
    <ul>
  <li>Download Eclipse IDE for Java EE Developers available at http://www.eclipse.org/downloads/eclipse-packages/. </li>
  <li>Download Liferay Portal 7.0 GA4 Bundled with Tomcat available at https://www.liferay.com/de/downloads.  </li>
  <li>Check out platform repository in your preffered location (e.g. the Eclipse workspace).
  </ul>
  <li>
    <h4>Installing Liferay in Eclipse </h4>
    <ol>
        <li>Open Eclipse IDE and click on Help -> Install New Software.</li>
        <li>Click "Add..." button to open Add Site dialog.  </li>
        <li>Name : "Liferay IDE" and Location – http://releases.liferay.com/tools/ide/latest/stable/.</li>
        <li>Select OK and Select the Liferay IDE site from the combo selection box.  </li>
        <li> When the table refreshes you should see the Liferay tooling category and three entries for the Liferay IDE feature, select the checkboxes to install the feature.</li>
    </ol>
  </li>
  <li>
  <h4>Integrating Liferay Server in Eclipse</h4>
    <ol>
    <li>In Eclipse IDE, under Preference, look for Server and then Runtime Environment.  </li>
    <li>Look for Liferay 7.x, select it and click Next  </li>
    <li>Point the Liferay Portal Bundle Directory to the location of your Liferay server (downloaded in step 2).  </li>
    <li>Runtime JRE should be configured with the java version installed in the system (should point to a JDK). </li>
    </ol>
  </li>
  <li><h4>Start Liferay 7.x </h4> 
    <ul>
    <li>The server will start on localhost:8080 </li>
    <li>The server can now be found under Servers</li>
    </ul>
  </li>
  <li><h4>Liferay Server Configuration </h4>
  <ol>
    <li>When the server starts, a configuration screen will show up on localhost:8080</li>
    <li>We are using a MySQL database so you have to change the databases configuration (click on "change" under Database).</li>
    <li>Select "MySQL" as the Database Type.</li>
    <li>Change the field JDBC URL to <br> <b>jdbc:mysql://localhost/lportal?characterEncoding=UTF-8&dontTrackOpenResources=true&holdResultsOpenOverStatementClose=true&useFastDateParsing=false&useUnicode=true</b> </li>
    <li>Now set User Name and Password according to your MySQL installation.</li>
    <li>If everything was configured correctly, you will be redirected to the Liferay landing page. </li>
  </ol>
  <li> <h4> Common Errors </h4> </li> 
    <ol>
        <li>Liferay requires JDK 1.8 and wouldn't work if JRE1.8 is setup as Java_Path in environment variable.</li>
        <li>Always install the same bit version (32/64) of Java and eclipse. A wrong combination may lead to startup failure in Eclipse in some systems. </li>
        <li>Always make sure that the userName and pwd provided for db during the setup of Liferay is correct. <br> The database settings can later be found in, portal folder in the portal-setup-wizard.properties file. </li>
        <li>If you can't depoly any portlet directly throguh Eclipse, make sure that <b>liferay.workspace.home.dir</b> in gradle.properties is set to the location of your Liferay server.
        <li>To improve the startup time of the server you may set the Custom Launch Settings within the server configuration (in Eclipse) to "-XX:MaxPermSize=2048m -XX:PermSize=2048m  -Xms2048m  -Xmx2048m -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:SurvivorRatio=20 -XX:ParallelGCThreads=4". </li>
    </ol>
    <li> <h4> Next Steps </h4>
    <ol>
        <li> If you are new to Liferay developement go through the tutorial at https://dev.liferay.com/de/develop/tutorials/-/knowledge_base/7-0/developing-a-web-application.
        <li> You can ask Liferay related questions on their forum at https://web.liferay.com/de/community/forums/-/message_boards/category/239390. </li>
        <li> Use "git update-index --skip-worktree gradle.properties" to prevent the gradle.properties file from being overriden.
        <li> Add 'javascript.single.page.application.enabled=false' to the portal-setup-wizard file </li>
</ol>
